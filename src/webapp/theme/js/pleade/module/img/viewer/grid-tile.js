//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$
/**
 * @fileOverview Classe des tuiles utilsées pour créer la grid sur une image
 * @author Johan Cwiklinski
 * @author Julien Huet
 */
/**
 * @class Classe des tuiles utilsées pour créer la grid sur une image
 * @author Johan Cwiklinski
 * @author Julien Huet
*/
var PivGridTile = new Class(/** @lends PivGridTile.prototype */{
	/**
	 * Instance de la visionneuse
	 * @private
	 * @type pivViewer
	 */
	viewer: null,
	/**
	 * Type de l'image
	 * @type String
	 */
	mineType: null,
	/**
	 * Image parente de la tuile
	 * @type Image
	 */
	image : null,
	/**
	 * Position de la tuile dans le tableau
	 * @type Integer
	 */
	i: 0,
	/**
	 * Position de la tuile dans le tableau
	 * @type Integer
	 */
	j: 0,
	/**
	 * Position horizontale de la tuile
	 * @type Integer
	 */
	x: 0,
	/**
	 * Position verticale de la tuile
	 * @type Integer
	 */
	y: 0,
	/**
	 * Largeur de la tuile
	 * @type Float
	 */
	w: 0,
	/**
	 * Hauteur de la tuile
	 * @type Float
	 */
	h: 0,
	/**
	 * Version de la tuile
	 * @type Object
	 */
	version: null,
	
	/**
	* <strong>Constructeur</strong>
	* @param {pivViewer} viewer
	* @param {Image} img
	* @param {String} minetype
	* @param {Integer} i
	* @param {Integer} j
	* @param {Integer} x
	* @param {Integer} y
	* @param {Float} w
	* @param {Float} h
	*/
	initialize: function(viewer, img, mimetype, i, j, x, y, w, h) {
		this.viewer = viewer;
		this.image = img;
		this.mimetype = mimetype;
		this.i = i;
		this.j = j;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	},
	
	/**
	 * Retourne la source de l'image
	 * @return {String}
	 */
	getSrc: function() {
		var ret = this.image.originalVersion.src;
		if ( ret.indexOf('?') > -1 ) ret += '&';
		else ret += '?';
		return ret + 'x=' + this.x + '&y=' + this.y + '&w=' + this.w + '&h=' + this.h;
	},

	/**
	 * Retourne le nom de l'image
	 * @return {String}
	 */
	getName: function() {
		var ret = this.image.originalVersion.name;
		return ret;
	},

	/**
	 * Retourne l'url de l'image
	 * @return {String}
	 */
	getUrl: function() {
		var ret = this.image.originalVersion.url;
		if ( ret.indexOf('?') > -1 ) ret += '&';
		else ret += '?';
		return ret + 'x=' + this.x + '&y=' + this.y + '&w=' + this.w + '&h=' + this.h;
	},

	/**
	 * Retourne la tuile dans le but d'être affiché
	 * @param {Integer} ratio Le ratio utile pour le calcul des dimensions de la tuile
	 * @return {Element}
	 */
	getHTML: function(ratio) {
		var div = window.document.createElement('div');
		div = $(div);
		div.addClass('piv-mosaic-grid');
		div.setStyle('position', 'absolute');
		div.setStyle('left', this.x * ratio + 'px');
		div.setStyle('top', this.y * ratio + 'px');
		div.setStyle('width', this.w * ratio + 'px');
		div.setStyle('height', this.h * ratio + 'px');
		//var handler = this.handleClick(this);
		div.addEvent('click', this.handleClick.bind(this));
		return div;
	},

	/**
	 * Creer une version tuile de l'image
	 * @return {Object}
	 */
	getVersion: function() {
		if (this.version) return this.version;
		this.version = {
			src: this.getSrc(),
			url: this.getUrl(),
			name: this.getName(),
			width: this.w,
			height: this.h,
			label: _pivMessages.current_tile.replace(/%1/, this.i).replace(/%2/, this.j),
			role: 'tile',
			tile: this,
			num: this.num,
			mimetype: this.mimetype
		};
		this.image.addNewVersion(this.version);
		return this.version;
	},

	/**
	 * Action lorsque l'on clique sur une tuile pour l'afficher en tant qu'image principale
	 */
	handleClick: function() {
		//Pas de tooltip!
		this.viewer.showVersionToolTip = false;
		// On désactive la grille, pour revenir aux fonctionnalités "normales"
		/** FIXME: désactiver ça ici empêche la grille d'être désactivée par la suite... */
		/*var _btn = this.viewer.toolbar.btnGrid
		_btn.addClass('disabled');
		_btn.removeEvents();
		this.viewer.toolbar.createGrid();*/
		// On appelle le chargement de cette image
		this.viewer.displayVersion(this.getVersion());
		//Si l'aperçu est actif et qu'il est masqué, on l'affiche pour aider à la navigation dans les tuiles
		if(this.viewer.actives.preview && this.viewer.overview.hidden === true){
			this.viewer.overview.show();
		}
	},

	/**
	 * Retourne le type de la classe
	 * @return {String}
	 */
	identify: function() {
		return 'Classe PivGridTile';
	}
}
);