//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Commentaires sur une image
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
/**
 * @class Dessin d'un cadre sur un div
 * @author Julien Huet
 * @author Johan Cwiklinski
 * @augments MooTools.Log
 */
Selection = new Class( /** @lends Selection.prototype */{

	///////////////////////////////////////////////////////////////////////////////////
	// Constantes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Nom de l'évènement pour la molette de la souris
	 * DOMMouseScroll pour gecko et mousewheel pour les autres (http://www.quirksmode.org/dom/events/scroll.html#t20)
	 */
	/*MOUSE_WHEEL_EVENT_NAME: Prototype.Browser.Gecko ? 'DOMMouseScroll' : 'mousewheel',*/

	/**
	 * Est-ce que le client est Firefox 2, pour contourner le bug du pointerX/pointerY
	 */
	/*firefox2: false,*/

	///////////////////////////////////////////////////////////////////////////////////
	// Attributs
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Référence vers le cadre
	 * @type Frame
	 */
	frame: null,

	/**
	 * Div désinant le cadre de sélection du zoom
	 * @type Element
	 */
	box: null,

	/**
	 * Pointer vers les handlers du déplacement de la souris
	 * Utiles pour désenregistrer ces handlers
	 * @type Function
	 */
	mouseDownHandler: null,
	/**
	 * Pointer vers les handlers du déplacement de la souris
	 * Utiles pour désenregistrer ces handlers
	 * @type Function
	 */
	mouseMoveHandler: null,
	/**
	 * Pointer vers les handlers du déplacement de la souris
	 * Utiles pour désenregistrer ces handlers
	 * @type Function
	 */
	mouseUpHandler: null,

	///////////////////////////////////////////////////////////////////////////////////
	// Méthodes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 *  <strong>Constructeur</strong> Classe permettant de dessiner un rectangle sur un div	
	 * @param {Frame} frame
	 */
	initialize: function(frame) {
		this.frame = frame;
		this.isActive = false;
		this.firefox2 = navigator.userAgent.indexOf('Firefox/2') > -1 ? true : false;

		this.box = $('zoom-box');

		this.mouseDownHandler = this.onMouseDown.bindWithEvent(this);
		this.mouseMoveHandler = this.onMouseMove.bindWithEvent(this);
		this.mouseUpHandler= this.onMouseUp.bindWithEvent(this);

	/*	this.frame.div.stall(
			this.MOUSE_WHEEL_EVENT_NAME,
			this.onWheel.bindAsEventListener(this),
			this.frame.panel.startWheelZoom.bind(this.frame.panel),
			this.frame.panel.endWheelZoom.bind(this.frame.panel),
			piv.ZOOM_DELAY
		);*/
	},
	
	/**
	 * Activation de la fonction de dessin du rectangle
	 */
	selectOn: function() {
		this.frame.div.addEvent('mousedown', this.mouseDownHandler);
		this.frame.div.addEvent('mouseup', this.mouseUpHandler);
		this.isActive = true;
	},
	
	/**
	 * Désactivation de la fonction de dessin du rectangle
	 */
	selectOff: function() {
		this.frame.div.removeEvent('mousedown', this.mouseDownHandler);
		this.frame.div.removeEvent('mouseup', this.mouseUpHandler);
		this.isActive = false;
	},

	///////////////////////////////////////////////////////////////////////////////////
	// Evénements
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Molette de la souris
	 * Passe l'événement au cadre avec en paramètres les coordonnées du pointeur relatives au cadre.
	 * @param {Mootools.Event} event
	 */
	onWheel: function(event) {
		// BUG : dans cet événement, event.page.x et event.page.y sont incorrects sous firefox 2
		// On lui passe les coordonnées du centre du cadre
		/*if (this.firefox2) {
			this.frame.onWheelZoom(Event.wheelDelta(event), this.frame.width / 2, this.frame.height / 2);
		} else {
			this.frame.onWheelZoom(Event.wheelDelta(event), event.page.x - this.frame.left, event.page.y - this.frame.top);
		}*/
	},

	/**
	 * Initialisation du rectangle que l'on va dessiner
	 * @param {Mootools.Event} event
	 */
	onMouseDown: function(event) {
		this.startLeft = event.page.x - this.frame.left;
		this.startTop = event.page.y - this.frame.top;
		this.box.style.left = this.startLeft + 'px';
		this.box.style.top = this.startTop + 'px';
		this.box.style.visibility = 'visible';
		this.frame.div.addEvent('mousemove', this.mouseMoveHandler);
		event.stop();
		return false;
	},

	/**
	 * Redessine instantanement le cadre
	 * @param {Mootools.Event} event
	 */
	onMouseMove: function(event) {
		width = event.page.x  - this.frame.left - this.startLeft;
		height = event.page.y - this.frame.top - this.startTop;
		if (width < 0) {
			this.box.style.left = event.page.x - this.frame.left + 'px';
		}
		if (height < 0) {
			this.box.style.top = event.page.y - this.frame.top + 'px';
		}
		this.box.style.width = Math.abs(width) + 'px';
		this.box.style.height = Math.abs(height) + 'px';
		event.stop();
		return false;
	},

	/**
	 * Validation du rectangle
	 * @param {Mootools.Event} event
	 */
	onMouseUp: function(event) {
		this.frame.div.removeEvent('mousemove', this.mouseMoveHandler);

		// Coordonnées du cadre de sélection relatives au cadre
		var left = Math.min(this.startLeft, event.page.x - this.frame.left);
		var right = Math.max(this.startLeft, event.page.x - this.frame.left);
		var top = Math.min(this.startTop, event.page.y - this.frame.top);
		var bottom = Math.max(this.startTop, event.page.y - this.frame.top);

		// Coordonnées du panneau (l'image)
		var panelLeft = this.frame.panel.left;
		var panelRight = this.frame.panel.left + this.frame.panel.width;
		var panelTop = this.frame.panel.top;
		var panelBottom = this.frame.panel.top + this.frame.panel.height;

		// L'action n'est effectuée que si la selection contient une portion de l'image
		if (left < panelRight && right > panelLeft && top < panelBottom && bottom > panelTop) {
			// Coordonnées de l'intersection du cadre de zoom et de l'image :
			// Nécessaire si le rectangle de sélection a une partie en dehors du panneau de l'image ;
			// ne sera considéré que la zone de sélection qui recouvre l'image.
			var interLeft = left < panelLeft ? panelLeft : left;
			var interRight = right > panelRight ? panelRight : right;
			var interTop = top < panelTop ? panelTop : top;
			var interBottom = bottom > panelBottom ? panelBottom : bottom;
			var interWidth = interRight - interLeft;
			var interHeight = interBottom - interTop;
			var interRatio = interWidth / interHeight;

			var scale = 1.;
			if (this.frame.ratio > interRatio) {
				scale = this.frame.height / interHeight;
			} else {
				scale = this.frame.width / interWidth;
			}

			if (Math.abs(scale) != Infinity) {
				this.onSelectComplete(scale,interLeft,interRight,interTop,interBottom);
			}
		}

		this.box.style.width = '0px';
		this.box.style.height = '0px';
		this.box.style.visibility = 'hidden';

		event.stop();
		return false;
	},
	
	/**
	 * Action declenchée lorsque l'on a fini de créer le rectangle
	 * @param {Integer} scale 
	 * @param {Integer} interLeft
	 * @param {Integer} interRight
	 * @param {Integer} interTop
	 * @param {Integer} interBottom
	 */
	onSelectComplete: function(scale,interLeft,interRight,interTop,interBottom){}

});