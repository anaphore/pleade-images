//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Fonctions de zoom par zone de sélection
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
/**
 * @class Fonctions de zoom par zone de sélection
 * @author Julien Huet
 * @author Johan Cwiklinski
 * @augments Selection
 */
var Zoom = new Class(/** @lends Zoom.prototype */{
	Extends: Selection,
	
	/**
	*  <strong>Constructeur</strong> Permet de zoomer sur une sélection à main levée
	* @param {Frame} frame
	*/
	initialize: function(frame) {
		this.parent(frame);
	},
	
	/**
	* Action declenchée lorsque l'on a fini de créer le rectangle
	* @param {Integer} scale
	* @param {Integer} interLeft
	* @param {Integer} interRight
	* @param {Integer} interTop
	* @param {Integer} interBottom
	*/
	onSelectComplete: function(scale, interLeft, interRight, interTop, interBottom){
		this.frame.panel.onSelectZoom(
			scale,
			(interLeft + interRight) / 2,
			(interTop + interBottom) / 2
		);
		//une seule sélection suffit, on repasse en mode "normal"
		this.frame.setDraggable();
		this.frame.viewer.toolbar.btnZoomSelect.removeClass('checked');
	}
});
