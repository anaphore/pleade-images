//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$
/**
 * @fileOverview
 * @author David Bambouché
 * @author Johan Cwiklinski
 * @author Julien Huet
 */

/**
 * @class Un objet image et l'objet qui contient toutes les informations d'une images.
 * Il contient le nom, taille etc de chaque version de l'image à différente résolution.
 * C'est la classe resonsable de lancer la requète Ajax pour récupérer ces informations.<br/>
 * Le cadre et le conteneur du panneau ({@link pivPanel}).
 * @author David Bambouché
 * @author Johan Cwiklinski
 * @author Julien Huet
 * @augments Mootools.Log
 */
var Image = new Class(
	/** @lends Image.prototype */
	{
	///////////////////////////////////////////////////////////////////////////////////
	// Attributs
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Référence vers l'objet series
	 * @type Series
	 */
	series: null,

	/**
	 * Numéro de l'image dans la série si elle fait partie d'une série
	 * @type Integer
	 */
	num: 0,

	/**
	 * Tableau contenant les versions de l'image
	 * @type Object
	 */
	versions: null,

	/**
	 * La vue "grille" si définie
	 */
	grid: null,

	/**
	 * Référence vers la version originale
	 * @type Object
	 */
	originalVersion: null,

	/**
	 * Référence vers la version par défaut
	 * @type Object
	 */
	defaultVersion: null,

	/**
	 * Référence vers la version courante (celle affichée)
	 * @type Object
	 */
	currentVersion: null,

	/**
	 * Largeur / Hauteur (float)
	 * @type Float
	 */
	ratio: 1.0,

	/**
	 * Nom de l'image
	 * @type String
	 */
	name: "",

	/**
	 * Rotation de l'image
	 * @type Integer
	*/
	rotation: 0,

	///////////////////////////////////////////////////////////////////////////////////
	// Méthodes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * <strong>Constructeur</strong> 
	 * @param {Series} series
	 * @param {Integer} num
	 */
	initialize: function(series, num) {
		this.series = series;
		this.num = num;
		this.comments = new Array();
	},

	/**
	 * Récupère les informations d'une image dont le numéro est num
	 */
	get: function() {
		new Request({
			method: 'get',
			url: this.series.requestUrl,
			onSuccess: this.onGet.bind(this)
		}).send(this.getParams());
	},
	
	/**
	* Retourne les paramètres utilisé pour la récupération d'information d'une image
	*/
	getParams: function(){
		var param = "n=" + this.num;
		if(this.series.start){
			if(!isNaN(this.series.start)){
				param += '&p='+this.series.start;
			}else{
				param += '&np='+this.series.start;
			}
			
		}
		if(this.series.end){
			if(!isNaN(this.series.end)){
				param += '&d='+this.series.end;
			}else{
				param += '&nd='+this.series.end;
			}
			
		}
		return param;
	},

	/**
	 * Positionne une des images comme l'image courante
	 * @param {Object} version
	 */
	setCurrentVersion: function(version) {
		if($type(version)=='object'){
				this.currentVersion = this.versions[version.label];
		}else{
		this.currentVersion = this.versions[version];
		}

	},

	/**
	* Permet d'ajouter une novuelle version à l'image
	* @param {Object} version
	*/
	addNewVersion: function(version){
		this.versions[version.label] = version;
	},
	
	/**
	* Ajour d'un novueau commentaire
	* @param {CommentArea} comment
	*/
	addComment: function(comment){
		this.comments.push({id: comment.commonId, object: comment});
	},

	///////////////////////////////////////////////////////////////////////////////////
	// Evènements
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Récupère les informations d'une image dont le numéro est num
	 * @param {Mootools.Request} request
	 */
	onGet: function(request) {
		if(DEBUG_MODE){
			this.log('Image onGet');
		}
		
		var imageData = JSON.decode(request)[0];

		if (imageData.versions != undefined)
			this.versions = imageData.versions;
		/** FIXME: l'élément renvoyé par le JSON devrait être 'grid' et non 'mosaic' */
		if (imageData.mosaic != undefined)
			this.grid = imageData.mosaic;

		for (var i = 0; i < this.versions.length; i++) {
			if (this.versions[i].role == 'original')
				this.originalVersion = this.versions[i];
			if (this.versions[i].role == 'default')
				this.defaultVersion = this.versions[i];
			if (this.versions[i].label == undefined)
				this.versions[i].label = '';
			this.versions[i].summary = this.versions[i].label;
			this.versions[i].summary += " (" + ((this.isFlipped())?this.versions[i].height + ' x ' + this.versions[i].width:this.versions[i].width + ' x ' + this.versions[i].height) + ' px)'
			this.versions[i].url = this.series.serverUrl + '/' + this.versions[i].src;
			this.versions[i].num = i;
			if ( (this.name==null || this.name=='')
					&& this.versions[i].name!=null && this.versions[i].name!='')
				this.name = this.versions[i].name;
		}

		if (this.defaultVersion == null)
			this.defaultVersion = this.originalVersion;
		this.currentVersion = this.defaultVersion;

		this.ratio = this.getWidth('original') / this.getHeight('original');
		//this.name = imageData.name;

		this.series.onGetImage(this.num);
		if(DEBUG_MODE){
			this.log('Image onGet OK');
		}
	},

	/**
	 * Méthode appelée pendant le drag de l'image principale (pan).
	 * @param {Mootools.Event} e
	 */
	onDrag: function(e) {
		this.viewer.update();
	},

	/**
	 * Méthode appelée à la fin d'un drag de l'image principale (pan).
	 * @param {Mootools.Event} e
	 */
	endDrag: function(e) {
		this.viewer.update();
	},

	/**
	 * Exécuté une fois le chargement de l'image effectué
	 * @param {Mootools.Event} e
	 */
	onLoad: function(e) {
		this.notify();
	},
	/**
	 * Retourne l'angle de rotation de l'image
	 */
	getRotation: function() {
		return this.rotation;
	},
	/**
	 * Initialise l'angle de rotation de l'image
	 */
	reInitRotation: function() {
		this.rotation = 0;
	},
	/**
	 * Change l'angle de rotation de l'image
	 * @param {Integer} angle 
	 * @param {Boolean} add Savoir si c'est une rotation a droite ou à gauche
	 */
	setRotation: function(angle, add) {
		if ( angle && angle > 0 && angle <= 360 ) {
			if(add === false) { this.rotation += angle;
			} else {
				this.rotation = angle;
			}
			if( this.rotation > 360 ) {
				this.rotation -= 360;
			}
		}
		// on redefinit la ratio car on a pu redefinir les lageur et hauteur suivant l'angle de rotation (cf. getWith et getHeight)
		this.ratio = this.getWidth('original')/this.getHeight('original');
	},

	/**
	* Renvoie 'true' si la rotation est de 90° ou 270° ; 'false' sinon
	* @return {Boolean}
	*/
	isFlipped: function(){
		return (this.rotation == 90 || this.rotation == 270)?true:false;
	},

	/**
	* Retourne la largeur de l'image
	* @param version version à traiter. Peut prendre les valeurs original, default ou current
	* @return {Float}
	*/
	getWidth: function(version){
		var _ret = null;
		switch(version) {
			case 'original':
				return (this.isFlipped())?this.originalVersion.height:this.originalVersion.width;
				break;
			case 'default':
				return (this.isFlipped())?this.defaultVersion.height:this.defaultVersion.width;
				break;
			default:
				return (this.isFlipped())?this.currentVersion.height:this.currentVersion.width;
				break;
		}
	},

	/**
	* Retourne la hauteur de l'image
	* @param version version à traiter. Peut prendre les valeurs original, default ou current
	* @return {Float}
	*/
	getHeight: function(version){
		switch(version) {
			case 'original':
				return (this.isFlipped())?this.originalVersion.width:this.originalVersion.height;
				break;
			case 'default':
				return (this.isFlipped())?this.defaultVersion.width:this.defaultVersion.height;
				break;
			default:
				return (this.isFlipped())?this.currentVersion.width:this.currentVersion.height;
				break;
		}
	},

	/** Fonction appellée pour dessiner tous les commentaires relatifs à l'image
	 * @param {Float} width Largeur du commentaire
	 * @param {Float} height Hauteur du commentaire
	 */
	drawComments: function(width, height){
		this.comments.each(function(comment){
			comment.object.draw(width, height);	
		});
	},

	/** Suppression d'un commentaire
	 * @param {CommentArea} comment
	 */
	removeComment: function(comment){
		viewer.series.currentImage.comments.each(function(current){
				if (current.id === comment) {
					this.comments.erase(current);
				}
		}.bind(this));
	}

});
