//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * Event
 *
 * Extension de l'objet Event de prototype
 * Ajoute la méthode stall qui permet de lancer de façon temporisée
 * des callbacks à la fin et au début d'une série d'évènements répétés
 * La syntaxe est similaire à Event.observe avec des arguments supplémentaires.
 * 		Event.stall(
 * 			element,
 * 			"action",
 * 			eventHandler,
 * 			startHandler,
 * 			endHandler,
 * 			delay
 * 		);
 * Seuls les handlers eventHandler et startHandler pourront récupérer l'objet "event" en premier argument.
 * N'importe lequel des handlers peut être nul.
 * delay est en secondes
 */

$extend(Event, (function() {
	var cache = Event.cache;

	function wrapEndHandler(element, endHandler) {
		return function() {
			element.stallTimeOutId = 0;
			if (endHandler) {
				endHandler();
			}
		};
	}

	function getStallHandler(element, handler, startHandler, endHandler, delay) {
		return function(event) {
			if (element.stallTimeOutId == 0) {
				if (startHandler) {
					startHandler(event);
				}
			} else {
				window.clearTimeout(element.stallTimeOutId);
			}
			element.stallTimeOutId = window.setTimeout(wrapEndHandler(element, endHandler), delay * 1000);
			if (handler) {
				handler(event);
			}
		};
	}

	return {
		stall: function(element, eventName, handler, startHandler, endHandler, delay) {
			element.stallTimeOutId = 0;
			return $(element).addEvent(eventName, getStallHandler(element, handler, startHandler, endHandler, delay));
			//return Event.observe(element, eventName, getStallHandler(element, handler, startHandler, endHandler, delay));
		},

		wheelDelta: function(event) {
			var delta = 0;
			if (!event)
				event = window.event;
			if (event.wheelDelta) {
				delta = event.wheelDelta / 120;
				if (window.opera) delta = -delta;
			} else if (event.detail) {
				delta = -event.detail/3;
			}
			return Math.round(delta); //Safari Round
		}

/*		wheelSign: function(event) {
			var delta = 0;
			if (!event)
				event = window.event;
			return (event.wheelDelta )
			if (event.wheelDelta) {
				delta = event.wheelDelta / 120;
				if (window.opera) delta = -delta;
			} else if (event.detail) {
				delta = -event.detail/3;
			}
			return Math.round(delta); //Safari Round
		}*/
	};

})());

//$extend(Event, Event.Methods);

/*Element.addMethods({
  stall: Event.stall
});*/

//$extend(document, { stall: Element.Methods.stall.methodize() });
