//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Serie d'image
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
/**
 * @class Gestion d'une serie d'image
 * @author Julien Huet
 * @author Johan Cwiklinski
 * @augments Selection
 */
Series = new Class(/** @lends Series.prototype */{
	
	///////////////////////////////////////////////////////////////////////////////////
	// Attributs
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Référence vers la classe principale
	 * @type pivViewer
	 */
	viewer: null,

	/**
	 * Url du serveur
	 * @type String
	 */
	serverUrl: '',

	/**
	 * Chemin relatif du dossier des images depuis l'url du serveur
	 * @type String
	 */
	path: '',

	/**
	 * Url du dossier des images
	 * @type String
	 */
	baseUrl: '',

	/**
	 * Url des requètes json pour récupérer les informations des images, utilisée dans la classe Image.
	 * @type String
	 */
	requestUrl: '',
	
	/**
	 *
	 * @type String
	 */
	baselink: '',

	/**
	 * Tableau contenant les objets pivImage
	 * Cette classe ne sera instanciée qu'une fois, le tableau peut être initialisé ici.
	 * @type Array
	 */
	images: new Array(),

	/**
	 * Référence vers l'objet pivImage courant, contenu dans le tableau images
	 * @type Image
	 */
	currentImage: null,

	///////////////////////////////////////////////////////////////////////////////////
	// Méthodes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * <strong>Constructeur</strong> Représente une série d'images et permet de naviguer dans cette série.<br/>
La classe contient un tableau contenant tous les "imageset" utilisés. Un "image set" est en fait un tableau contenant<br/>
Les informations sur les différentes versions (résolutions) d'une image.
	 * @param viewer {piv.Viewer} Référence vers l'objet parent viewer
	 * @param serverUrl {String} L'adresse de base du serveur d'images
	 * @param path {String} Le chemin (path) de la série consultée
	 * @param size {int} La taille (nombre d'images) de la série
	 * @param baseLink
	 */
	initialize: function(viewer, serverUrl, path, size, baseLink, start, end) {
		this.viewer = viewer;
		this.serverUrl = serverUrl;
		this.path = path;
		this.baseUrl = this.serverUrl + '/' + this.path;
		this.size = size;
		this.requestUrl = this.serverUrl + '/' + this.path;
		this.baselink = baseLink;
		this.start = null;
		this.end = null;
		if(start){
			this.start = start;
		}
		if(end){
			this.end = end;
		}
	},
	/**
	 * Definit l'url pour les requêtes pour les series
	 * @param {String} end
	 */
	setRequestUrl: function(end){
		this.requestUrl += end;
	},
	/**
	 * Est-ce que la série contient déjà l'image ?
	 * @param {Integer} num
	 * @return {Boolean}
	 */
	hasImage: function(num) {
		return (this.images[num] != undefined);
	},

	/**
	 * Récupère les informations d'une image dont le numéro est num
	 * @param {Integer} num
	 */
	getImage: function(num) {
		/*if (num < 1 || num > this.size)*/
			if (num < 1 || (num > this.size && (this.start!=this.end)))
			return;
		this.images[num] = new Image(this, num);
		this.images[num].get();
	},

	/**
	 * Positionne une des images comme l'image courante
	 * @param {Integer} num
	 */
	setCurrentImage: function(num) {
		this.currentImage = this.images[num];
	},

	/**
	 * Positionne une des images comme l'image courante
	 * @param {Object} version
	 */
	setCurrentVersion: function(version) {
		this.currentImage.setCurrentVersion(version);
	},

	///////////////////////////////////////////////////////////////////////////////////
	// Evénements
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Appelé quand la nouvelle image a chargé ses informations
	 * @param {Integer} num
	 */
	onGetImage: function(num) {
		this.viewer.displayImage(num);
	}

});
