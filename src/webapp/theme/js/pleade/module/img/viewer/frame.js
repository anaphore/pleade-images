//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview
 * @author David Bambouché
 * @author Johan Cwiklinski
 * @author Julien Huet
 */

/**
 * @class Définition des propriétés et des méthodes du cadre<br/>
 * Le cadre et le conteneur du panneau ({@link pivPanel}).
 * @author David Bambouché
 * @author Johan Cwiklinski
 * @author Julien Huet
 * @augments LayoutManager
 */
var Frame = new Class(/** @lends Frame.prototype */{
	Implements: [LayoutManager],
	
	///////////////////////////////////////////////////////////////////////////////////
	// Attributs
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Référence vers la classe principale
	 * @type pivViewer
	 */
	viewer: null,

	/**
	 * Classe réalisant la sélection d'une portion d'image dans un cadre pour zoomer dessus,
	 * ainsi que le zoom avec la molette de la souris.
	 * @type Zoom
	 */
	zoom: null,

	/**
	 * Div constituant le cadre
	 * @type Element
	 */
	div: null,

	/**
	 * La taille des marges verticales cumulées, entre le cadre et le viewport (l'espace de la page)
	 * C'est une donnée constante qui ne change pas après redimenssionnement de la fenêtre et qui doit
	 * être calculée dans le constructeur.
	 * Elle est utilisée dans la fonction resize
	 * @type Integer
	 */
	margin: 0,

	/**
	 * Raport de la largeur du cadre sur sa hauteur
	 * @type Float
	 */
	ratio: 1.0,

	/**
	 * Largeur du cadre
	 * @type Float
	 */
	width: 0.,

	/**
	 * Hauteur du cadre
	 * @type Float
	 */
	height: 0.,

	/**
	 * Coordonnée horizontale du panneau par rapport au viewport
	 * @type Float
	 */
	left: 0.,

	/**
	 * Coordonnée verticale du panneau par rapport au viewport
	 * @type Float
	 */
	top: 0.,

	/**
	 * Verrou sur le zoom
	 * @type Boolean
	 */
	zoomLock: false,

	/**
	 * Position associée au verrou du zoom
	 * @type Object
	 */
	lockedPos: {
		left: 0,
		top: 0
	},
	
	/**
	 * Angle de rotation associé au verrou du zoom
	 * @type Integer
	 */
	 zoomAngle: 0,

	/**
	 * Facteur de zoom de l'image associé au verrou du zoom
	 * C'est la largeur de l'image affichée divisée par sa largeur réelle
	 * @type Integer
	 */
	lockedScale: 1,

	/**
	 * Tableaux de références de méthodes à exécuter lors de l'événement resize
	 * Les diverses classes peuvent enregistrer et désenregistrer un ou plusieurs handlers avec les méthodes
	 * onResizeRegister et onResizeUnregister
	 * @type Array
	 */
	onResizeObservers: new Array(),

	/**
	 * Image signifiant qu'une image se charge
	 * @type Element
	 */
	imageLoading: null,
	
	/**
	 * Référence vers la panneau contenant l'image
	 * @type pivPanel
	 */
	panel: null,

	///////////////////////////////////////////////////////////////////////////////////
	// Méthodes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * <strong>Constructeur</strong>
	 * @param {pivViewer} viewer Instance de la visionneuse
	 */
	initialize: function(viewer) {
		this.viewer = viewer;
		this.info = $('piv-series-info');
		this.div = $('piv-mcol-frame');
		this.initMargin();
		
		//l'évènement resize ne fonctionne pas correctement sous IE
		if ( !Browser.Engine.trident )
			window.addEvent('resize',this.onResizeStart.bindWithEvent(this));
	
		this.initPanel();
		this.initLoading();
		this.initZoom();
		this.setDraggable();
	},

	/** Initialise l'image de chargement */
	initLoading: function(){
		this.loading = $('pnl-loading');
		this.imageLoading = $('pnl-image-loading');
	},

	/** Initialise l'outil de zoom */
	initZoom: function(){
		this.zoom = new Zoom(this);
	},
	
	/**
	 * Initialisation des marges
	 */
	initMargin: function(){
		var margin = 0;
		margin = this.div.getCoordinates().top;
		//les bordures de 'piv-body-main-mcol' doivent être prises en compte
		margin += $('piv-body-main-mcol').getStyle('border-top').toInt()
		margin += $('piv-body-main-mcol').getStyle('border-bottom').toInt()
		if( $('pl-pg-footer') ) {
			margin += $('pl-pg-footer').getHeight();
		}
		this.margin = margin;
	},
	
	/**
	 * Redimensionne le cadre afin qu'il prenne toute la place disponible.
	 */
	resize: function() {
    var tmp = Window.getHeight() - this.margin;
		this.div.style.height = tmp + "px";
		this.height = this.div.getHeight();
		this.width = this.div.getWidth();
		this.left = this.div.getCoordinates().left;
		this.top = this.div.getCoordinates().top;
		this.ratio = this.width / this.height;
		this.centerLoading();
	},

	/**
	* Initialise le panel de l'objet frame
	* @returns {pivPanel} Le panel
	*/
	initPanel: function(){
		this.panel = new pivPanel(this.viewer, this);
	},

	/**
	 * Définit le panneau comme pouvant être déplacé
	 */
	setDraggable: function() {
		this.zoom.selectOff();
		this.panel.unlockDrag();
		this.panel.div.setStyle('cursor', 'move');
		this.div.setStyle('cursor', 'auto');
	},

	/**
	 * Définit le panneau comme pouvant être zoomé par sélection d'une région
	 */
	setZoomable: function() {
		this.panel.lockDrag();
		this.div.setStyle('cursor', 'crosshair');
		this.panel.div.setStyle('cursor', 'crosshair');
		this.zoom.selectOn();
	},

	/**
	 * Centre le dialogue d'attente de chargement
	 */
	centerLoading: function() {
		this.loading.setStyle('top', ((this.height - this.getHeight(this.loading) ) / 2));
		this.loading.setStyle('left', ((this.width - this.getWidth(this.loading) ) / 2));
	},

	/**
	 * Affiche le panneau de chargement
	 */
	showLoading: function() {
		this.loading.style.visibility = 'visible';
	},

	/**
	 * Cache le panneau de chargement
	 */
	hideLoading: function() {
		this.loading.style.visibility = 'hidden';
	},

	/**
	 * Vérouille de zoom et sauvegarde la position et le ratio
	 */
	lockZoom: function() {
		this.zoomLock = true;
		this.lockedPos.left = this.panel.left;
		this.lockedPos.top = this.panel.top;
		this.lockedScale = this.panel.width / this.panel.image.getWidth('current');
		this.zoomAngle = this.panel.image.getRotation();
		this.onResizeUnregister(this.panel.resizeHandler);
	},

	/**
	 * Dévérouille de zoom
	 */
	unlockZoom: function() {
		this.zoomLock = false;
		this.onResizeRegister(this.panel.resizeHandler, true);
	},

	/**
	 * Enregistre les paramètres courant d'échelle, de position du panel et d'angle
	 */
	updateLockedParams: function() {
		if (this.zoomLock) {
			this.lockedPos.left = this.panel.left;
			this.lockedPos.top = this.panel.top;
			this.lockedScale = this.panel.width / this.panel.image.getWidth('current');
			this.zoomAngle = this.panel.image.getRotation();
		}
	},

	/**
	 * Permet à un autre objet d'enregistrer un callback à exécuter lors de l'évènement resize.
	 * Si first est vrai, la référence sera insérée en premier et donc exécutée en premier lors de l'événement.
	 */
	onResizeRegister: function(callback, first) {
		if (first) {
			this.onResizeObservers.unshift(callback);
		} else {
			this.onResizeObservers.push(callback);
		}
	},

	/**
	 * Permet à un autre objet de se désenregistrer de l'évènement resize
	 */
	onResizeUnregister: function(callback) {
		this.onResizeObservers = this.onResizeObservers.erase(callback);
	},

	///////////////////////////////////////////////////////////////////////////////////
	// Evènements
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Méthode appelée lorsque la molette de la souris tourne
	 * delta (entier relatif) : quantité associée à déplacement angulaire de la molette
	 * left : coordonnée horizontale du pointeur relative au cadre
	 * top : coordonnée verticale du pointeur relative au cadre
	 */
	onWheelZoom: function(delta, left, top) {
		this.panel.onWheelZoom(delta, left, top);
		this.viewer.overview.setMask();
		this.viewer.updateZoomStatus();
	},

	/**
	 * Appelé lors d'un zoom par la sélection d'un région.
	 * scale est le facteur d'agrandissement calculé en fonction de la taille du rectangle de sélection
	 * left et top sont les coordonnées du centre du rectangle de sélection relatives au cadre
	 */
	onSelectZoom: function(scale, left, top) {
		this.panel.onSelectZoom(scale, left, top);
		this.frame.viewer.overview.setMask();
		this.viewer.updateZoomStatus();
	},

	/**
	 * Redimensionne le cadre afin qu'il prenne toute la place disponible.
	 * Evénement initial
	 */
	onResizeStart: function(event) {
		this.resize();
		//event.stop();
		return false;
	},

	/**
	 * Redimensionne le cadre afin qu'il prenne toute la place disponible.
	 * Evénement final
	 */
	onResizeEnd: function() {
		this.onResizeObservers.each(function(callback) { callback(); });
	}

});
