//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Conteneur de l'image
 * @author David Bambouché
 * @author Johan Cwiklinski
 * @author Julien Huet
 */

/**
 * @class Conteneur de l'image
 * @author David Bambouché
 * @author Johan Cwiklinski
 * @author Julien Huet
 * @augments MooTools.Log
 */
pivPanel = new Class(/** @lends pivPanel.prototype */{
	///////////////////////////////////////////////////////////////////////////////////
	// Constantes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Facteurs de zoom in
	 * @constant
	 * @type Float
	 */
	ZOOM_IN: 1.2,
	/**
	 * Facteurs de zoom out
	 * @constant
	 * @type Float
	 */
	ZOOM_OUT: 0.8333333,
	/**
	 * Maximum de zoom autorisé
	 * @constant
	 * @type Integer
	 */
	MAX_ZOOM: 120,
	/**
	 * Minimum de zoom autorisé
	 * @constant
	 * @type Integer
	 */
	MIN_ZOOM: 20,
	/**
	 * @type Boolean
	 */
	max_zoom_reached: false,
	/**
	 * @type Boolean
	 */
	min_zoom_reached: false,

	///////////////////////////////////////////////////////////////////////////////////
	// Attributs
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Référence vers la classe principale
	 * @type pivViewer
	 */
	viewer: null,

	/**
	 * Référence vers le cadre
	 * @type Frame
	 */
	frame: null,

	/**
	 * Une référence vers l'image affichée
	 * @type Image
	 */
	image: null,

	/**
	 * Div contenant l'image
	 * @type Element
	 */
	div: null,

	/**
	 * L'image
	 * @type Element
	 */
	img: null,

	/**
	 * Classe yui réalisant le déplacement du panel
	 * @type Mootols.Drag
	 */
	dd: null,

	/**
	 * Largeur affichée de l'image (du panneau) en valeur décimale
	 * @type Float
	 */
	width: 0.,

	/**
	 * Hauteur affichée de l'image (du panneau) en valeur décimale
	 * @type Float
	 */
	height: 0.,

	/**
	 * Coordonnée horizontale du panneau par rapport au cadre
	 * @type Float
	 */
	left: 0.,

	/**
	 * Coordonnée verticale du panneau par rapport au cadre
	 * @type Float
	 */
	top: 0.,
	
	/**
	* Booleen pour bloquer le zoom.
	 * @type Boolean
	*/
	zoomLock: false,

	/**
	 * Référence utilisée pour enregistrer la méthode center pour l'événement resize du frame
	 * Il est nécessaire de stocker cette référence construite avec bind() pour pouvoir la désenregistrer.
	 * @type Function
	 */
	resizeHandler: null,

	///////////////////////////////////////////////////////////////////////////////////
	// Méthodes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * <strong>Constructeur</strong>
	 * @param {pivViewer} viewer Instance de la visionneuse
	 * @param {pivFrame} frame
	 */
	initialize: function(viewer, frame) {
		this.viewer = viewer;
		this.frame = frame;

		this.MAX_ZOOM = viewer.actives.maxZoom;
		this.MIN_ZOOM = viewer.actives.minZoom;

		this.div = $('pnl-picture');
	
		this.dd = this.div.makeDraggable({
			onComplete: this.endDrag.bind(this),
			preventDefault: true
		});
	
		this.resizeHandler = this.center.bind(this);
		
		this.div.addEvent('mousewheel', this.onMousWheel.bind(this));
		
	},
	/**
	 * Action lors du l'utilisation de la molette
	 * @param {Mootools.Event} e
	 */
	onMousWheel: function(e){
		var left = e.client.x;
		var top = e.client.y;
		if(e.wheel > 0){
			// zoomPoint ou zoomCenter 
			this.zoomPointIn(left,top);
		}
		if(e.wheel < 0){
			this.zoomPointOut(left,top);
		}
	},
	
	/**
	 * Défini une nouvelle image à afficher dans le cadre.
	 */
	setImage: function(image) {
		this.image = image;
		this.hide();
		this.div.empty();
		this.img = $(document.createElement('img'));
		this.div.adopt(this.img);
		this.loadImage();
		this.image.drawComments(this.width, this.height);
	},

	/**
	 * Lance le chargement de l'objet HTML image.
	 * Ce code est séparé de setImage pour que les surchages (classe HiDef) puisse insérer des éléments
	 * dans le panneau avant que le chargement de l'image principale soit lancé.
	 */
	loadImage: function() {
		// si on a enregistré un angle de rotation, il faut le resservir avant de charger l'image
		if (this.frame.zoomLock == false) {} else { this.viewer.series.currentImage.setRotation(this.frame.zoomAngle, true); }
		this.img.addEvent('load', this.onLoad.bind(this));
		var _src = this.image.currentVersion.url;
		if (_src.indexOf('?') == -1)
			_src += '?';
		else
			_src += '&';
		_src += 'r=' + this.image.rotation;
		this.img.src = _src;
	},

	/**
	 * Change l'image pour pour la version courante
	 */
	setCurrentVersion: function() {
		this.hide();
		this.img.src = this.image.currentVersion.url;
	},

	/**
	 * Calcul la valeur du zoom en pourcents, relativement à la taille de l'image originale
	 * @return {Float}
	 */
	getZoom: function() {
		var zoom = parseInt(100 * this.width / this.image.getWidth('current'));
		//affice un tooltip stipulant l'existance d'une image de meilleure qualité
		if( this.viewer.showVersionToolTip ){
			//on n'affiche le tooltip en question que si le zomm dépasse 100% et qu'on n'est pas à l'image originale
			if( zoom > 100 && this.image.currentVersion != this.image.originalVersion ){
					this.viewer.toolbar.createVersionsToolTip();
			} else {
				if( this.viewer.toolbar.versionToolTip )
					this.viewer.toolbar.versionToolTip.hide();
			}
		}
		return zoom;
	},

	/**
	 * Retourne les coordonnées de la partie visible de l'image à l'écran, relativement aux proportions de l'objet image passé en argument.
	 * Si la fonction est appelée sans argument, les coordonnées et les proportions seront celles affichées dans le navigateur.
	 * @param {Float} imageWidth
	 * @return {Object}
	 */
	getRegion: function(imageWidth) {
		var scale = (imageWidth != undefined) ? imageWidth / this.width : 1;
		var region = {};
		region.left = this.left > 0 ? 0 : -this.left * scale;
		region.top = this.top > 0 ? 0 : -this.top * scale;
		region.right = (this.left + this.width < this.frame.width ? this.width : this.frame.width - this.left) * scale;
		region.bottom = (this.top + this.height < this.frame.height ? this.height : this.frame.height - this.top) * scale;
		region.width = region.right - region.left;
		region.height = region.bottom - region.top;
		return region;
	},

	/**
	 * Retourne les coordonnées de la partie visible de l'image à l'écran,
	 * relativement aux proportions de l'image originale.
	 * @return {Object}
	 */
	getOriginalRegion: function() {
		return this.getRegion(this.image.getWidth('original'));
	},

	/**
	 * Fixe la position du panneau dans le cadre
	 * @param {Float} left
	 * @param {Float} top
	 */
	setPos: function(left, top) {
		this.setLeftPos(left);
		this.setTopPos(top);
	},

	/**
	* Fixe la position en hauteur du panneau dans le cadre
	*/
	setTopPos: function(value) {
		this.top = value;
		this.div.style.top = Math.ceil(value) + 'px';
	},

	/**
	* Fixe la position en hauteur du panneau dans le cadre
	*/
	setLeftPos: function(value) {
		this.left = value;
		this.div.style.left = Math.ceil(value) + 'px';
	},

	/**
	 * Met à jour les coordonnées left et top de la classe à partir des coordonnées du div
	 */
	updatePos: function() {
		this.left = parseInt(this.div.style.left);
		this.top = parseInt(this.div.style.top);
	},

	/**
	 * Fixe la taille du panneau et de son image
	 * @param {Float} width
	 * @param {Float} height
	 */
	setSize: function(width, height) {
		this.width = width;
		this.height = height;
		this.img.width = width;
		this.img.height = height;
		this.viewer.overview.onPanelResize();
		this.viewer.updateZoomStatus();
		this.image.drawComments(width, height);
	},

	/**
	 * Fixe la largeur
	 * @param {Float} width
	 */
	setWidth: function(width) {
		this.setSize(width, width / this.image.ratio);
	},

	/**
	 * Fixe la hauteur
	 * @param {Float} height
	 */
	setHeight: function(height) {
		this.setSize(height * this.image.ratio, height);
	},

	/**
	 * Fixe la taille de l'image à sa taille réelle (taille de l'image originale)
	 */
	setRealSize: function() {
		this.setSize(this.image.getWidth('current'), this.image.getHeight('current'));
		this.frame.panel.center();
	},

	/**
	 * Fixe la taille de l'image pour qu'elle rentre entièrement dans le cadre
	 * En fonction des ratios, la taille de l'image est définie par la largeur ou la hauteur du cadre
	 *
	 * Si l'image est plus petite que la frame, on affiche celle ci à 100%
	 */
	fitFrame: function() {
		if (this.frame.ratio > this.image.ratio) {
			this.setHeight( (this.image.currentVersion.height > this.frame.height) ? this.frame.height : this.image.currentVersion.height );
		} else {
			this.setWidth( (this.image.currentVersion.width > this.frame.width) ? this.frame.width : this.image.currentVersion.width );
		}
		this.frame.panel.center();
	},

	/**
	 * Centre l'image dans le cadre
	 */
	center: function() {
		this.setPos((this.frame.width - this.width) / 2, (this.frame.height - this.height) / 2);
	},

	/**
	 * Centre horizontalement l'image dans le cadre
	 */
	hcenter: function(){
		this.setPos((this.frame.width - this.width) / 2, this.top);
	},

	/**
	 * Zoom
	 * @param {Float} scale Facteur de zoom
	 */
	zoom: function(scale) {
		this.setSize(this.width * scale, this.height * scale)
	},

	/**
	 * Zoom d'un facteur "scale" et déplace l'image de sorte que le point de l'image sous le curseur ("left", "top") reste fixe à l'écran.
	 * Démonstration du calcul pour la coordonnée horizontale :
	 * Soit f le facteur de zoom
	 * Soit p la coordonnée horizontale du point de l'image sous le curseur relative au cadre
	 * Soit p' la coordonnée horizontale du point de l'image qui était sous le pointeur, après agrandissement
	 * Soit x la coordonnée horizontale de l'image avant le zoom
	 * Soit x' la coordonnée horizontale de l'image après le zoom
	 * Soit l la distance du point gauche de l'image, jusqu'au pointeur avant le zoom
	 * Soit l' la distance du point gauche de l'image, jusqu'au pointeur après le zoom
	 *  l' = fl
	 *  x + l = p
	 *  x' + l' = p'
	 * On veut que le point de l'image reste sous le curseur, soit p = p', donc :
	 *  x' = fx + p(1 - z)
	 * Le point sous le curseur devient le centre de l'homothétie.
	 * @param {Float} scale Facteur de zoom
	 * @param {Float} left Coordonnée du curseur
	 * @param {Float} top Coordonnée du curseur
	 */
	zoomPoint: function(scale, left, top) {
		if(!this.zoomLock){
			var _nextZoom = this.getZoom() * scale
			//On ne lance le zoom que si on est dans la bonne tranche
			if( scale > 1 ) { //cas zoomin
				this.min_zoom_reached = false;
				if( _nextZoom > this.MAX_ZOOM ) { //calcul du nouveau scale
					scale = this.MAX_ZOOM / this.getZoom();
					this.max_zoom_reached = true;
				}
			} else { //cas zoomout
				this.max_zoom_reached = false;
				if( _nextZoom < this.MIN_ZOOM ) { //calcul du nouveau scale
					scale = this.MIN_ZOOM / this.getZoom();
					this.min_zoom_reached = true;
				}
			}
			this.setPos(scale * this.left + left * (1 - scale), scale * this.top + top * (1 - scale));
			this.zoom(scale);
		}
	},

	/**
	 * Zoom d'un facteur "scale" et positionne l'image de sorte que le point dont les coordonnées sont passées
	 * en arguments, se retrouve au centre du cadre.
	 * Démonstration du calcul pour la coordonnée horizontale :
	 * Soit f le facteur de zoom
	 * Soit p la coordonnée horizontale relative au cadre du point de l'image que l'on veut centrer
	 * Soit p' la coordonnée horizontale relative au cadre du point de l'image que l'on veut centrer, après agrandissement
	 * Soit x la coordonnée horizontale de l'image avant le zoom
	 * Soit x' la coordonnée horizontale de l'image après le zoom
	 * Soit l la distance du point gauche de l'image, jusqu'au pointeur avant le zoom
	 * Soit l' la distance du point gauche de l'image, jusqu'au pointeur après le zoom
	 * Soit L la largeur du cadre
	 *  l' = fl
	 *  x + l = p
	 *  x' + l' = p'
	 * On veut que le point de l'image ciblé se retrouve au centre du cadre, soit p' = L/2, donc :
	 *  x' = L/2 - f(p - x)
	 * @param {Float} scale Facteur de zoom
	 * @param {Float} left Coordonnée que l'on souhaite voir au centre
	 * @param {Float} top Coordonnée  que l'on souhaite voir au centre
	 */
	zoomPointToCenter: function(scale, left, top) {
		this.setPos(
			this.frame.width / 2 - scale * (left - this.left),
			this.frame.height / 2 - scale * (top - this.top)
		);
		//FIXME: prendre en compte le zoom maxi ici
		this.zoom(scale);
	},

	/**
	 * Zoom avant sur un point
	 * Cette méthode est définie dans le constructeur avec la méthode prototype curry à partir de zoomPoint
	 * @param {Float} left Coordonnée du curseur
	 * @param {Float} top Coordonnée du curseur
	 */
	zoomPointIn: function(left, top) {
		this.zoomPoint(this.ZOOM_IN, left, top);
	},

	/**
	 * Zoom arrière sur un point
	 * Cette méthode est définie dans le constructeur avec la méthode prototype curry à partir de zoomPoint
	 * @param {Float} left Coordonnée du curseur
	 * @param {Float} top Coordonnée du curseur
	 */
	zoomPointOut: function(left, top) {
			this.zoomPoint(this.ZOOM_OUT, left, top);
	},

	/**
	 * Zoom avant sur le centre du cadre
	 */
	zoomCenterIn: function() {
		this.zoomPointIn(this.frame.width / 2, this.frame.height / 2);
	},

	/**
	 * Zoom arrière sur le centre du cadre
	 */
	zoomCenterOut: function() {
		this.zoomPointOut(this.frame.width / 2, this.frame.height / 2);
	},

	/**
	 * Montre le panneau s'il est caché
	 */
	show: function() {
		this.div.style.visibility = 'visible';
	},

	/**
	 * Cache le panneau
	 */
	hide: function() {
		this.div.style.visibility = 'hidden';
	},
	
	/**
	* Pour bloquer le zoom
	*/
	lockZoom: function(){
		this.zoomLock = true;
	},
	/**
	* Pour débloquer le zoom
	*/
	unlockZoom: function(){
		this.zoomLock = false;
	},
	/**
	 * Empêche le drag and drop de l'image
	 */
	lockDrag: function(){
		this.isDraggable = false;
		this.dd.detach();
	},
	/**
	 * Autorise le drag and drop de l'image
	 */
	unlockDrag: function(){
		this.isDraggable = true;
		this.dd.attach();
	},
	///////////////////////////////////////////////////////////////////////////////////
	// Evènements
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Zoom avant depuis le bouton de la toolbar
	 */
	onClickZoomIn: function() {
		this.zoomCenterIn();
	},

	/**
	 * Zoom arrière depuis le bouton de la toolbar
	 */
	onClickZoomOut: function() {
		this.zoomCenterOut();
	},

	/**
	 * Commencement d'une série de zooms avec la molette de la souris
	 */
	startWheelZoom: function() {},

	/**
	 * Méthode appelée lorsque la molette de la souris tourne pour effectuer un zoom
	 * delta (entier relatif) : quantité associée à déplacement angulaire de la molette
	 * left : coordonnée horizontale du pointeur relative au cadre (frame)
	 * top : coordonnée verticale du pointeur relative au cadre
	 */
	onWheelZoom: function(delta, left, top) {
		this.zoomPoint(delta > 0 ? this.ZOOM_IN : this.ZOOM_OUT, left, top);
	},

	/**
	 * Fin de la série de zooms avec la molette de la souris
	 */
	endWheelZoom: function() {},

	/**
	 * Appelé lors d'un zoom par la sélection d'un région.
	 * scale est le facteur d'agrandissement calculé en fonction de la taille du rectangle de sélection
	 * left et top sont les coordonnées du centre du rectangle de sélection relatives au cadre
	 * @param {Float} scale facteur d'agrandissement calculé en fonction de la taille du rectangle de sélection
	 * @param {Float} left coordonnées du centre du rectangle de sélection relatives au cadre
	 * @param {Float} top coordonnées du centre du rectangle de sélection relatives au cadre
	 */
	onSelectZoom: function(scale, left, top) {
		this.zoomPointToCenter(scale, left, top);
	},

	/**
	 * Méthode appelée à la fin d'un drag de l'image principale (pan).
	 */
	endDrag: function() {
		this.updatePos();
		this.viewer.onDrag();
	},

	/**
	 * Méthode appelée une fois l'image chargée
	 * Si le verrou est activé, le panneau ne s'enregistre pas sur l'événement resize du frame
	 */
	onLoad: function() {
		if (this.frame.zoomLock == false) {
			this.fitFrame();
			this.center();
			this.frame.onResizeRegister(this.resizeHandler, true);
		} else {
			this.setPos(this.frame.lockedPos.left, this.frame.lockedPos.top);
			this.setWidth(this.image.getWidth('current') * this.frame.lockedScale);
		}
		this.show();
		this.frame.hideLoading();
		this.viewer.onImageLoaded();
	}
});
