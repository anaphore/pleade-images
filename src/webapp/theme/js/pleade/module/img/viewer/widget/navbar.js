//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Barre de navigation de la visionneuse
 * @author David Bambouché
 * @author Johan Cwiklinski
 * @author Julien Huet
 */
/**
 * @class Classe de la barre de navigation de la visionneuse
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
NavBar = new Class(/** @lends NavBar.prototype */{
	///////////////////////////////////////////////////////////////////////////////////
	// Constantes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Unité de déplacement pour les boutons Backward et Forward
	 * @constant
	 * @type Integer
	 */
	STEP_SIZE: 10,

	///////////////////////////////////////////////////////////////////////////////////
	// Attributs
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Instance de la visionneuse
	 * @private
	 * @type pivViewer
	 */
	viewer: null,

	/**
	 * Instance du bouton première image de la série
	 * @private
	 * @type Element
	 */
	btnMvFirst: null,
	/**
	 * Instance du bouton 10è image précédente de la série
	 * @private
	 * @type Element
	 */
	btnMvBack: null,
	/**
	 * Instance du bouton image précédente de la série
	 * @private
	 * @type Element
	 */
	btnMvPrev: null,
	/**
	 * Instance du bouton image suivante de la série
	 * @private
	 * @type Element
	 */
	btnMvNext: null,
	/**
	 * Instance du bouton 10è image suivante de la série
	 * @private
	 * @type Element
	 */
	btnMvForw: null,
	/**
	 * Instance du bouton dernière image de la série
	 * @private
	 * @type Element
	 */
	btnMvLast: null,

	/**
	 * Conteneur de l'objet en html
	 * @type String
	 */
	html_element: 'piv-body-hmenu-navigation',

	///////////////////////////////////////////////////////////////////////////////////
	// Méthodes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * <strong>Constructor</strong><br/> 
	 * @param {pivViewer} viewer Instance de la visionneuse
	 */
	initialize: function(viewer) {
		this.viewer = viewer;
		$(this.html_element).style.visibility = "visible";

		this.btnMvFirst = $("btn-mv-first");
		this.btnMvBack = $("btn-mv-stepbackward");
		this.btnMvPrev = $("btn-mv-previous");
		this.btnMvNext = $("btn-mv-next");
		this.btnMvForw = $("btn-mv-stepforward");
		this.btnMvLast = $("btn-mv-last");

		this.btnMvFirst.addEvent("click", this.onMvFirst.bind(this)).set('href', '');
		this.btnMvBack.addEvent("click", this.onMvBack.bind(this)).set('href', '');
		this.btnMvPrev.addEvent("click", this.onMvPrev.bind(this)).set('href', '');
		this.btnMvNext.addEvent("click", this.onMvNext.bind(this)).set('href', '');
		this.btnMvForw.addEvent("click", this.onMvForw.bind(this)).set('href', '');
		this.btnMvLast.addEvent("click", this.onMvLast.bind(this)).set('href', '');

		//ajout des tooltips locaux
		this.viewer.tips.attach('#' + this.html_element + ' > a');
	},

	/**
	 * Ajuste les boutons, en les (dés)activant selon le rang de l'image affichée.
	 * @private
	 * @param {Integer} imageNumber Le numéro de l'image que l'on affiche
	 */
	redraw: function(imageNumber) {
		this.btnMvFirst.removeClass('disabled');
		this.btnMvBack.removeClass('disabled');
		this.btnMvPrev.removeClass('disabled');
		this.btnMvNext.removeClass('disabled');
		this.btnMvForw.removeClass('disabled');
		this.btnMvLast.removeClass('disabled');
		if (imageNumber == this.viewer.series.size) {
			this.btnMvNext.addClass('disabled');
			this.btnMvForw.addClass('disabled');
			this.btnMvLast.addClass('disabled');
		} else if (imageNumber == 1) {
			this.btnMvFirst.addClass('disabled');
			this.btnMvBack.addClass('disabled');
			this.btnMvPrev.addClass('disabled');
		}
	},

	///////////////////////////////////////////////////////////////////////////////////
	// Evénements
	///////////////////////////////////////////////////////////////////////////////////
	/**
	 * Action pour voir la première image de la série
	 * @param {Event} event
	 */
	onMvFirst: function(event) {
		if( !this.btnMvFirst.hasClass('disabled') )
			this.viewer.slidebar.setImageNumber(1);
		event.stop();
	},
	/**
	 * Action pour faire un saut en arrière de STEP_SIZE (constante) dans la série d'image
	 * @param {Event} event
	 */
	onMvBack: function(event) {
		if( !this.btnMvBack.hasClass('disabled') )
			this.viewer.slidebar.setImageNumber((this.viewer.series.currentImage.num - this.STEP_SIZE) >= 1 ? (this.viewer.series.currentImage.num - this.STEP_SIZE) : 1);
		event.stop();
	},
	/**
	 * Action pour aller à l'image précedente dans la série
	 * @param {Event} event
	 */
	onMvPrev: function(event) {
		if( !this.btnMvPrev.hasClass('disabled') )
			this.viewer.slidebar.setImageNumber((this.viewer.series.currentImage.num - 1) >= 1 ? (this.viewer.series.currentImage.num - 1) : 1);
		event.stop();
	},
	/**
	 * Action pour aller à l'image suivante dans la série
	 * @param {Event} event
	 */
	onMvNext: function(event) {
		if( !this.btnMvNext.hasClass('disabled') )
			this.viewer.slidebar.setImageNumber(((this.viewer.series.currentImage.num).toInt() + 1) <= this.viewer.series.size ? ((this.viewer.series.currentImage.num).toInt() + 1) : this.viewer.series.size);
		event.stop();
	},
	/**
	 * Action pour faire un saut en avant de STEP_SIZE (constante)dans la série d'image
	 * @param {Event} event
	 */
	onMvForw: function(event) {
		if( !this.btnMvForw.hasClass('disabled') )
			this.viewer.slidebar.setImageNumber(((this.viewer.series.currentImage.num).toInt() + this.STEP_SIZE) <= this.viewer.series.size ? ((this.viewer.series.currentImage.num).toInt() + this.STEP_SIZE) : this.viewer.series.size);
		event.stop();
	},
	/**
	 * Action pour voir la dernière image de la série
	 * @param {Event} event
	 */
	onMvLast: function(event) {
		if( !this.btnMvLast.hasClass('disabled') )
			this.viewer.slidebar.setImageNumber(this.viewer.series.size);
		event.stop();
	}

});
