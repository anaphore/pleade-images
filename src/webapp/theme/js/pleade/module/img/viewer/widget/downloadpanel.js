//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Panneau de téléchargement
 * @author Johan Cwiklinski
 */

/**
 * @class Panneau de téléchargement
 * @author Johan Cwiklinski
 * @augments CWindow
 */
DownloadPanel = new Class(/** @lends DownloadPanel*/{
	Extends: CWindow,

	/** Instance de la visionneuse auquel le menu est rattaché
	* @type pivViewer
	*/
	viewer: null,

	/** L'element du dom contenant le menu
	* @type Element
	*/
	mnuDl: null,

	/**
	* <strong>Constructeur</strong>
	* Conteneur pour le formulaire de téléchargement
	* @param {pivViewer} viewer
	* @pram {Integer} imageCount
	*/
	initialize: function(viewer, imageCount){
		this.viewer = viewer;
		if( $('piv-dl-menu') != null ) {//if we do not have the required element, we do nothing
			this.mnuDl = $('piv-dl-menu');
			this.parent({
				container: this.viewer.frame.div,
				foreground: false,
				background: false,
				draggable: false,
				defaultColor: 'transparent',
				resizable: false,
				enableHeader:false,
				closable: false,
				isSlide: 'vertical',
				slideId: 'piv-dl-menu'
			});
			this.render();
			this.container.adopt(this.mnuDl);
			// FIXME trouver une meilleur facon d'attacher le panneau d'impression.
			this.viewer.frame.div.adopt(this.container);
			this.slide.hide();
			this.mnuDl.removeClass('invisible');
			this.mnuDl.setStyle('white-space','nowrap');
			this.container.setPosition({x: 0, y: 0});

			//action sur l'envoi du formulaire
			$$('#piv-dl-menu form').addEvent('submit', this.onDlFormSubmit.bind(this));
			//image max
			$('txt-download-to').set('value', imageCount);
		}
	},

	/** Surcharge de la méthode de suppresion de la fenêtre (on la masque seulement) */
	destroy: function(){
		this.slide.hide();
		this.container.hide();
		this.viewer.toolbar.btnDl.removeClass('checked');
	},

  /**
  * Validation du formulaire
  * @param {MooTools.Event} event
  */
  onDlFormSubmit: function(event){
    var from = $('txt-download-from').get('value').trim();
    var to = $('txt-download-to').get('value').trim();

    if ( to-from > this.viewer.actives.downloadMax ) {
      alert(_pivMessages.max_images_download.replace(/%i/, this.viewer.actives.downloadMax));
    } else {
      var format = null;
      if( !this.viewer.actives.pdf ) {
        format = 'zip';
      } else if( !this.viewer.actives.zip ) {
        format = 'pdf';
      } else {
        format = $('rad-download-type-pdf').get('checked') ? 'pdf' : 'zip';
      }

      var href = this.viewer.series.baseUrl + '/dir.' + format + '?';
      if ( this.viewer.series.start != null && this.viewer.series.end && this.viewer.series.start == this.viewer.series.end ) {
        href += 'name=' + this.viewer.series.start ;
      } else {
        if ( this.viewer.series.start != null && this.viewer.series.start != '' ) {
          href += 'if=' + this.viewer.series.start;
        }
        if ( this.viewer.series.end != null && this.viewer.series.end != '' ) {
          if ( this.viewer.series.start != '' ) {
            href += '&';
          }
          href += 'il=' + this.viewer.series.end;
        }
        if ( this.viewer.series.start != null && this.viewer.series.end != null && this.viewer.series.start != '' && this.viewer.series.end != '' ) {
          href += '&';
        }
        href += '&p=' + from + '&d=' + to;
      }
      windowManager.winFocus(href, 'pdfserie');
    }
    event.stop();
  },

	/**
	* Affiche le panneau
	*/
	show: function() {
		_sizes = this.mnuDl.getSize();
		this.setHeight(_sizes.y);
		this.setWidth(_sizes.x);
		this.container.setPosition({x: this.viewer.toolbar.btnDl.getPosition().x + this.viewer.toolbar.btnDl.getSize().x - _sizes.x, y: 0});
		this.parent();
	}

});