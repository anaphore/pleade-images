//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id: fragmentinfospanel.js 19480 2010-10-05 12:42:16Z jcwiklinski $

/**
 * @fileOverview Panneau d'informations sur le fragment
 * @author Johan Cwiklinski
 */

/**
 * @class Panneau d'informations sur le fragment
 * @author Johan Cwiklinski
 * @augments CWindow
 */
FragmentInfosPanel = new Class(/** @lends FragmentInfosPanel*/{
  Extends: CWindow,

  /** Instance de la visionneuse auquel le panneau est rattaché
  * @type pivViewer
  */
  viewer: null,

  /** L'element du dom contenant le panneau
  * @type Element
  */
  mnuInfos: null,

  /**
  * <strong>Constructeur</strong>
  * Conteneur pour le panneau d'informations
  * @param {pivViewer} viewer
  */
  initialize: function(viewer, posY){
    this.viewer = viewer;
    if ( $('pnl-fragment-infos') != null ) {//if we do not have the required element, we do nothing
      this.mnuInfos = $('pnl-fragment-infos');
      this.parent({
        container: this.viewer.frame.div,
        foreground: false,
        background: false,
        opacity: this.viewer.transparentBgOpacity,
        onOverOpacity: this.viewer.transparentBgOpacity,
        defaultColor: this.viewer.transparentBgColor,
        depth: 1,
        resizable: false,
        enableHeader:false,
        closable: false,
        draggable: false,
        isSlide: 'vertical',
        slideId: 'pnl-fragment-infos'
      });
      this.render();
      this.container.adopt(this.mnuInfos);
      // FIXME trouver une meilleur facon d'attacher le panneau.
      this.viewer.frame.div.adopt(this.container);
      this.slide.hide();
      this.mnuInfos.removeClass('invisible');
      this.container.setPosition({x: 0, y: $('current_title').getSize().y});
      this.setWidth('25%');
    }
  },

  hide: function() {
    this.parent();
    this.container.hide();
  },

  show: function() {
    this.container.show();
    this.parent();
  }

});