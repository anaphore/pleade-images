//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
* @fileOverview Barre d'outils de la visionneuse
* @author David Bambouché
* @author Johan Cwiklinski
* @author Julien Huet
 */
/**
* @class Gestion de la boite d'outil
* @author David Bambouché
* @author Johan Cwiklinski
* @author Julien Huet
* @augments Mootools.Log
*/
ToolBar = new Class(/** @lends ToolBar.prototype */{
	///////////////////////////////////////////////////////////////////////////////////
	// Attributs
	///////////////////////////////////////////////////////////////////////////////////

	/**
	* Instance de la visionneuse
	* @private
	* @type pivViewer
	*/
	viewer: null,

	/**
	* Id de l'élément HTML qui contient la barre
	* @type String
	*/
	html_element: 'piv-body-main-lcol',
	
	/**
	* Booleen, flag pour savoir si l'overview est affiché 
	* @type Boolean
	*/
	overviewIsHidden: true,
	
	/**
	* Booleen, flag pour savoir si le panneau d'impression est affiché 
	* @type Boolean
	*/
	printPanelIsHidden: true,
	
	/**
	* Booleen, flag pour savoir si le panneau de téléchargement est affiché 
	* @type Boolean
	*/
	dlPanelIsHidden: true,
	
	/**
	* Instance du bouton de drag and drop de l'image
	* @type Element
	*/
	btnPan: null,
	/**
	* Instance du bouton de selection de version
	* @type Element
	*/
	btnVersions: null,
	/**
	* Instance du bouton de zoom avant
	* @type Element
	*/
	btnZoomIn: null,
	/**
	* Instance du bouton de zoom arrière
	* @type Element
	*/
	btnZoomOut: null,
	/**
	* Instance du bouton de l'outil de zoom par selection
	* @type Element
	*/
	btnZoomSelect: null,
	/**
	* Instance du bouton verrouillage du zoom
	* @type Element
	*/
	btnLockZoom: null,
	/**
	* Instance du bouton d'affichage de la veritable taille de l'image
	* @type Element
	*/
	btnRealSize: null,
	/**
	* Instance du bouton de plein ecran de l'image
	* @type Element
	*/
	btnFitScreen: null,
	/**
	* Instance du bouton pour afficher ou cacher l'overview
	* @type Element
	*/
	btnOverview: null,
	/**
	* Instance du bouton pour afficher ou cacher le panneau d'impression
	* @type Element
	*/
	btnPrint: null,
	/**
	* Instance du bouton pour afficher ou cacher le panneau de téléchargement
	* @type Element
	*/
	btnDl: null,
	/**
	* Instance du bouton pour effectuer une rotation sur l'image
	* @type Element
	*/
	btnRotate: null,
	/**
	* Instance du bouton pour afficher ou cacher la grille de l'image
	* @type Element
	*/
	btnGrid: null,

	/**
	* Menu des versions
	*/
	mnuVersions: null,

	///////////////////////////////////////////////////////////////////////////////////
	// Méthodes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	* <strong>Constructeur</strong>
	* @param {pivViewer} viewer Instance de la visionneuse
	*/
	initialize: function(viewer) {
		this.viewer = viewer;
		windowManager.initMenuBox();
		this.initButtons();
		this.overviewIsHidden = true;
		this.printPanelIsHidden = true;
		this.dlPanelIsHidden = true;
		$$('a.persistent').addEvent('click', this.setSelected.bind(this), this);
	},

	/**
	* Selectionne un bouton lorsqu'on clique dessus
	* @param {MooTools.Event} e
	*/
	setSelected: function(e){
		var _this = $(e.target);
		if( !_this.hasClass('disabled') ){
			$$('#' + this.html_element + ' a.checked:not(.persistent)').each(function(item){
				item.removeClass('checked');
			});
			if ( _this.hasClass('persistent') && _this.hasClass('checked') ) {
				_this.removeClass('checked');
			} else if ( _this.hasClass('checkable') || _this.hasClass('persistent')  ) {
				if ( _this.hasClass('group_1') && !_this.hasClass('checked') || !_this.hasClass('group_1')) {
					if ( _this.hasClass('group_1') ) {
						//remove 'checked' on each other 'group_1' members
						$$('.group_1').each(function(item){
							item.removeClass('checked');
						});
					}
					_this.addClass('checked');
				}
			}
		}
	},

	///////////////////////////////////////////////////////////////////////////////////
	// Fonctions d'intilisation des boutons
	///////////////////////////////////////////////////////////////////////////////////

	/**
	* Initialisation de tous les boutons
	*/
	initButtons: function(){
		this.btnPan = $('btn-pan');
		if ( this.btnPan ) {
			this.btnPan.addEvent('click', this.onBtnPanClick.bindWithEvent(this));
		}
		
		this.btnVersions = $('pnl-status-version');
		if ( this.btnVersions ) {
			this.btnVersions.addEvent('click', this.onBtnVersionsClick.bindWithEvent(this));
		}

		if ( this.viewer.actives.zoom ) {
			//Définition des boutons
			this.btnZoomIn = $('btn-zoomin');
			this.btnZoomOut = $('btn-zoomout');
			this.btnZoomSelect = $('btn-zoomselect');
			// souscription aux événements
			if ( this.btnZoomIn ) {
				this.btnZoomIn.addEvent('click', this.onBtnZoomInClick.bindWithEvent(this));
			}
			if ( this.btnZoomOut ) {
				this.btnZoomOut.addEvent('click', this.onBtnZoomOutClick.bindWithEvent(this));
			}
			if ( this.btnZoomSelect ) {
				this.btnZoomSelect.addEvent('click', this.onBtnZoomSelectClick.bindWithEvent(this));
			}
		}

		if ( this.viewer.actives.lock ) {
			this.btnLockZoom = $('btn-lockzoom');
			if ( this.btnLockZoom ) {
				this.btnLockZoom.addEvent('click', this.onBtnLockZoomClick.bindWithEvent(this));
			}
		}
		
		if ( this.viewer.actives.resize ) {
			this.btnRealSize = $('btn-realsize');
			this.btnFitScreen = $('btn-fitscreen');
			// souscription aux événements
			if ( this.btnRealSize ) {
				this.btnRealSize.addEvent('click', this.onBtnRealSizeClick.bindWithEvent(this));
			}
			if ( this.btnFitScreen ) {
				this.btnFitScreen.addEvent('click', this.onBtnFitScreenClick.bindWithEvent(this));
			}
		}

		if ( this.viewer.actives.preview ) {
			this.btnOverview = $('btn-overview');
			if ( this.btnOverview ) {
				this.btnOverview.addEvent('click', this.onBtnOverviewClick.bindWithEvent(this));
			}
		}

		if ( this.viewer.actives.print ) {
			this.btnPrint = $('btn-print');
			if ( this.btnPrint ) {
				this.btnPrint.addEvent('click', this.onBtnPrintClick.bindWithEvent(this));
			}
		}

		if ( this.viewer.actives.pdf || this.viewer.actives.zip ) {
			this.btnDl = $('btn-dl');
			if ( this.btnDl ) {
				this.btnDl.addEvent('click', this.onBtnDlClick.bindWithEvent(this));
			}
		}

 		if ( this.viewer.actives.rotation ) {
			this.btnRotate = $('btn-rotate');
			if ( this.btnRotate ) {
				this.btnRotate.addEvent('click', this.onBtnRotateClick.bindWithEvent(this));
			}
 		}

		if ( this.viewer.actives.grid ) {
			this.btnGrid = $('btn-grid');
			if ( this.btnGrid ) {
				this.btnGrid.addEvent('click', this.onBtnGridClick.bindWithEvent(this));
			}
		}

    if ( this.viewer.actives.navigationCotes ) {
      //initialisation des boutons poru la navigation par cote
      //les données sont initialisées ailleurs, lorsque l'image est chargée
      this.btnPrevCote = $('cote-previous');
      this.btnNextCote = $('cote-next');
      /*if ( this.btnPrevCote ) {
        this.btnPrevCote.addEvent('click', this.onBtnPrevCoteClick.bindWithEvent(this));
      }
      if ( this.btnNextCote ) {
        this.btnNextCote.addEvent('click', this.onBtnNextCoteClick.bindWithEvent(this));
      }*/
    }
	},

	///////////////////////////////////////////////////////////////////////////////////
	// Evénements
	///////////////////////////////////////////////////////////////////////////////////
	/**
	* Fonction appellée lors du click sur le bouton PAN
	**/
	onBtnPanClick: function(){
		if ( !this.btnPan.hasClass('disabled') ) {
			this.viewer.frame.setDraggable();
		}
	},

	/**
	* Fonction appellée lors du click sur le bouton version
	**/
	onBtnVersionsClick: function(){
		if ( !this.btnVersions.hasClass('disabled') ) {
			if( this.mnuVersions != null ) {
				//slide.toggle would be nice... but does not work here
				if ( this.mnuVersions.slide.open ) {
					this.mnuVersions.slide.slideOut();
				} else {
					this.mnuVersions.slide.slideIn();
				}
			} else {
				this.loadVersions();
			}
		}
	},

	/**
	* Fonction appellée lors du click sur le bouton zoom in
	**/
	onBtnZoomInClick: function(){
		if ( !this.btnZoomIn.hasClass('disabled') ) {
			this.viewer.onClickZoomIn();
		}
	},

	/**
	* Fonction appellée lors du click sur le bouton zoom out
	**/
	onBtnZoomOutClick: function(){
		if ( !this.btnZoomOut.hasClass('disabled') ) {
			this.viewer.onClickZoomOut();
		}
	},

	/**
	* Fonction appellée lors du click sur le bouton de zoom par selection
	**/
	onBtnZoomSelectClick: function(){
		if ( !this.btnZoomSelect.hasClass('disabled') ) {
			if ( !this.viewer.frame.zoom.isActive ) {
				this.viewer.frame.setZoomable();
			} else {
				this.viewer.frame.setDraggable();
			}
		}
	},

	/**
	* Fonction appellée lors du click sur le bouton de lock du zoom
	**/
	onBtnLockZoomClick: function(){
		if ( !this.btnLockZoom.hasClass('disabled') ) {
			this.toggleZoomLock();
		}
	},

	/**
	* Fonction appellée lors du click sur le bouton de rotation
	**/
	onBtnRotateClick: function(){
		if ( !this.btnRotate.hasClass('disabled') ) {
			this.viewer.onClickRotate();
		}
	},

	/**
	* Fonction appellée lors du click sur le bouton pour afficher la grille
	**/
	onBtnGridClick: function(){
		if ( !this.btnGrid.hasClass('disabled') ) {
			this.createGrid();
		}
	},

  onBtnPrevCoteClick: function(e){
    e.stop();
    console.log('previous cote');
  },

  onBtnNextCoteClick: function(e){
    e.stop();
    console.log('next cote');
  },

	/**
	* Fonction appellée lors du click sur le bouton pour obtenir la veritable taille de l'image
	**/
	onBtnRealSizeClick: function(e){
		if ( !this.btnRealSize.hasClass('disabled') ) {
			this.viewer.onClickRealSize(e);
		}
	},

	/**
	* Fonction appellée lors du click sur le bouton pour ajuster la taille de l'image
	**/
	onBtnFitScreenClick: function(e){
		if ( !this.btnFitScreen.hasClass('disabled') ) {
			this.viewer.onClickFitFrame(e);
		}
	},

	/**
	* Fonction appellée lors du click sur le bouton d'affichage de l'apercu
	**/
	onBtnOverviewClick: function(e){
		if ( !this.btnOverview.hasClass('disabled') ) {
			this.toggleOverview();
		}
	},

	/**
	* Fonction appellée lors du click sur le bouton d'impression
	**/
	onBtnPrintClick: function(){
		if ( !this.btnPrint.hasClass('disabled') ) {
			this.togglePrintPanel();
		}
	},

	/**
	* Fonction appellée lors du click sur le bouton de téléchargement
	**/
	onBtnDlClick: function(){
		if ( !this.btnPrint.hasClass('disabled') ) {
			this.toggleDlPanel();
		}
	},
	
	/**
	* Initialise le menu des versions.
	*/
	loadVersions: function() {
		_image = this.viewer.series.currentImage;

		var _ul = new Element('ul');
		
		var _mnuVersions = new Element('div', { id: 'piv-version-menu' }).adopt(_ul);

		var items = [];
		for ( i = 0; i < _image.versions.length; i++ ) {
			_li = new Element('li', {
				id: 'version_' + i,
				html: '<a href="#">' + _image.versions[i].summary + '</a>',
				events: {
					click: this.onVersionClick.bind(this)
				}
			});
			if (_image.versions[i] == _image.currentVersion)
				_li.addClass('checked')
			_ul.adopt(_li);
		}

		$('piv-body-main-mcol').adopt(_mnuVersions);

		//set position
		_mnuVersions.setStyles({
			top: 0,
			left: this.btnVersions.getPosition().x + this.btnVersions.getSize().x - _mnuVersions.getSize().x
		});

		this.mnuVersions = new CWindow({
			container:this.viewer.frame.div,
			defaultColor: 'transparent',
			resizable: false,
			enableHeader:false,
			closable: false,
			draggable: false,
			isSlide: 'vertical',
			slideId: 'piv-version-menu'
		});
		this.mnuVersions.render();
		this.mnuVersions.container.adopt(_mnuVersions);
		this.viewer.frame.div.adopt(this.mnuVersions.container);

    // IE n'aime pas le position:absolute sur cet élément, il ne s'affiche pas
    if ( Browser.Engine.trident ) {
      this.mnuVersions.container.setStyle('position', '');
    }

	},

	/**
	* Creation d'un panneau sur l'image
	* @return {Element} Le panneau sur l'image
	*/
	createPanel: function(){
		var pnl = new CWindow({
			container:this.viewer.frame.div,
			defaultColor: '#222',
			resizable: false,
			draggable: false,
			enableHeader: false,
			closable: false,
			windowName: 'piv-version-tooltip',
			opacity:0.8,
			onOverOpacity: 0.8
		});

		pnl.render();

		$('piv-body').adopt(pnl.container);
		_place = $('pnl-status-version');

		pnl.container.adopt(
			new Element('div', {
				html: _pivMessages.mnu_versions_tooltip,
				styles: {
					width: 250
				}
			})
		);

		pnl.container.setStyles({
			top: _place.getPosition('body').y + 27,
			left: _place.getPosition('body').x + _place.getWidth() - $('piv-version-tooltip').getWidth()
		});

		pnl.container.addEvent('click', function(){
			this.versionToolTip.hide();
			if ( !this.mnuVersions || !this.mnuVersions.slide.open ) {
				this.onBtnVersionsClick();
			}
		}.bindWithEvent(this));
		return pnl;
	},

	///////////////////////////////////////////////////////////////////////////////////
	// Evénements
	///////////////////////////////////////////////////////////////////////////////////

	/**
	* Méthode appelée lorsqu'il y a un clic sur un item du menu des versions.
	* @param {Mootools.Event}
	*/
	onVersionClick: function(e) {
		var _this = $(e.target);
		var _id = _this.getParent().get('id');
		var _index = _id.substr(_id.indexOf('_')+1, _id.length);
		this.mnuVersions.slide.slideOut();
		this.mnuVersions.destroy();
		this.mnuVersions = null;
		if ( this.viewer.actives.grid ) {
			if ( this.btnGrid.hasClass('checked') ) {
				this.btnGrid.removeClass('checked');
				this.btnGrid.removeClass('disabled');
				this.btnGrid.addEvent('click', this.onBtnGridClick.bindWithEvent(this));
			}
		}
		this.viewer.displayVersion(_index);
		e.stop();
	},
	
	/**
	* Creation de la grille sur l'image
	*/
	createGrid: function() {
		if( !this.btnGrid.hasClass('disabled') ) {
			this.toggleGridButtons();
			if ( this.viewer.gridActive ) {
				this.viewer.hideGrid();
			} else {
				this.viewer.showGrid();
			}
		}
	},

	toggleGridButtons: function(){
		if (this.btnPan ) {
			this.btnPan.toggleClass('disabled');
			if( !this.btnPan.hasClass('disabled') ) {
				this.btnPan.addClass('checked');
				this.btnGrid.removeClass('checked');
			}
		}
		if (this.btnVersions ) {
			this.btnVersions.toggleClass('disabled');
		}
		if (this.btnZoomIn ) {
			this.btnZoomIn.toggleClass('disabled');
		}
		if (this.btnZoomOut ) {
			this.btnZoomOut.toggleClass('disabled');
		}
		if (this.btnZoomSelect ) {
			this.btnZoomSelect.toggleClass('disabled');
		}
		if (this.btnLockZoom ) {
			this.btnLockZoom.toggleClass('disabled');
		}
		if (this.btnRealSize ) {
			this.btnRealSize.toggleClass('disabled');
		}
		if (this.btnFitScreen ) {
			this.btnFitScreen.toggleClass('disabled');
		}
		if ( this.btnOverview ) {
			this.btnOverview.toggleClass('disabled');
		}
		if ( this.btnPrint ) {
			this.btnPrint.toggleClass('disabled');
		}
		if ( this.btnRotate ) {
			this.btnRotate.toggleClass('disabled');
		}
	},

	/**
	* Creation d'un tooltip afin d'avertir l'utilisateur qu'il existe une autre version de l'image.
	*/
	createVersionsToolTip: function(){
		if ( !this.versionToolTip ) {
			this.versionToolTip = this.createPanel();
		} else {
			this.versionToolTip.show();
		}
	},

	/**
	* Met à jour la grid sur l'image
	* @param {Image} image
	*/
	update: function(image){
		if ( this.viewer.actives.grid ) {
			this.btnGrid.removeClass('checked');
			if( image.grid ) {
				this.btnGrid.removeClass('disabled');
			} else {
				this.btnGrid.addClass('disabled');
			}
		}
	},

	///////////////////////////////////////////////////////////////////////////////////
	// Evénements
	///////////////////////////////////////////////////////////////////////////////////

	/**
	* Affiche et masque le panneau d'aperçu
	*/
	toggleOverview: function() {
		if ( this.overviewIsHidden ) {
			this.overviewIsHidden = false;
			this.viewer.showOverview();
		} else {
			this.overviewIsHidden = true;
			this.viewer.hideOverview();
		}
	},

	/**
	* Affiche et masque le panneau d'impression
	*/
	togglePrintPanel: function() {
		if ( this.printPanelIsHidden ) {
			this.printPanelIsHidden = false;
			if ( !this.dlPanelIsHidden ) {
				this.dlPanelIsHidden = true;
				this.viewer.downloadPanel.slide.hide();
			}
			this.viewer.showPrintPanel();
		} else {
			this.printPanelIsHidden = true;
			this.viewer.hidePrintPanel();
		}
	},

	/**
	* Affiche et masque le panneau de téléchargement
	*/
	toggleDlPanel: function() {
		if ( this.dlPanelIsHidden ) {
			this.dlPanelIsHidden = false;
			if ( !this.printPanelIsHidden ) {
				this.printPanelIsHidden = true;
				this.viewer.printPanel.slide.hide();
			}
			this.viewer.showDlPanel();
		} else {
			this.dlPanelIsHidden = true;
			this.viewer.hideDlPanel();
		}
	},

	/**
	* Active ou désactive le blocage du zoom
	*/
	toggleZoomLock: function() {
		if ( !this.btnLockZoom.hasClass('checked') ) {
			this.viewer.lockZoom();
		} else {
			this.viewer.unlockZoom();
		}
	}

});
