//Copyright (C) 2003-2011 AJLSM
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview SlideBar de la visionneuse
 * @author Julien Huet
 * @author Johan Cwiklinski
 */

/**
 * @class Classe du slideBar de la visionneuse
 * @author Julien Huet
 * @author Johan Cwiklinski
 * @augments Mootools.Log
 */
SlideBar = new Class(/** @lends  SlideBar.prototype */{
	///////////////////////////////////////////////////////////////////////////////////
	// Constantes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * L'id du div qui contient le background
	 * @constant
	 * @type String
	 */
	BACKGROUND_ID: 'sld-imageselect-bg',

	/**
	 * L'id du curseur
	 * @constant
	 * @type String
	 */
	THUMB_ID: 'sld-imageselect-hdl',

	/**
	 * Si vrai, le curseur passera par toutes les positions intermédiaires du slider
	 * pour aller d'un point à un autre; à éviter car provoque des rafales de onSlideEnd.
	 * @constant
	 * @type Boolean
	 */
	ANIMATE: false,

	///////////////////////////////////////////////////////////////////////////////////
	// Attributs
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Référence vers la classe principale
	 * @type pivViewer
	 */
	viewer: null,

	/**
	 * Le numéro de l'image sélectionné
	 * @type Integer
	 */
	imageNumber: 1,

	/**
	 * The slider element
	 * @type Moootols.Slider
	 */
	slider: null,

	/**
	 * Nombre de pixels par image le long du curseur
	 * @type Integer
	 */
	pixelsPerImage: 0,

	/**
	 * Element input contenant le numéro de l'image
	 * @type Element
	 */
	selectInput: null,

	/**
	 * Span contenant le nombre d'images
	 * @type Element
	 */
	spanImageCount: null,

	/**
	 * Flag pour savoir si le changement est dû à une action de l'utilisateur
	 * @type Boolean
	 */
	extChanged: false,

	///////////////////////////////////////////////////////////////////////////////////
	// Méthodes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * <strong>Constructeur</strong> Création d'un slider pour la sélection d'une image.
	 * @param {pivViewer} viewer Instance de la visionneuse
	 * @param {Integer} imageCount Le nombre d'images dans la série
	 */
	initialize: function(viewer, imageCount) {
		this.viewer = viewer;

		this.selectInput = $('txt-imageselect');
		this.spanImageCount = $('span-imageselect-last');

		this.slider = new Slider(this.BACKGROUND_ID, this.THUMB_ID, {
			steps: imageCount,
			range: [1, imageCount],
      initialStep: 1,
			onChange: this.onChange.bind(this),
			onComplete: this.onSlideEnd.bind(this)
			//wheel: true //mousewheel is cool, but will throw onComplete each time :/
		});

		this.spanImageCount.set('text', imageCount);
		this.initTxtSelect();

		//ajout des tooltips locaux
		this.viewer.tips.attach('#sld-imageselect-bg, #txt-imageselect');
	},

	/**
	* Initialisation du sélecteur numérique d'image
	* Rend le span éditable
	*/
	initTxtSelect: function(){
		el = this.selectInput;
		el.addEvent('click', function() {
			if( !el.hasClass('in-edit') ) {
				el.addClass('in-edit');//un flag, on est en cours d'édition
				//store "before" value
				var before = el.get('html').trim();
				//erase current
				el.set('html', '');
				//replace current text/content with input element
				var _input = new Element('input', { 'class':'piv-txt-input', 'value':before });
				_input.inject(el).select();
				//add blur event to input
				_input.addEvent('keydown',  function(event) {
					if ( event.key == 'enter' ) {
						if ( parseInt(_input.get('value').trim()) ) {
							//get value, place it in original element
							val =  _input.get('value').trim();
							el.set('text', val);
							this.setImageNumber(val);
							el.removeClass('in-edit');
						} else {
							alert(_pivMessages.number_required);
							//event.stop();
						}
					}
				}.bind(this));
			}
		}.bind(this));
	},

	/**
	 * Initialisation du slider sur une image particulière
	 * @param {Integer} imageNumber Le numéro de l'image
	 */
	initImageNumber: function(imageNumber) {
		//set a flag, we are not changing value, but simply receving it
		this.setImageNumber(imageNumber);
	},

	/**
	* Renvoie la position actuelle du slider
	* @return {Integer}
	*/
	getCurrentPos: function(){
		return this.slider.step;
	},

	/**
	 * Fixe le numéro de l'image
	 * @param {Integer} imageNumber Le numéro de l'image
	 */
	setImageNumber: function(imageNumber) {
		this.imageNumber = imageNumber;
		this.slider.set(this.imageNumber);
		this.selectInput.set('text', imageNumber);
		this.extChanged = false;
	},
	
	///////////////////////////////////////////////////////////////////////////////////
	// Evénements
	///////////////////////////////////////////////////////////////////////////////////

	/**
	 * Evénement déclenché après soumission d'un numéro dans le input
	 */
	onInput: function() {
		if(parseInt(this.selectInput.value)){
			this.setImageNumber(this.selectInput.value);
		}else{
			alert(_pivMessages.number_required);
		}
	},

	/**
	 * Evénement déclenché après soumission d'un numéro dans le input via la touche entrée
	 * @param {MooTools.Event} event
	 */
	onEnter: function(event) {
		if( event.key == 'enter') {
			if(parseInt(this.selectInput.value)){
				this.setImageNumber(this.selectInput.value);
			}else{
				alert(_pivMessages.number_required);
			}
			event.stop();
			return false;
		}
		return true;
	},

	/**
	 * Evénement déclenché par le déplacement du curseur
	 * @param {Integer} value le numéro de l'image
	 */
	onChange: function(value) {
		this.imageNumber = value;
		this.selectInput.set('text', this.imageNumber);
	},

	/**
	 * Met à jour l'image du viewer
	 * @param {Integer} value le numéro de l'image à charger
	 */
	onSlideEnd: function(value) {
		if( !this.extChanged ) {
			this.viewer.displayImage(parseInt(value));
		} else {
			this.extChanged = false;
		}
	}

});
