//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Création de fenêtre paramétables
 * @author Julien Huet
 * @author Johan Cwiklinski
 */
/**
 * @class Création de fenêtre paramétrables
 * @author Julien Huet
 * @author Johan Cwiklinski
 * @augments MooTools.Log
 * @augments MooTools.Options
 */
var CWindow = new Class(/** @lends CWindow.prototype */{
	Implements: [Options],
	/**
	* Container de la fenêtre
	* @type Element
	*/
	container: null,
	/**
	* Outil de redimensionnement de la fenêtre
	* @type Element
	*/
	resizeTool: null,
	/**
	* Outil de drag and drop de la fenêtre
	* @type Element
	*/
	ddTool: null,
	/**
	* Outil de suppression de la fenêtre
	* @type Element
	*/
	supTool: null,
	/**
	* Outil pour passer la fenêtre au premier plan
	* @type Element
	*/
	fgTool: null,
	/**
	* Outil pour passer la fenêtre au second plan
	* @type Element
	*/
	bgTool: null,
	/**
	* Taille des outils en px
	* Par défaut, les icônes font 16x16 pixels
	* @type Integer
	*/
	toolSize: 16,
	/** Identifiant unique du panneau 
	* @type String
	*/
	id: null,
	
	/**
	 * Largeur de la fenêtre
	 * @type {Integer}
	 */
	width: 0,
	/**
	 * Hauteur de la fenêtre
	 * @type {Integer}
	 */
	height: 0,

	slide: null,

	slideId: null,
	
	/**
	* Options à l'initialisation
	* @type Object
	*/
	options: {
		enableHeader: true,
		title: null,
		autoHideHeader: false,
		resizable: true,
		draggable: true,
		/**
		* Suppression des limites pour les possibilités de drag
		* Passer à true pour supprimer la limte
		* @type Boolean
		*/
		unRestrictDrag: false,
		restrictDragTo: null,
		closable: true,
		foreground: false,
		background: false,
		opacity: 0.8,
		onOverOpacity: 1,
		defaultColor: 'white',
		container: null,
		minWidth: 20,
		minHeight: 20,
		klass: null,
		showOnLoad: false,
		autoHideTools: false,
		allowMultiple: false,
		windowName: null,
		isSlide: false, //false|horizontal|vertical
		slideId: null, //id for the slider (required if isSlide != false !)
    depth: 10, //z-index
		i18n: {
			close: {
				title: _pivMessages.cwindow_close_panel_title,
				text: _pivMessages.cwindow_close_panel
			},
			resize: {
				title: _pivMessages.cwindow_resize_panel_title,
				text: _pivMessages.cwindow_resize_panel
			},
			drag: {
				title: _pivMessages.cwindow_drag_panel_title,
				text: _pivMessages.cwindow_drag_panel
			},
			foreground: {
				title: _pivMessages.cwindow_foreground_panel_title,
				text: _pivMessages.cwindow_foreground_panel
			},
			background: {
				title: _pivMessages.cwindow_background_panel_title,
				text: _pivMessages.cwindow_background_panel
			}
		}
	},

	/**
	* <strong>Constructeur</strong>
	* Création de fenêtres comprennant des boutons de :
	* <ul>
	* <li>fermeture</li>
	* <li>redimension</li>
	* <li>deplacement</li>
	* <li>position (background, foreground)</li>
	* </ul>
	* @param {MooTools.Options} options les options d'initialisation (voir <a href="#options">les options</a>)
	*/
	//FIXME: détailler les options, cf carousel
	initialize: function(options){
		//Recuperation des options
		this.options = $merge(this.options, options);
		this.firstDrag = true;
	},

	/** Création des éléments nécessaires a la fenètre */
	render: function(){
		this.nbToolsBottomLeft = 0;
		this.nbToolsBottomRight = 0;
		this.nbToolsTopLeft= 0;
		this.nbToolsTopRight = 0;
		
		//elements de la zone de commentaire
		this.container = new Element('div', {
			styles: {
				'z-index': this.options.depth,
				'background-color': this.options.defaultColor,
				'position': 'absolute',
				'opacity': this.options.opacity
			}
		});

		if( this.options.isSlide === false ) {
			this.container.setStyle('visibility', (this.options.showOnLoad) ? 'visible' : 'hidden');
			this.isVisible = this.options.showOnLoad;
		}

		if ( this.options.windowName != null ) {
			this.container.set( ((this.options.allowMultiple) ? 'class' : 'id'), this.options.windowName);
		}

		if ( this.options.klass != null ) {
			this.container.addClass( this.options.klass );
		}
		
		if ( this.options.enableHeader ) {
			this.header = new Element('div', {
				'class':  'panelHeader',
				styles: {
					height: this.toolSize + 2 //adding 2px to center vertically tools in the header
				}
			});
			this.container.adopt(this.header);
			if ( this.options.title ) {
				//this.header.set('text', '<span>' + this.options.title + '</span>');
				titleSpan = new Element(
					'span', {
						text: this.options.title,
						'class': 'title'
					}
				);
				this.header.grab(titleSpan, 'top');
			}
		}
		
		/** Creation des outils pour la fenêtre*/
		if ( this.options.resizable ) {
			this.resizeTool = new Element(
				'div', {
					'class': 'piv-resize-panel',
					text: this.options.i18n.resize.text,
					title: this.options.i18n.resize.title,
					styles: {
						cursor: 'se-resize'
					}
				}
			);
			this.addTool(this.resizeTool,'bottom','right');
			/** Resize options */
			this.resizableOptions = {
				limit: {x: [this.options.minWidth], y:[this.options.minHeight]},
				container: this.options.container,
				handle: this.resizeTool,
				onStart: this.onReiszeStart.bind(this),
				onComplete: this.onResizeEnd.bind(this)
			};
			this.resizable = this.container.makeResizable(this.resizableOptions);
			// C'est l'utilitaire de resize qui est le plus important donc on le met avec un z-index legerement suprérieur pour qu'il soit afficher dans tous les cas
			this.resizeTool.setStyle('z-index', parseInt(this.resizeTool.getStyle('z-index')) + 1);
		}

		if ( this.options.draggable ) {
			if ( !this.options.enableHeader ) {
				this.ddTool = new Element(
					'div', {
						'class': 'piv-drag-panel',
						text: this.options.i18n.drag.text,
						title: this.options.i18n.drag.title,
						styles: {
							cursor: 'move'
						}
					}
				);
				this.addTool(this.ddTool, 'top', 'left');
				/** Draggable options */
				handler = this.ddTool;
			} else {
				this.header.setStyle('cursor', 'move');
				this.ddTool = this.header;
			}

			var _restrict = ((this.options.unRestrictDrag === true) ? false : (this.options.restrictDragTo) ? this.options.restrictDragTo : this.options.container);
			this.draggableOptions = {
				container: _restrict,
				handle: this.ddTool,
				drag: this.onDragStart.bind(this),
				onStart: this.onDragStart.bind(this),
				onComplete: this.onDragEnd.bind(this)
			};	
			this.container.makeDraggable(this.draggableOptions); 
		}

		if ( this.options.closable ) {
			this.supTool = new Element(
				'div', {
					'class': 'piv-close-panel',
					html: this.options.i18n.close.text,
					title: this.options.i18n.close.title,
					styles: {
						cursor: 'pointer'
					}
				}
			);
			this.addTool(this.supTool, 'top', 'right');
			this.supTool.addEvent('click', this.destroy.bind(this));
		}

		if ( this.options.foreground ) {
			this.fgTool = new Element(
				'div', {
					'class': 'piv-foreground-panel',
					html: this.options.i18n.foreground.text,
					title: this.options.i18n.foreground.title,
					styles: {
						cursor: 'pointer'
					}
				}
			);
			this.addTool(this.fgTool, 'top', 'right');
			this.fgTool.addEvent('click', this.onForegroundClick.bind(this));
		}

		if ( this.options.background ) {
			this.bgTool = new Element(
				'div', {
					'class': 'piv-background-panel',
					html: this.options.i18n.background.text,
					title: this.options.i18n.background.title,
					styles: {
						cursor: 'pointer'
					}
				}
			);
			this.addTool(this.bgTool, 'top', 'right');
			this.bgTool.addEvent('click', this.onBackgroundClick.bind(this));
		}

		if( this.options.autoHideTools ) {
			this.hideTools();
		}

		if( this.options.isSlide !== false && this.options.slideId != null ) {
			this.slide = new Fx.Slide($(this.options.slideId));
		}

		// Ajout des evenements liés a la zone de commentaire
		this.container.addEvent('mousedown', this.onMousedown.bind(this));
		this.container.addEvent('mouseup', this.onMouseup.bind(this));
		this.container.addEvent('mouseover', this.onMouseover.bind(this));
		this.container.addEvent('mouseout', this.onMouseout.bind(this));
		this.container.addEvent('mousedown', this.onClick.bind(this));
	},
	/**
	* Action lors du drag de la fenêtre
	*/
	onDrag: function(){
		this.enableLog().log('Start draging ' + this.options.windowName);
	},
	/**
	* Action lors de la fermeture de la fenêtre
	*/
	close: function(){
		this.onClose();
	},

	/** Destruction du panneau*/
	destroy: function(){
		this.container.dispose();
	},

	/**
	* Ajout des boutons au panneau
	* @param {Element} tool L'element à ajouter au paneau
	* @param {String} vPosition Position verticale par rapport a la fenêtre, soit top ou bottom
	* @param {String} hPosition Position horizontale par rapport à la fenêtre, soit left soit right
	**/
	addTool: function(tool, vPostion, hPosition){
		var leftPosition = 0;
		var topPosition = 0;
		var sideMargin = 0;
		var topMargin = 0;
		if ( vPostion == 'bottom' ) {
			topPosition = '100%';
			if ( hPosition == 'right' ) { //en bas a droite : bottom right
				this.nbToolsBottomRight++;
				leftPosition = '100%';
				topMargin =  -this.toolSize;
				sideMargin = (this.nbToolsBottomRight-1) * this.toolSize;
			} else { // en bas a gauche : bottom left
				sideMargin = (this.nbToolsBottomLeft * this.toolSize);
				this.nbToolsBottomLeft++;
			}
		}else{
			if ( hPosition == 'right' ) { // en haut a droite: top right
				this.nbToolsTopRight++;
				leftPosition = '100%';
				sideMargin = (this.nbToolsTopRight-1) * this.toolSize;
			} else { // en haut a gauche : top left
				sideMargin = (this.nbToolsTopLeft * this.toolSize);
				this.nbToolsTopLeft++;
			}
		}

		tool.setStyles({
			width: this.toolSize,
			height: this.toolSize,
			top: (this.options.enableHeader) ? topPosition + 1 : topPosition,
			'margin-top': topMargin,
			'z-index': 100,
			position: 'absolute'
		});

		if ( leftPosition === '100%' ) {
			tool.setStyles({
				right: (this.options.enableHeader) ? '1px' : 0,
				'margin-right': sideMargin 
			});
		} else {
			tool.setStyles({
				left: leftPosition,
				'margin-left': sideMargin
			});
		}

		if ( vPostion == 'top' ) {
			if ( this.options.enableHeader ) {
				this.header.adopt(tool);
			} else {
				this.container.adopt(tool);
			}
		} else {
			this.container.adopt(tool);
		}
	},

	/** Affiche les outils sur le panneau*/
	showTools: function(){
		if ( this.isVisible ) {
			this.container.setStyle('cursor', 'default');
			if ( this.resizeTool ) {
				this.resizeTool.show();
			}
			if ( this.ddTool ) {
				this.ddTool.show();
			}
			if ( this.fgTool ) {
				this.fgTool.show();
			}
			if ( this.bgTool ) {
				this.bgTool.show();
			}
			if ( this.supTool ) {
				this.supTool.show();
			}
		}
	},

	/** Cache les outils sur le panneau*/
	hideTools: function(){
		if ( this.resizeTool ) {
			this.resizeTool.hide();
		}
		if ( this.ddTool ) {
			this.ddTool.hide();
		}
		if ( this.fgTool ) {
			this.fgTool.hide();
		}
		if ( this.bgTool ) {
			this.bgTool.hide();
		}
		if ( this.supTool ) {
			this.supTool.hide();
		}
	},

	/** Fonction pour cacher la fenêtre */
	hide: function(){
		this.isVisible = false;
		if ( this.options.isSlide !== false ) {
			this.slide.slideOut();
		} else {
			this.container.hide();
		}
	},

	/** Fonction pour afficher la fenêtre */
	show: function(){
		this.isVisible = true;
		if ( this.options.isSlide !== false ) {
			this.slide.slideIn();
		} else {
			this.container.show();
		}
	},

	/**
	* Définir la largeur de la fenêtre
	* @param {Integer} w Largeur désirée
	*/
	setWidth: function(w){
		this.container.setStyle('width', w);
		this.width = w;
	},

	/**
	* Définir la hauteur de la fenêtre
	* @param {Integer} h Hauteur désirée
	*/
	setHeight: function(h){
		if(this.header){
			h += this.header.getSize().y;
		}
		this.container.setStyle('height', h);
		this.height = h;
	},

	///////////////////////////////////////////////////////////////////////////////////
	// Evénements
	///////////////////////////////////////////////////////////////////////////////////

	/** Lorsque l'on clic sur le panneau*/
	onClick: function(){},

	/** Lorsque l'on clic sur le panneau*/
	onMousedown: function(){},

	/** Lorsque le clic est fini */
	onMouseup: function(){},

	/**Pour augmenter le z-index du panneau*/
	onForegroundClick: function(){
		if ( DEBUG_MODE ) {
			this.log(this.options.windowsName + 'place at foreground (' + this.container.getStyle('z-index') + ')');
		}
		this.container.setStyle('z-index', parseInt(this.container.getStyle('z-index')) + 1)
	},

	/**Pour diminuer le z-index du panneau*/
	onBackgroundClick: function(){
		if ( parseInt(this.container.getStyle('z-index')) > 0 ) {
			if ( DEBUG_MODE ) {
				this.log(this.options.windowsName + ' placed at background (' + this.container.getStyle('z-index') + ')');
			}
			this.container.setStyle('z-index', parseInt(this.container.getStyle('z-index')) - 1)
		}
	},

	/** Appelée lorsque l'on quitte le survol de la fenêtre */
	onMouseout: function(){
		this.container.setStyle('opacity', this.options.opacity);
		if ( this.options.autoHideTools ) {
			this.hideTools();
		}
	},

	/** Appelée lorsque l'on survole de la fenêtre */
	onMouseover: function(){
		this.container.setStyle('opacity', this.options.onOverOpacity);
		if ( this.options.autoHideTools ) {
			this.showTools();
		}
	},

	/** Appelée au début du resize de la fenêtre */
	onReiszeStart: function(){},

	/** Appelée à la fin du resize de la fenêtre */
	onResizeEnd: function(){},

	/** Appelée au début du drag de la fenêtre */
	onDragStart: function(){},

	/** Appelée à la fin du drag de la fenêtre */
	onDragEnd: function(){
		this.firstDrag = false;
	},
	/**
	* Appelée lorsque le bouton de la souris est enfoncé
	*/
	onMouseDown: function(){}

});