//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
* @namespace 
* Espace de noms de la visionneuse.<br/>
* Il définit un tableau qui contient toutes les fonctions et les classes.<br/>
* piv signifie "pLEADE iMAGE vIEWER"
 */
var piv = {};

/**
* Passe la visionneuse en mode déboguage
*/
DEBUG_MODE = false;

/**
* @namespace
* Espace de noms des différents types de panneaux
*/
piv.panel = {};

/**
* Délai de temporisation lors du zoom avec la molette de la souris ou les boutons
*/
piv.ZOOM_DELAY = 0.5;

/**
* Rapport de la taille de l'image affichée sur la taille réelle au delà duquel il ne sera pas demandé au serveur
* de retailler la portion d'image en haute def.<br/>
* Il est logique de mettre 1 car au-delà, la resolution haute def demandée sera supérieure à la résolution de l'image,
* donc imagemagick devra interpoler les points supplémentaires. L'intérêt est éventuellement d'obtenir un lissage,
* l'image est plus jolie car imagemagick fait de l'antialiasing, ou quelque chose comme ça.
*
* Si par exemple la valeur vaut 2, la résolution haute def sera adaptée jusqu'à un zoom de 200%.
*
* Valeur décimale (float)
*/
piv.SERVER_RESIZE_MAX_RATIO = 2.0;

Element.implement({
	//implement show
	show: function() {
		this.setStyle('display', '');
		this.fade('in');
		this.store('_visible', true);
	},
	//implement hide
	hide: function() {
		this.setStyle('display', 'none');
		this.fade('out');
		this.store('_visible', false);
	},
	toggle: function(){
		if ( this.retrieve('_visible') === false ) {
			this.show();
		} else {
			this.hide();
		}
	},
	isVisible: function(){
		return this.retrieve('_visible');
	}
});

/**
* @fileOverview Classe principale de la visionneuse.
* @author Julien Huet
* @author Johan Cwiklinski
*/

/**
* @class Classe principale de la visionneuse.
* @author Julien Huet
* @author Johan Cwiklinski
* @augments Mootools.Log
*/
var pivViewer = new Class(/** @lends pivViewer.prototype */{
	
	/**
	* Serie d'image
	* @type Series
	*/
	series: null,

	/**
	* La barre avec le slider pour les séries images
	* @type SlideBar
	*/
	slidebar: null,

	/**
	* Elément contenant les infomations du status de l'image courante
	* @type Element
	*/
	imageStatus: null,

	/**
	* Elément contenant les infomations du status de la version courante
	* @type Element
	*/
	versionStatus: null,

	/**
	* Elément contenant les infomations du zoom
	* @type Element
	*/
	zoomStatus: null,

	/**
	* div contenant les informations sur la localisation de l'image
	* @type Element
	*/
	localization: null,

	/**
	* (Dés)Activation des différents paramètres et boutons
	* @type Object
	*/
	actives: null,
	
	/**
	* Couleur des fonds à rendre transparents (pour l'affichage du titre par exemple)
	* @type String
	*/
	transparentBgColor: '#222',

	/**
	* Opacité par défaut pour les fonds transparents
	* @type Float
	*/
	transparentBgOpacity: .8,
	
	/**
	* Valeur d'incrémentation de la rotation
	* @type Integer
	*/
	rotationAngleIncrement: 90,

	/**
	* Affichage de l'infobulle qui indique qu'une meilleure version est disponible
	* @type Boolean
	*/
	showVersionToolTip: true,
	
	/**
	* Partie de l'url pour obtenir la serie d'image
	* @type String
	*/
	seriesRequestUrl: '/dir.json',

	/**
	* Cadre de gestion d'image
	* @type Frame
	*/
	frame: null,

	/**
	* Boite à outils de la visionneuse
	* @type ToolBar
	*/
	toolbar: null,

	/**
	* Fenêtre d'apercu de l'image
	* @type Overview
	*/
	overview: null,

	/**
	* Panneau d'impression
	* @type PrintPanel
	*/
	printPanel: null,

	/**
	* Panneau de téléchargement
	* @type DownloadPanel
	*/
	downloadPanel: null,

  /**
  * Panneau d'informations sur le fragment
  * @type FragmentInfosPanel
  */
  fragmentInfosPanel: null,

	/**
	* Grille, découpage de l'image courante en tuile
	* @type Grid
	*/
	grid: null,

	/**
	* Barre de navigation de la visionneuse
	* @type NavBar
	*/
	navbar: null,

	/**
	* L'ensemble des Tips déclarés
	*/
	tips: null,
	
	///////////////////////////////////////////////////////////////////////////////////
	// Méthodes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	* <strong>Constructeur</strong>
	* @contructs
	*/
	initialize: function(serverPath, imagePath, imageCount, imageNum, baseLink, params, start, end) {
		this.actives = params;
    this.imageCount = imageCount;
		windowManager = new WindowManager();

		this.tips = new Tips( '#piv-body-main-lcol > a, #piv-mnu-recup > a, #piv-mnu-zoom > a, #piv-div-zoom > a, #zoom-sld-imageselect-bg', {
			className: 'maintip', // We need this to force the tip in front of nav window
			// We have to first set opacity to zero to fix a bug in mootools 1.2
			// where the tip appears without any fade in the first time it is loaded
			onShow: function(t){t.setStyle('opacity',0); t.fade(0.9); },
			onHide: function(t){t.fade(0); }
		});

		this.initStatus();
		this.initRotationAngle();

		if ( this.actives.navigation ) {
			if (imageCount > 1 ) {
				this.initNavigation(imageCount);
			}else{
				/** FIXME: certains éléments devraient être désactivés pour lesimages attachées et/ou relatives (comme la rotation par exemple). Pour le moment, on ne peut pas disinguer une image unique d'une image attachée :( */
        if ( $('btn-mosaic') ) {
          $('btn-mosaic').hide();
        }
				$('btn-lockzoom').hide();
				$('piv-body-hmenu').setStyle('visibility', 'hidden');
			}
		}
		this.initFrame();
		this.initToolBar();

		if ( imagePath == '' ) {
			this.frame.div.adopt(new Element(
				'div', {
					id: 'no-image',
					html: '<span class="title">' + _pivMessages.error + '</span>' + _pivMessages.missing_image
				}
			));
			this.resizeFrame();
		} else {
			this.resizeFrame();
			this.initSeries(serverPath, imagePath, imageCount, baseLink,start, end);
			this.displayImage(imageNum);

      this.initTitle();
			this.initOverview();
			this.initPrintPanel();
			this.initDownloadPanel(imageCount);

			if ( this.actives.previewOnLoad && this.toolbar ) {
				this.toolbar.toggleOverview();
			}
	
			this.initGrid();
			document.addEvent('keydown', this.key.bindWithEvent(this));
		}

		if ( DEBUG_MODE ) {
			this.enableLog().log('OK: Viewer initialized.');
		}
	},

	/**
	* Navigation au clavier dans la série et zoom
	*/
	key: function(e){
		var d = 100;
		switch (e.code) {
		case 39: //right arrow
			if ( this.navbar ) {
				this.navbar.btnMvNext.fireEvent('click', e);
			}
			break;
		case 37: //left arrow
			if ( this.navbar ) {
				this.navbar.btnMvPrev.fireEvent('click', e);
			}
			break;
		case 33: //page up
			if ( this.navbar ) {
				this.navbar.btnMvForw.fireEvent('click', e);
			}
			break;
		case 34: //page down
			if ( this.navbar ) {
				this.navbar.btnMvBack.fireEvent('click', e);
			}
			break;
		case 35: //end
			if ( this.navbar ) {
				this.navbar.btnMvLast.fireEvent('click', e);
			}
			break;
		case 36: //home
			if ( this.navbar ) {
				this.navbar.btnMvFirst.fireEvent('click', e);
			}
			break;
		case 107: // '+' key
			if ( !e.control ) {
				this.frame.panel.zoomCenterIn();
			}
			break;
		case 109: // '-' key
			if ( !e.control ) {
				this.frame.panel.zoomCenterOut();
			}
			break;
		}
	},

	///////////////////////////////////////////////////////////////////////////////////
	// Méthodes d'initialisations
	///////////////////////////////////////////////////////////////////////////////////
	/** Initialisation des barres de statut */
	initStatus: function(){
		if ( this.actives.status ) {
			this.tips.attach('#pnl-status-version');
			this.imageStatus = $('pnl-status-image');
			this.versionStatus = $('pnl-status-version');
			if ( this.actives.zoom ) {
				this.zoomStatus = $('pnl-status-zoom');
			}
		}
	},
	
  /** Initialisation de la barre de titres */
  initTitle: function(){
    if ( this.actives.title ) {
      this.localization = $('piv-series-info');
    }
  },
	
	/** Initialisation de l'angle de rotation */
	initRotationAngle: function(){
		if ( this.actives.rotationAngle ) {
			this.rotationAngleIncrement = this.actives.rotationAngle;
		}
	},

	/** Initialisation de la grille mosaique*/
	initGrid: function(){
		if ( this.actives.grid ) {
			this.grid = new Grid(this);
		}
	},
	
	/** Initialisation de la barre d'outil */
	initToolBar: function(){
		this.toolbar = new ToolBar(this);
	},
	
	/** Initialisation de l'overview */
	initOverview: function(){
		if ( this.actives.preview ) {
			this.overview = new Overview(this);
		}
	},
	
	/** Initialisation du panneau d'impression */
	initPrintPanel: function(){
		if ( this.actives.print ) {
			this.printPanel = new PrintPanel(this);
		}
	},
	
	/** Initialisation la bar de navigation */
	initNavigation: function(imageCount){
		this.slidebar = new SlideBar(this, imageCount);
		this.navbar = new NavBar(this);
	},
	
	/**
	* Initialisation du panneau de téléchargement
	* @param {Integer} imageCount
	*/
	initDownloadPanel: function(imageCount){
		if(this.actives.pdf || this.actives.zip) {
			this.downloadPanel = new DownloadPanel(this, imageCount);
		}
	},
	
	/** Initialisation du conteneur d'image */
	initFrame: function(){
		this.frame = new Frame(this);
	},

	/** Resize de la visionneuse */
	resizeFrame: function(){
		this.frame.resize();
	},
	
	/** Initialisation de la serie d'image */
	initSeries : function(serverPath, imagePath, imageCount, baseLink,start, end){
		this.series = new Series(this, serverPath, imagePath, imageCount, baseLink,start, end);
		this.series.setRequestUrl(this.seriesRequestUrl);
	},

	/**
	* Méthode appellée une fois que l'image est chargée
	*/
	onImageLoaded: function(){
		if( this.actives.preview && this.actives.previewOnLoad ) {
			this.showOverview();
		}
		//si on arrive ici, c'est que l'édition est forcément finie...
		if( $('txt-imageselect').hasClass('in-edit') ) {
			$('txt-imageselect').removeClass('in-edit')
		}

    if ( this.actives.navigationCotes ) {
      this.getCotes();
    }
	},
	
	/**
	* Affiche l'image à partir de son numéro<br/>
	* Si les informations de l'image n'existent pas dans l'objet series, ces informations seront demandées.<br/>
	* Si les informations sont téléchargées de façon asynchrone (en Ajax par exemple), la méthode responsable
	* devra rappeler cette méthode ci dans son événement.
	* @param {Integer} imageNum Le numéro de l'image
	*/
	displayImage: function(imageNum) {
		if(DEBUG_MODE){
			this.enableLog().log('displayImage... '+imageNum);
		}
		//suppression du menu des versions s'il existe
		if( this.toolbar ) {
			if(this.toolbar.mnuVersions) this.toolbar.mnuVersions.destroy();
			this.toolbar.mnuVersions = null;
		}
		this.showImageLoading();
    if(this.series){
      if ( this.series.hasImage(imageNum) == false ) {
        this.series.getImage(imageNum);
      } else {
        this.series.setCurrentImage(imageNum);
        if ( this.series.size > 1 ) {
          this.slidebar.extChanged = true;
          this.slidebar.setImageNumber(imageNum);
          this.navbar.redraw(imageNum);
        }
        this.displayCurrentImage();
        //il est nécessaire de préciser le numéro de l'image ici ; pour les surcharges
        this.update(imageNum);
        if ( DEBUG_MODE ) {
          this.enableLog().log('End Display IMG num' + imageNum + ' OK');
        }
      }
    }
	},
	
	/**
	* Affichage de l'image courante de la serie
	**/
	displayCurrentImage: function(){
		this.frame.updateLockedParams();
		this.frame.panel.setImage(this.series.currentImage);
		this.toolbar.update(this.series.currentImage);
		if ( this.actives.grid ) {
			this.grid.setImage(this.series.currentImage);
		}
		if ( this.actives.grid ) {
			this.grid.reset();
		}
		if ( this.overview ) {
			this.overview.setImage(this.series.currentImage);
		}
	},
	
	/**
	* Mise à jour des informations sur l'image courante
	*/
	update: function(){
		if ( this.actives.status ) {
			this.updateImageStatus();
		}
		if ( this.actives.status ) {
			this.updateVersionStatus();
		}
		this.updateZoomStatus();
		if ( this.actives.title ) {
			this.getImageInfos();
		}
	},


	/**
	* Affiche les informations de l'image courante
	*/
	updateImageStatus: function() {
		if ( this.imageStatus ) {
			this.imageStatus.set('html', this.series.currentImage.num + ' : ' + this.series.currentImage.currentVersion.name);
		}
	},

	/**
	* Affichage d'un message pour annoncer le chargement de l'image
	*/
	showImageLoading: function(){
    if(this.frame){
		  this.frame.showLoading();
    }
	},
	
	
	/**
	* Affiche une version de l'image
	* @param {Integer} version Le numéro de la version à afficher
	*/
	displayVersion: function(version) {
		if ( this.actives.grid ) {
			this.grid.hide();
		}
		this.frame.showLoading();
		this.series.setCurrentVersion(version);
		this.frame.panel.setCurrentVersion();
		if ( version.role == 'tile' ) {
			this.showTile();
		}
		
		if ( this.actives.status ) {
			this.updateVersionStatus();
		}
		this.updateZoomStatus();
	},

	/**
	* Affiche les informations de la version courante
	*/
	updateVersionStatus: function() {
		if ( this.versionStatus ) {
			this.versionStatus.set('html', this.series.currentImage.currentVersion.summary);
		}
	},

	/**
	* Affiche la valeur actuelle du zoom
	*/
	updateZoomStatus: function() {
		if ( this.actives.status ) {
			this.zoomStatus.set('html', this.frame.panel.getZoom() + '%');
		}
	},

	/**
	* Va chercher le titre du document auquel est rattachée l'image
	*/
	getImageInfos: function() {
		var s_path = this.getImageInfosUrl();
		var param = this.getImageInfosParam();
		
		var req = new Request.HTML({
			url: s_path,
			method: 'GET',
			onSuccess: this.onInfos.bind(this),
			onFailure: function() {
				if ( DEBUG_MODE ) {
					this.enableLog().log('getImageInfos FAIL, serie path =' + img_p);
				}
			}
		});
		req.send(param);
	},

  getCotes: function(){
    var _url = this.series.baselink + 'functions/functions/navigation-cotes.json';

    var req = new Request.JSON({
      url: _url,
      method: 'GET',
      onSuccess: this.onCotes.bind(this),
      onFailure: function() {
        if ( DEBUG_MODE ) {
          this.enableLog().log('getCotes FAIL, url =' + _url);
        }
      }
    });
    req.send('value=' + this.series.path)

  },

  /**
  * Fonction retournant l'url utilisée pour récuperer les informations sur une image
  * @return {String} url
  */
  getImageInfosUrl: function(){
    return this.series.baselink + 'functions/ead/infosimage.ajax';
  },

  /**
  * Fonction retournant l'url utilisée pour récuperer les informations sur un fragment
  * @return {String} url
  */
  getFragmentInfosUrl: function(id){
    return this.series.baselink + 'functions/ead/' + id + '/fragment-infos.ajax';
  },

  /**
  * Fonction retournant les paramêtres utilisés pour récuperer les informations sur une image
  * @return {String} param
  */
  getImageInfosParam: function(){
    var img_p = this.series.path;
    var reg1 = new RegExp('^_ead/functions/ead/relative');
    if ( img_p.match(reg1) ) {
      //dans le cas d'une image relative, on ne récupère que la fin de l'url
      img_p = img_p.substring( img_p.lastIndexOf('/') + 1 );
    }
    var reg2 = new RegExp('[\?]');
    if ( img_p.match(reg2) ) {
      //dans le cas d'une image retaillée ou autre, on ne veut que le nom de l'image
      img_p = img_p.substring(0, img_p.indexOf('?'));
    }

    var param =  'path=' + img_p + '&baselink=' + this.series.baselink;

    var _qs = window.location.href.substring(window.location.href.indexOf('?') + 1, window.location.href.length).parseQueryString();
    //si on a une sous série
    if ( _qs.np && _qs.np != '' && _qs.nd && _qs.nd != '' ) {
      param += '&np=' + _qs.np + '&nd=' + _qs.nd;
    }
    //si on a une image unique
    if( _qs.name && _qs.name != '' ) {
      param += '&name=' + _qs.name;
    } else {
      //dans tous les cas, on va envoyer le nom de l'image courante ; si rien n'est trouvé, la XSP se charge de supprimer le nom de l'image de toutes façons
      param += '&name=' + this.series.currentImage.name
    }
    return param;
  },

  ///////////////////////////////////////////////////////////////////////////////////
  // Evénements
  ///////////////////////////////////////////////////////////////////////////////////

  /**
  * Après récupération des infos image
  * @param {Element} r Le résultat de l'appel à infosimage
  */
  onInfos: function(r) {
    if ( DEBUG_MODE ) {
      this.enableLog().log('onInfos');
    }
    // index 1, le 0 est la déclaration XML...
    var i = 0;
    var find = false;
    while ( (i < r.length) && (!find) ) {
      if ( $type(r[i])=='element' ) {
        find = true;
      } else {
        i++;
      }
    }
    if( (find) && (r[i].getChildren().length > 0) ) { //on n'affiche pas la barre de titre si on n'a pas récupéré de titre
      var _el = null;
      if($('piv-mcol-frame')){
        _el = $('piv-mcol-frame').getElementById('current_title');
        if ( !_el ) {
          _el = new Element(
            'div', {
              id: 'current_title',
              opacity: this.transparentBgOpacity,
              styles: {
                background: this.transparentBgColor
              }
            }
          );
        }else{
          _el.empty();
        }
        //si on veut pouvoir récupérer plus d'infos sur le fragment
        if ( this.actives.fragmentInfos ) {
          var _moreInfos = new Element(
            'a', {
              id: 'fragment-infos',
              href: '#',
              text: _pivMessages.fragment_infos
            }
          );
          _el.adopt(r, _moreInfos);
          if ( this.fragmentInfosPanel != null ) {
            this.fragmentInfosPanel.destroy();
            this.fragmentInfosPanel = null;
          }
          _moreInfos.addEvent('click', this.getFragmentInfos.bind(this));
        } else {
          _el.adopt(r);
        }

        $('piv-mcol-frame').adopt(_el);
      }
    }
    if ( DEBUG_MODE ) {
      this.enableLog().log('onInfos OK');
    }
  },

  onCotes: function(r) {
    if(r.preceding){
      this.toolbar.btnPrevCote.href = this.series.baselink + 'img-viewer/' + r.preceding.replace(/\//, '') + '/viewer.html';
      this.toolbar.btnNextCote.href = this.series.baselink + 'img-viewer/' + r.following.replace(/\//, '') + '/viewer.html';
      $('current-cote').set('text', r.current.replace(/\//, ''));
    }
    else console.log("série : "+this.series.path+" ne posséde pas de navigation par côte.");
  },

  getFragmentInfos: function(raz){
    var _fragId = $('current_title').getFirst('span').get('id');
    if ( _fragId == null || _fragId == '' ) {
      if ( DEBUG_MODE ) {
        this.enableLog().log('[getFragmentInfos]: Fragment ID could not be found.');
      }
      return;
    }
    if ( raz === true ) {
      this.fragmentInfosPanel.destroy();
      this.fragmentInfosPanel = null;
    }
    if ( this.fragmentInfosPanel != null ) {
      if ( this.fragmentInfosPanel.isVisible ) {
        this.fragmentInfosPanel.hide();
      } else {
        this.fragmentInfosPanel.show();
      }
    } else {
      this.fragmentInfosPanel = new FragmentInfosPanel(this);
      var s_path = this.getFragmentInfosUrl(_fragId);
      var param = this.getImageInfosParam();

      var req = new Request.HTML({
        url: s_path,
        method: 'GET',
        onSuccess: this.onFragmentInfos.bind(this),
        onFailure: function() {
          if ( DEBUG_MODE ) {
            this.enableLog().log('getFragmentInfos FAIL, serie path =' + img_p);
          }
        }
      });
      req.send(param);
    }
  },

  /**
  * Après récupération des infos sur le fragment
  * @param {Element} r Le résultat de l'appel à infosimage en mode étendu
  */
  onFragmentInfos: function(r){
    this.fragmentInfosPanel.mnuInfos.adopt(r[0]);
    this.fragmentInfosPanel.show();
  },

	/**
	* Méthode appelée pendant le drag de l'image principale (pan).
	*/
	onDrag: function() {
		this.overview.setMask();
	},

	/**
	* Affichage d'une tuile de la grille
	*/
	showTile: function(){
		if ( this.actives.grid ) {
			// MAJ des boutons de la vue "grille (ancienne mosaïque Pleade libre)
			this.grid.initNavigationButton();
			if ( !this.overview.hidden ) {
				this.grid.showOverViewGrid();
			}
		}
		this.frame.panel.unlockZoom();
	},

	/**
	* Affichage de la grille
	*/
	showGrid: function(){
		if ( this.series.currentImage.currentVersion.tile ) {
			this.displayVersion(0);
		}
		this.onClickFitFrame();
		this.overview.hideMask();
		this.grid.showMainGrid();
		if ( !this.overview.hidden ) {
			this.grid.showOverViewGrid();
		}
		this.frame.panel.lockZoom();
		this.gridActive = true;
	},

	/**
	* Masquage de la grille
	*/
	hideGrid: function(){
		if ( this.series.currentImage.currentVersion.tile ) {
			this.displayVersion(0);
		} else {
			if ( !this.overview.hidden ) {
				this.grid.hideOverViewGrid();
				this.overview.showMask();
			}
		}
		this.onClickFitFrame();
		this.grid.hideMainGrid();
		this.frame.panel.unlockZoom();
		this.gridActive = false;
	},

	/**
	* Affichage de la fenêtre d'aperçu
	*/
	showOverview: function() {
		this.overview.show();
	},
	
	/**
	* Masquage de la fenêtre d'aperçu
	*/
	hideOverview: function() {
		this.overview.hide();
	},
	
	/**
	* Affichage de la fenêtre d'impression
	*/
	showPrintPanel: function() {
		this.printPanel.show();
	},

	/**
	* Masquage de la fenêtre d'impression
	*/
	hidePrintPanel: function() {
		this.printPanel.hide();
	},

	/**
	* Affichage de la fenêtre de téléchargement
	*/
	showDlPanel: function() {
		this.downloadPanel.show();
	},

	/**
	 * Masquage de la fenêtre d'impression
	 */
	hideDlPanel: function() {
		this.downloadPanel.hide();
	},

	/**
	* Zoom avant depuis le bouton de la toolbar
	*/
	onClickZoomIn: function() {
		this.frame.panel.onClickZoomIn();
		this.overview.setMask();
		this.updateZoomStatus();
	},

	/**
	* Zoom arrière depuis le bouton de la toolbar
	*/
	onClickZoomOut: function() {
		this.frame.panel.onClickZoomOut();
		this.overview.setMask();
		this.updateZoomStatus();
	},

	/**
	* Affichage de la taille réelle depuis le bouton de la toolbar
	*/
	onClickRealSize: function() {
		this.frame.panel.setRealSize();
		this.overview.setMask();
		this.updateZoomStatus();
	},

	/**
	* Affichage de la taille optimale depuis le bouton de la toolbar
	*/
	onClickFitFrame: function() {
		this.frame.panel.fitFrame();
		this.overview.setMask();
		this.updateZoomStatus();
	},
	
	/**
	* Bloquer le zoom
	*/
	lockZoom: function(){
		this.frame.lockZoom();
	},
	
	/**
	* Débloquer le zoom
	*/
	unlockZoom: function(){
		this.frame.unlockZoom();
	},

	/**
	* Rotation de l'image
	*/
	onClickRotate: function() {
		this.series.currentImage.setRotation(this.rotationAngleIncrement, false);
		this.displayImage(this.series.currentImage.num);
		if ( this.actives.grid && this.toolbar.btnGrid) {
			if ( (this.series.currentImage.getRotation() == 0 || this.series.currentImage.getRotation() == 360) && this.toolbar.btnGrid.hasClass('disabled') ) {
				this.toolbar.btnGrid.removeClass('disabled');
			} else if ( this.series.currentImage.getRotation() != 0 && this.series.currentImage.getRotation() != 360 && !this.toolbar.btnGrid.hasClass('disabled') ) {
				this.toolbar.btnGrid.addClass('disabled');
			}
		}
	}

});
