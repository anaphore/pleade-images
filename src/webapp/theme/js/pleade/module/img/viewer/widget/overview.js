///Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Fenêtre d'aperçu
 * @author David Bambouché
 * @author Johan Cwiklinski
 * @author Julien Huet
 */
/**
 * @class Fenêtre d'aperçu
 * @author David Bambouché
 * @author Johan Cwiklinski
 * @author Julien Huet
 * @augments CWindow
*/
Overview = new Class(/** @lends Overview.prototype */{
	Extends: CWindow,
	
///////////////////////////////////////////////////////////////////////////////////
	// Attributs
	///////////////////////////////////////////////////////////////////////////////////

	/**
	* Référence vers la classe principale
	* @type pivViewer
	*/
	viewer: null,

	/**
	* Le panneau d'aperçu
	* @type pivPanel
	*/
	panel: null,

	/**
	* Le div qui contient l'aperçu
	* @type Element
	*/
	div: null,

	/**
	* La barre de boutons de zoom
	*/
	buttonsBar: null,

	/**
	* L'élément HTML img dans l'aperçu
	* @type Element
	*/
	img: null,

	/**
	* Le div du mask
	* @type Element
	*/
	mask: null,

	/**
	* Rapport entre la largeur de l'image dans l'aperçu et sa largeur affichée
	* Facteur de diminution, inférieur à < 1
	* @type Integer
	*/
	scale: 1,

	/**
	* Référence utilisée pour enregistrer la méthode center pour l'événement resize du frame
	* @type Function
	*/
	resizeHandler: null,
	
	/**
	* Hauteur de la barre de chargement
	* @type {Integer}
	*/
	loadBarHeight: 0, /** FIXME Rendre cette taille dynamique*/

	///////////////////////////////////////////////////////////////////////////////////
	// Méthodes
	///////////////////////////////////////////////////////////////////////////////////

	/**
	* <strong>Constructor</strong>
	* @param {pivViewer} viewer Instance de la visionneuse
	*/
	initialize: function(viewer) {
		this.viewer = viewer;
		this.firstShow = true;
		this.div = $('pnl-overview-vp');
		this.img = $('pnl-overview-img');
		this.mask = $('pnl-overview-mask');
		this.buttonsBar = $('piv-mnu-zoom');
		this.firstLoad = true;
		this.parent({
			resizable: false,
			defaultColor: 'transparent',
			container: this.viewer.frame.div,
			showOnLoad: this.viewer.actives.previewOnLoad,
			windowName: 'overview',
			title: ( this.viewer.actives.zoom && this.viewer.actives.status ) ? _pivMessages.zoom : ''
		});
		this.render();
		this.isVisible = this.viewer.actives.previewOnLoad;
		if( this.viewer.actives.previewOnLoad ) {
			this.viewer.toolbar.btnOverview.addClass('checked');
		}
		this.viewer.frame.div.adopt(this.container);
		this.container.adopt(this.div);
		this.container.setStyle('width', 200);
		this.container.adopt(this.buttonsBar);

		if ( this.viewer.actives.zoom && this.viewer.actives.status ) {
			//on attache les infos de zoom au titre de la fenêtre
			$$('#overview > .panelHeader > .title').grab($('pnl-status-zoom').removeClass('hidden'), 'bottom');
		}
		
		this.setWidth(this.div.getWidth());
		this.buttonsBar.setStyle('width', this.div.getWidth());
		
		this.draggableOptions = {
			container: $('pnl-overview-vp'),
			onStart: this.startDrag.bindWithEvent(this),
			onComplete: this.endDrag.bindWithEvent(this)
		};
		this.mask.makeDraggable(this.draggableOptions); 

		this.mask.set('morph',{
			duration: 500,
			transition: Fx.Transitions.Quad.easeInOut
		});
		this.initEvent();
		this.resizeHandler = this.onFrameResize.bind(this);
		this.initLoadBar();
	},
	
	/**
	* Initialise des evenements de l'overview
	*/
	initEvent: function(){
		this.div.addEvent( 'click', this.onClick.bindWithEvent(this) );
		this.div.addEvent( 'mousewheel', this.onMousewheel.bindWithEvent(this) );
		this.mask.addEvent( 'mousewheel', this.onMousewheel.bindWithEvent(this) );
		this.mask.addEvent( 'dblclick', this.onDblClick.bindWithEvent(this) );
	},
	
	/**
	* Initialisation de la barre de chargement de l'overview
	*/
	initLoadBar: function(){},
	
	/**
	* Cache l'overview
	*/
	destroy: function(){
		if ( this.viewer.toolbar ) {
			this.viewer.toolbar.btnOverview.removeClass('checked');
			this.viewer.toolbar.toggleOverview();
		}
	},

	/**
	* Affiche le panneau
	*/
	show: function() {
		this.parent();
		this.viewer.frame.onResizeRegister(this.resizeHandler);
		if ( this.viewer.toolbar.btnGrid ) {
			if ( this.viewer.gridActive
				|| (this.viewer.series.currentImage && this.viewer.series.currentImage.currentVersion.role=='tile')
			) {
				this.viewer.grid.showOverViewGrid();
			} else {
				this.showMask();
			}
		} else {
			this.showMask();
		}
	},

	/**
	* Cache le panneau
	*/
	hide: function() {
		this.parent();
		this.viewer.frame.onResizeUnregister(this.resizeHandler);
		if ( this.viewer.actives.grid ) {
			this.viewer.grid.hideOverViewGrid();
		}
		this.hideMask();
	},

	/**
	* Affiche l'image passée en paramêtre dans l'overiew
	* @param {Image} image
	*/
	setImage: function(image){
		this.img.src = this.getOverviewImageUrl(image);
		this.setImageSize(image);
		if ( this.img.src ) {
			this.computeScale();
			this.setMask();
			/** Si aucun drag n'a eu lieu on place la fenetre en bas a droite */
			if ( this.firstDrag ) {
				this.initOverviewPosition();
			}
		}
	},
	
	/**
	* Initialise la position de la fenêtre d'aperçu ; rend visible la buttonsBar
	*/
	initOverviewPosition: function(){
		var posX = this.viewer.frame.div.getSize().x - this.width;
		var posY = (this.viewer.frame.div.getSize().y - this.height - (((this.isVisible || this.firstLoad) ? 1 : 2) * this.loadBarHeight)) / 2;
		this.firstLoad = false;
		this.container.setPosition({
			x: (posX > 0) ? posX : 0,
			y: (posY > 0) ? posY : 0
		});
		//on supprime ici la classe hidden de la butonBar pour éviter qu'elle ne soit affichée trop tôt.
		this.buttonsBar.removeClass('invisible');
	},

	/**
	* Définit la taille de l'image en fonction du ratio
	* @param {Image} image
	*/
	setImageSize: function(image){
		this.img.width = this.width;
		if ( image.ratio ) {
			this.img.height = this.width / image.ratio;
		} else {
			this.img.height = this.width;
		}
		this.setWidth(this.img.width);
		this.setHeight(this.img.height + this.buttonsBar.getHeight());
	},

	/**
	* Récuperation de l'URL de l'image de l'overview
	* @return {String} url
	*/
	getOverviewImageUrl: function(image){
		var src = image.currentVersion.url;
		if ( src.indexOf('?') == -1 ) {
			src += '?';
		} else {
			src += '&';
		}
		src += 'r=' + image.rotation;
		return src;
	},
	
	/**
	* Calcul le facteur de diminution
	*/
	computeScale: function() {
		this.scale =  this.width / this.viewer.frame.panel.width;
	},
	
	/**
	* Règle la taille du masque de l'aperçu.
	*/
	setMask: function() {
		if ( this.isVisible && !this.viewer.gridActive ) {
			//position left
			_left = - this.viewer.frame.panel.left * this.scale;
			if ( _left < 0 || isNaN(_left) ) {
				_left = 0;
			}
			//position top
			_top = - this.viewer.frame.panel.top * this.scale;
			if ( _top < 0 || isNaN(_top) ) {
				_top = 0;
			}
			//max sizes
			_overview_sizes = this.div.getSize();
			//setting mask width
			_width = this.viewer.frame.width * this.scale;
			if ( _width > _overview_sizes.x ) {
				_width = _overview_sizes.x;
			}
			//setting mask height
			_height = this.viewer.frame.height * this.scale;
			if( _height > _overview_sizes.y ) {
				_height = _overview_sizes.y;
			}
			// Move the zone to the new size and position
			this.mask.morph({
				left: _left,
				top: _top,
				width: _width,
				height: _height
			});
		}
	},
	
	/**
	* Cache le masque
	*/
	hideMask: function() {
		this.mask.setStyle('visibility', 'hidden');
	},
	
	/**
	* affiche le masque
	*/
	showMask: function() {
		this.setMask();
		this.mask.setStyle('visibility', 'visible');
	},
	
	///////////////////////////////////////////////////////////////////////////////////
	// Evènements
	///////////////////////////////////////////////////////////////////////////////////

	/**
	* Fonction appelée lorsque le panneau de l'image est redimensionné
	*/
	onPanelResize: function() {
		this.computeScale();
		this.setMask();
	},

	/**
	* Fonction appelée lorsque le masque de l'aperçu est déplacé.
	* Ajuste la position de l'image principale en fonction du rectangle correspondant au masque.
	*/
	onDrag: function() {},
	
	/**
	* Fonction appelée lorsque le masque de l'aperçu est déplacé et l'action debute.
	*/
	startDrag: function(){},
	
	/** 
	* Evenement lors d'un click dans l'overview
	*/
	onClick: function(e){
		var xmove = 0;
		var ymove = 0;
		// on verifie si on click bien sur la zone image
		if(e.target.get('id') == this.img.get('id')){
			// calcul des coordonnées
			var pos = this.div.getPosition();
			var zone_size = this.mask.getSize();
			var zone_w = zone_size.x;
			var zone_h = zone_size.y;
			xmove = e.event.clientX - pos.x - zone_w/2;
			ymove = e.event.clientY - pos.y - zone_h/2;
			if ( xmove > (this.width - zone_w) ) {
				xmove = this.width - zone_w;
			} else if ( xmove < 0 ) {
				xmove = 0;
			}
			if ( ymove > (this.height - zone_h) ) {
				ymove = this.height - zone_h;
			} else if ( ymove < 0 ) {
				ymove = 0;
			}
			// On appelle la fonction endDrag lorsque l'effet de deplacement du masque est fini. La fonction permet de deplacer l'image principale
			this.mask.set(
				'morph',  {
					onComplete : this.endDrag.bind(this)
				}
			);
			// On deplace le mask de xmove et de ymove
			this.mask.morph({
				left:  xmove,
				top:  ymove
			});
		}
	},
	
	/**
	* Evenement lors du double click
	*/
	onDblClick: function(){},
	
	/**
	* Evenement lors de l'utilisation de la molette
	*/
	onMousewheel: function(){},
	

	/**
	* Fonction appelée lorsque le cadre est redimenssionné
	*/
	onFrameResize: function() {
		this.setMask();
	},
	
	/**
	* Fonction appelée lorsque le masque de l'aperçu est déplacé et l'action terminée.
	*/
	endDrag: function(el, e) {
		//annulation de l'evenement onComplete qui a été activé lors du click sur la fenêtre d'apercu
		this.mask.set('morph',  {onComplete : $empty });
		_overview_sizes = this.div.getSize();
		_mask_sizes = this.mask.getSize();
		//on n'agit ici que si la taille du masque est inférieure à celle de l'aperçu
		if( _mask_sizes.x < _overview_sizes.x || _mask_sizes.y < _overview_sizes.y ) {
			var top = this.mask.getCoordinates().top - this.mask.getOffsetParent().getCoordinates().top;
			var left = this.mask.getCoordinates().left - this.mask.getOffsetParent().getCoordinates().left;

			if( _mask_sizes.x < _overview_sizes.x && _mask_sizes.y < _overview_sizes.y ) {
				//la taille du masque est inférieure en largeur et en hauteur
				this.viewer.frame.panel.setPos(
					- left / this.scale,
					- top / this.scale
				);
			} else if ( _mask_sizes.x < _overview_sizes.x ) {
				//la taille du masque est inférieure en largeur uniquement
				this.viewer.frame.panel.setLeftPos(
					- left / this.scale
				);
			} else if ( _mask_sizes.y < _overview_sizes.y ) {
				//la taille du masque est inférieure en hauteur uniquement
				this.viewer.frame.panel.setTopPos(
					- top / this.scale
				);
			}
			this.viewer.frame.panel.endDrag();
		}
	},

	/** Surcharge de la méthode : IE ne parviens pas en prendre en compte l'affet opacity avec le mask */
	onMouseout: function(){
		if ( !Browser.Engine.trident ) {
			this.container.setStyle('opacity', this.options.opacity);
		}
	},

	/** Surcharge de la méthode : IE ne parviens pas en prendre en compte l'affet opacity avec le mask */
	onMouseover: function(){
		if ( !Browser.Engine.trident ) {
			this.container.setStyle('opacity', this.options.onOverOpacity);
		}
	}
});
