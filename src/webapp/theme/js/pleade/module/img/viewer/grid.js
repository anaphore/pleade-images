//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$
/**
 * @fileOverview Cette classe représente une grille, soit le découpage d'une image en différentes tuiles
 * @author David Bambouché
 * @author Johan Cwiklinski
 * @author Julien Huet
 */
/**
 * @class Cette classe représente une grille, soit le découpage d'une image en différentes tuiles
 * @author David Bambouché
 * @author Johan Cwiklinski
 * @author Julien Huet
 * @augments Mootools.Log
*/
var Grid = new Class(/** @lends Grid.prototype */{
	
	/**
	 * Instance de la visionneuse
	 * @private
	 * @type pivViewer
	 */
	viewer: null,
	/**
	 * Flag pour savoir si la grille est affichée
	 * @type Boolean
	 */
	active: false,
	/**
	 * Flag pour savoir si la grille a déjà été initialisée
	 * @type Boolean
	 */
	initialized: false,
	/**
	 * Flag pour savoir si l'on découpe automatiquent les tuiles ou si l'on se base sur le fichier de configuation
	 * @type Boolean
	 */
	autoTileSize: false,
	/**
	 * Nombre determinant de nombre de tuile horizontale
	 * @type Integer
	 */
	nbHTiles: 0,
	/**
	 * Nombre determinant de nombre de tuile verticale
	 * @type Integer
	 */
	nbVTiles: 0,
	
	/** Largeur d'une tuile 
	 * @type Integer 
	 */
	tileWidth: 0,
	/** 
	 * Hauteur d'une tuile 
	 * @type Integer 
	 */
	tileHeight: 0,
	/**
	 * Image sur laquelle on déssine la grille
	 * @type Image
	 */
	image: null,
	/**
	 * La version par defaut de l'image
	 * @type Object
	 */
	overviewVersion: null,
	/**
	 * La version par defaut de l'image
	 * @type Object
	 */
	originalVersion: null,
	/**
	 * La grille qui se trouve sur la fenêtre d'apercu (l'overview)
	 * @type Element
	 */
	overviewGrid: null,
	/**
	 * La grille principale qui se trouve sur l'image
	 * @type Element
	 */
	mainGrid: null,	
	
	/**
	* <strong>Constructeur</strong>
	* Cette classe représente une grille, soit le découpage d'une image en différentes tuiles
	*/
	initialize: function(viewer) {
		
		this.viewer = viewer;
		// On indique que l'initialisation n'a pas été faite
		this.initialized = false;
		
		this.autoTileSize = false;
		
		// on initialise les boutons
		//this.initNavigationButton();
		this.removeAllNavigationControl();
	},
	
	/**
	 * Effacement des boutons de navigations de la grille lorsqu'une seule tuile est affichée.
	 */
	removeAllNavigationControl: function(){
		this.removeNavigationControl('top');
		this.removeNavigationControl('right');
		this.removeNavigationControl('bottom');
		this.removeNavigationControl('left');
	},
	
	/**
	 * Initialisation des boutons de navigation lorsqu'une tuile est affichée
	 */
	initNavigationButton: function(){
		var p = $('piv-mcol-frame');
		// Dimensions des boutons de navigation
		var controlS = 25;
		var controlL = 75;
		this.removeAllNavigationControl();
		
		var t = this.image.currentVersion.tile;
		// Dimensions du viewport
		/*var vpW = this.viewer.viewport.width;
		var vpH = this.viewer.viewport.height;*/
		var vpW = p.getWidth();
		var vpH = p.getHeight();
		var next = this.above(t);
		if ( next ) {
			var d = this.getTileNavigationControl('top');
			d.setStyle('top', 0 + 'px');
			d.setStyle('left', (vpW / 2) - (controlL/2) + 'px');
			d.addEvent('click', this.handleBtnGridNavigationClick(this,next));
			p.appendChild(d);
		}
		next = this.right(t);
		if ( next ) {
			var d = this.getTileNavigationControl('right');
			d.setStyle('top', (vpH/2) - (controlL/2) + 'px');
			d.setStyle('left', (vpW - controlS) + 'px');
			d.addEvent('click', this.handleBtnGridNavigationClick(this,next));
			p.appendChild(d);
		}
		next = this.below(t);
		if ( next ) {
			var d = this.getTileNavigationControl('bottom');
			d.setStyle('top', (vpH-controlS) + 'px');
			d.setStyle('left', (vpW / 2) - (controlL/2)+ 'px');
			d.addEvent('click', this.handleBtnGridNavigationClick(this,next));
			p.appendChild(d);
		}
		next = this.left(t);
		if ( next ) {
			var d = this.getTileNavigationControl('left');
			d.setStyle('top', (vpH/2) - (controlL/2) + 'px');
			d.setStyle('left', 0 + 'px');
			d.addEvent('click', this.handleBtnGridNavigationClick(this,next));
			p.appendChild(d);
		}
	},
	
	/**
	 * Reinitialise la grille
	 */
	reset: function(){
		if(this.mainGrid){
			this.mainGrid.dispose();
			this.mainGrid = null;
		}
		if(this.overviewGrid){
			this.overviewGrid.dispose();
			this.overviewGrid = null;
		}
		this.initialized = false;
		this.removeAllNavigationControl();
	},
	
	/**
	 * Initialise l'image sur laquelle on dessine une grille
	 * @param {Image} img
	 */
	setImage: function(img){
		// On conserve le set
		this.image = img;
		// On conserve la version qui sert d'aperçu
		this.overviewVersion = this.image.defaultVersion;
		// On conserve la version qui correspond à l'original
		this.originalVersion = this.image.originalVersion;
		
		if(this.image.grid){
			if(!this.autoTileSize){
				this.tileWidth = this.image.grid.width;
				this.tileHeight = this.image.grid.height;
			}else{
				this.tileWidth = this.viewer.frame.panel.div.getWidth();
				this.tileHeight = this.viewer.frame.panel.div.getHeight();
			}
		}
		
		this.initialized = false;
		
	},

	/**
	*	Lors d'un premier appel à cette mosaïque, on calcul les informations nécessaires.
	*/
	init: function() {
		// Si déjà fait, rien à faire
		if (this.initialized) return;
		// Nombre de tuiles en horizontal et en vertical
		var origW = this.originalVersion.width;
		this.nbHTiles = parseInt(origW / this.tileWidth);
		if ( origW % this.tileWidth != 0 ) this.nbHTiles++;
		var origH = this.originalVersion.height;
		this.nbVTiles = parseInt(origH / this.tileHeight);
		if ( origH % this.tileHeight != 0 ) this.nbVTiles++;

		// On boucle sur les tuiles pour les créer et les conserver
		this.tiles = new Array();
		for (i=1; i<=this.nbHTiles; i++) {
			this.tiles[i] = new Array();
			for (j=1; j<=this.nbVTiles; j++) {
				var w = Math.min(this.tileWidth, (origW - (i-1) * this.tileWidth + 1));
				var h = Math.min(this.tileHeight, (origH - (j-1) * this.tileHeight + 1));
				/*this.tiles[i][j] = new PivGridTile(this.viewer, this.image, this.originalVersion.mimeType, i, j, (i-1) * this.tileWidth + 1, (j-1) * this.tileHeight + 1, Math.min(this.tileWidth, (origW - (i-1) * this.tileWidth + 1)), Math.min(this.tileHeight, (origH - (j-1) * this.tileHeight + 1)));*/
				this.tiles[i][j] = new PivGridTile(this.viewer, this.image, this.originalVersion.mimeType, i, j, (i-1) * this.tileWidth + 1, (j-1) * this.tileHeight + 1, w, h);
			}
		}
		// On indique que l'initialisation a été faite
		this.initialized = true;
	},

	/**
	*	Ajoute des div à un élément HTML pour représenter la grille
	*/
	drawGrid: function(el, ratio) {
		this.init();
		for (i=1; i<=this.nbHTiles; i++) {
			for (j=1; j<=this.nbVTiles; j++) {
				var t = this.tiles[i][j];
				var h = t.getHTML(ratio);
				el.appendChild(h);
			}
		}
	},
	
	/**
	 * Cache la grille principale
	 */
	hideMainGrid: function(){
		if(this.mainGrid){
			this.mainGrid.setStyle('visibility', 'hidden');
		}
	},
	
	/**
	 * Cache la grille de l'overview
	 */
	hideOverViewGrid: function(){
		if(this.overviewGrid){
			this.overviewGrid.setStyle('visibility', 'hidden');
		}
	},
	
	/**
	 * Cache entierement les grilles. La grille de l'overview, la grille principale et les boutons de navigation
	 */
	hide: function(){
		this.hideMainGrid();
		this.hideOverViewGrid();
		this.removeAllNavigationControl();
		this.active = false;
	},
	
	/**
	 * Affiche entierement les grilles. La grille de l'overview, la grille principale.
	 */
	show: function(){
		this.showMainGrid();
		this.showOverViewGrid();
		this.active = true;
	},
	
	/**
	 * Affiche uniquement la grille principale
	 */
	showMainGrid: function(){
		// Le div où la grille s'insère
		var p = this.viewer.frame.panel.div;
		if(this.mainGrid){
			this.mainGrid.setStyle('visibility', 'visible');
		}else{	
			this.mainGrid = this.createGrid(p);
			// Le ratio entre l'image affichée et l'originale
			var r = ((this.viewer.frame.panel.getZoom() / 100 )* this.image.currentVersion.width ) / this.image.originalVersion.width
			this.drawGrid(this.mainGrid,r);
		}
	},
	
	/**
	 * Affiche uniquement la grille de l'overview
	 */
	showOverViewGrid: function(){
		var o = this.viewer.overview.div;
		this.viewer.overview.hideMask();
		if(this.overviewGrid){
			this.overviewGrid.setStyle('visibility', 'visible');
		}else{
			this.overviewGrid = this.createGrid(o);
			var r2 = o.getWidth() / this.image.originalVersion.width;
			this.drawGrid(this.overviewGrid,r2);
		}
	},
	
	/**
	 * Genère une grille que l'on insére dans l'element passé en paramètre
	 * @param {Element} p
	 */
	createGrid: function(p){
		// Un div pour contenir la grille
		var d = new Element('div');
		d.setAttribute("id", p.get('id') + '-piv-mosaic-grid');
		d = $(d);
		//d.makePositioned();
		d.setStyles({
			'z-index': 10,
			position: 'absolute',
			left:0,
			top:0
		});
		
		// On insère le nouveau div dans le panel
		p.appendChild(d);
		return d;
	},

	/**
	* Retourne la tuile au-dessus d'une tuile passée en paramètre.
	* @param {PivGridTile} t
	* @return {PivGridTile}
	*/
	above: function(t) {
		if ( t.j > 1 ) return this.tiles[t.i][t.j-1];
		else return undefined;
	},

	/**
	*	Retourne la tuile à gauche d'une tuile passée en paramètre.
	*	@param {PivGridTile} t
	* 	@return {PivGridTile}
	*/
	left: function(t) {
		if ( t.i > 1 ) return this.tiles[t.i - 1][t.j];
		else return undefined;
	},

	/**
	*	Retourne la tuile en-dessous d'une tuile passée en paramètre.
	*	@param {PivGridTile} t
	* 	@return {PivGridTile}
	*/
	below: function(t) {
		if ( t.j < this.nbVTiles ) return this.tiles[t.i][t.j+1];
		else return undefined;
	},

	/**
	*	Retourne la tuile à droite d'une tuile passée en paramètre.
	*	@param {PivGridTile} t
	* 	@return {PivGridTile}
	*/
	right: function(t) {
		if ( t.i < this.nbHTiles ) return this.tiles[t.i + 1][t.j];
		else return undefined;
	},
	
	/**
	 * @return {String} retourne la classe des tuiles
	 */ 
	identify: function() {
		return 'Classe PivGrid';
	},
	
	/**
	 * Action lorsque l'on click sur un bouton de navigation
	 * @param {Grid} grid
	 * @parem {PivGridTile} nextTile La tuile que l'on va afficher
	 */
	handleBtnGridNavigationClick: function(grid,nextTile) {
		return function() {
			grid.viewer.displayVersion(nextTile.getVersion());
			//alert(nextTile.i + "," + nextTile.j);
		};
	},
	
	/**
	 * Retourne le bouton de navigation en fonction de sa position
	 * @param {String} pos
	 * @return {Element} Element Le div contenant le bouton de navigation avec le bon css
	 */
	getTileNavigationControl: function(pos) {
		var d = $(window.document.createElement('div'));
		d.setAttribute('id', 'piv-mosaic-navigation-' + pos);
		d.addClass('piv-mosaic-navigation-' + pos);
		return d;
	},
	
	/**
	 * Supprime un bouton de navigation en fonction de sa position
	 * @param {String} pos
	 */
	removeNavigationControl: function(pos) {
		var d = $('piv-mosaic-navigation-' + pos);
		if(d){
			d.dispose();
		}
	},
	/**
	 * Chargement des tuiles adjacentes
	 */
	loadOtherTile: function(e){
		var c = this.series.currentImage.currentVersion.tile;
		var next = this.grid.right(c);
		/*console.log('ratio '+(this.frame.panel.getZoom() / 100));
		console.log('*** CURENT TILE *******');
		console.log('tille URL '+c.getUrl());
		console.log('tille URL '+c.x);
		console.log('tille URL '+c.y);*/
		if(next) {
			this.grid.addOtherTile(c,next);
		}
		next = this.grid.left(c);
		if(next) {
			this.grid.addOtherTile(c,next);
		}
		next = this.grid.above(c);
		if(next) {
			this.grid.addOtherTile(c,next);
		}
		next = this.grid.below(c);
		if(next) {
			this.grid.addOtherTile(c,next);
		}
	},
	/**
	 * Ajout les tuiles adjacentes
	 * @param {PivGridTile} currentTile
	 * @param {PivGridTile} tile La tuile que l'on souhaite charger
	 */
	addOtherTile: function(currentTile, tile){
		var p = this.viewer.frame.panel.div;
		var r = ((this.viewer.frame.panel.getZoom() / 100 )* this.image.currentVersion.width ) / this.image.originalVersion.width
		var top = (tile.y - currentTile.y);
		var left = (tile.x - currentTile.x);
		/*console.log('*** TILE *******');
		console.log('tille top '+ top);
		console.log('tille left '+left);*/
		
		var d = new Element('img', {id: tile.getUrl()});
		d.src = tile.getUrl();
		d.setStyle('background-color','red');
		d.setStyle('position', 'absolute');
		d.setStyle('top', top );
		d.setStyle('left', left);
		d.setStyle('width',(currentTile.w));
		d.setStyle('height',(currentTile.h));
		
		p.adopt(d);
	},
	
	/**
	 * Savoir si la grille est active
	 * @return {boolean}
	 */
	isActive: function(){
		return this.active;
	}
});