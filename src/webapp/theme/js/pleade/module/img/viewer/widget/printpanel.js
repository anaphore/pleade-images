//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/**
 * @fileOverview Panneau d'impression
 * @author Julien Huet
 * @author Johan Cwiklinski
 */

/**
 * @class Panneau d'impression
 * @author Julien Huet
 * @author Johan Cwiklinski
 * @augments CWindow
 */
PrintPanel = new Class(/** @lends PrintPanel*/{
	Extends: CWindow,

	/** Instance de la visionneuse auquel le menu est rattaché
	* @type pivViewer
	*/
	viewer: null,

	/** L'element du dom contenant le menu d'impression
	* @type Element
	*/
	mnuPrint: null,

	/**
	* <strong>Constructeur</strong>
	* Conteneur pour le formulaire d'impression
	* @param {pivViewer} viewer
	*/
	initialize: function(viewer){
		this.viewer = viewer;
		if ( $('piv-print-menu') != null ) {//if we do not have the required element, we do nothing
			this.mnuPrint = $('piv-print-menu');
			this.parent({
				container: this.viewer.frame.div,
				foreground: false,
				background: false,
				defaultColor: 'transparent',
				resizable: false,
				enableHeader:false,
				closable: false,
				draggable: false,
				isSlide: 'vertical',
				slideId: 'piv-print-menu'
			});
			this.render();
			this.container.adopt(this.mnuPrint);
			// FIXME trouver une meilleur facon d'attacher le panneau d'impression.
			this.viewer.frame.div.adopt(this.container);
			this.slide.hide();
			this.mnuPrint.removeClass('invisible');
			this.mnuPrint.setStyle('white-space','nowrap');
			this.container.setPosition({x: 0, y: 0});

			//action sur l'envoi du formulaire
			$$( '#piv-print-menu form' ).addEvent('submit', this.onPrintFormSubmit.bind(this));
		}
	},

	/** Surcharge de la methode de suppresion de la fenetre (on la masque seulement) */
	destroy: function(){
		this.slide.hide();
		this.container.hide();
		this.viewer.toolbar.btnPrint.removeClass('checked');
	},

	/**
	* Action lorsque l'on valide le formulaire d'impression
	* @param {Event} e
	*/
	onPrintFormSubmit: function(e){
		var items = [];
		var _url = this.getPrintUrl();
		window.open(_url);
		e.stop();
	},
		
	/**
	* Fonction permettant de récupérer l'url de géneration du pdf d'impression
	*/
	getPrintUrl: function(){
		//tableau des formats supportés
		_format = this.getPrintFormat();
		var _formatUrl = '';
		if(_format){
			_formatUrl = '?format=' + _format;
		}
		var region = this.viewer.frame.panel.getOriginalRegion();
		var _print_type = $$('input[name=piv-print-img-type]:checked');
		_url = this.viewer.series.baseUrl + '/' + this.viewer.series.currentImage.name + '.pdf' + _formatUrl;
		switch (_print_type[0].get('value')) {
		case 'showed':
			if ( !_format ) {
				_url += '?';
			} else {
				_url += '&';
			}
			_url += 'x=' + ( (region.left) ? Math.floor(region.left) : 0 ) +
			'&y=' + ( (region.top) ? Math.floor(region.top) : 0 ) +
			'&w=' + ( (region.width) ? Math.ceil(region.width) : '' ) +
			'&h=' + ( (region.height) ? Math.ceil(region.height) : '' ) +
			'&r=' + this.viewer.series.currentImage.getRotation();
			break;
		default:
			_url += '';
			break;
		}
		return _url;
	},

	/**
	* Fonction permettant de connaitre le format d'impression
	*/
	getPrintFormat: function(){},

	/**
	* Affiche le panneau d'impression
	*/
	show: function() {
		_sizes = this.mnuPrint.getSize();
    _x_pos = ( this.viewer.toolbar.btnDl ) ? this.viewer.toolbar.btnDl.getPosition().x : this.viewer.toolbar.btnPrint.getPosition().x;
    _x_size = ( this.viewer.toolbar.btnDl ) ? this.viewer.toolbar.btnDl.getSize().x : this.viewer.toolbar.btnPrint.getSize().x;
		this.setHeight(_sizes.y);
		this.setWidth(_sizes.x);
		this.container.setPosition({x: _x_pos + _x_size - _sizes.x, y: 0});
		this.parent();
	}

});