//Copyright (C) 2003-2011 AJLSM, Anaphore
//Voir le fichier LICENCE
//$Id$

/* Code Javascript pour le serveur d'images, en particulier la partie admin */

/**
*	Initialisation d'une page de l'explorateur, en particulier les onglets.
*/
function initExplorer() {
	
	// Création des onglets
	var tabs = new YAHOO.widget.TabView("pl-is-explore-tabs");
	
	// Création des datatable YUI

	// Un pointeur sur chaque tableau HTML
	var dirs = $("tbl-directories");
	var images = $("tbl-images");
	var files = $("tbl-files");
	var hpp = 20;
	var showPaginator = false;
	
	// Le datatable des sous-dossiers
	if ( dirs ) {
		var dirsColumnDef = [
			{ key: "name", label: _usersMessages.admin_explore_tbl_header_name, sortable: true },
			{ key: "date", label: _usersMessages.admin_explore_tbl_header_date, sortable: true }
		];
		var dirsDatasource = new YAHOO.util.DataSource(dirs);
		dirsDatasource.responseType = YAHOO.util.DataSource.TYPE_HTMLTABLE;
		dirsDatasource.responseSchema = {
			fields: [
				{ key: "name" },
				{ key: "date" }
			]
		};
		if ( dirs.getElementsBySelector("tr").size > hpp ) showPaginator = true;
		var dirsDatatable = new YAHOO.widget.DataTable("div-tbl-directories", dirsColumnDef, dirsDatasource, {});
	}
	
	// Le datatable des images
	if ( images ) {
		var imagesColumnDef = [
			{ key: "name", label: _usersMessages.admin_explore_tbl_header_name, sortable: true, resizeable: true },
			{ key: "date", label: _usersMessages.admin_explore_tbl_header_date, sortable: true, resizeable: true},
			{ key: "size_kb", label: _usersMessages.admin_explore_tbl_header_size_kb, sortable: true, resizeable: true, formatter: frNumberFormatter},
			{ key: "size_mb", label: _usersMessages.admin_explore_tbl_header_size_mb, sortable: true, resizeable: true, formatter: frNumberFormatter },
			{ key: "width", label: _usersMessages.admin_explore_tbl_header_width, sortable: true, resizeable: true, formatter: frNumberFormatterInt },
			{ key: "height", label: _usersMessages.admin_explore_tbl_header_height, sortable: true, resizeable: true, formatter: frNumberFormatterInt },
			{ key: "mimetype", label: _usersMessages.admin_explore_tbl_header_mimetype, sortable: true, resizeable: true },
			{ key: "links", label: _usersMessages.admin_explore_tbl_header_links, sortable: false, resizeable: true }
		];
		var imagesDatasource = new YAHOO.util.DataSource(images);
		imagesDatasource.responseType = YAHOO.util.DataSource.TYPE_HTMLTABLE;
		imagesDatasource.responseSchema = {
			fields: [
				{ key: "name" },
				{ key: "date" },
				{ key: "size_kb", parser: frNumberParser },
				{ key: "size_mb", parser: frNumberParser },
				{ key: "width", parser: frNumberParser },
				{ key: "height", parser: frNumberParser },
				{ key: "mimetype" },
				{ key: "links" }
			]
		};
		if ( images.getElementsBySelector("tr").length > hpp ) showPaginator = true;
		var paConfig = {
			rowsPerPage: hpp,
			nextPageLinkLabel : _usersMessages.admin_explore_paginator_next_page,
			previousPageLinkLabel : _usersMessages.admin_explore_paginator_previous_page,
			firstPageLinkLabel : _usersMessages.admin_explore_paginator_first_page,
			lastPageLinkLabel : _usersMessages.admin_explore_paginator_last_page
		};
		var pa = new YAHOO.widget.Paginator( paConfig );
		//var imagesDatatable = new YAHOO.widget.DataTable("div-tbl-images", imagesColumnDef, imagesDatasource, {paginated: showPaginator, paginator: {rowsPerPage: hpp}, scrollable: true, sortedBy: {key: "name", dir: "asc"}});
		var imagesDatatable = new YAHOO.widget.DataTable("div-tbl-images", imagesColumnDef, imagesDatasource, {paginator : pa, scrollable: true, sortedBy: {key: "name", dir: "asc"}});
	}
	
	// Le datatable des autres fichiers
	if ( files ) {
		var filesColumnDef = [
			{ key: "name", label: _usersMessages.admin_explore_tbl_header_name, sortable: true },
			{ key: "date", label: _usersMessages.admin_explore_tbl_header_date, sortable: true },
			{ key: "size_kb", label: _usersMessages.admin_explore_tbl_header_size_kb, sortable: true, formatter: frNumberFormatter },
			{ key: "size_mb", label: _usersMessages.admin_explore_tbl_header_size_mb, sortable: true, formatter: frNumberFormatter }
		];
		var filesDatasource = new YAHOO.util.DataSource(files);
		filesDatasource.responseType = YAHOO.util.DataSource.TYPE_HTMLTABLE;
		filesDatasource.responseSchema = {
			fields: [
				{ key: "name" },
				{ key: "date" },
				{ key: "size_kb", parser: frNumberParser },
				{ key: "size_mb", parser: frNumberParser }
			]
		};
		if ( files.getElementsBySelector("tr").size > hpp ) showPaginator = true;
		var filesDatatable = new YAHOO.widget.DataTable("div-tbl-files", filesColumnDef, filesDatasource, {});
	}

	// On détermine lequel est actif
	/* if ( dirs ) tabs.getTab(0).initAttributes({active: true});
	else if ( images ) tabs.getTab(1).initAttributes({active: true});
	else tabs.getTab(2).initAttributes({active: true}); */
	
	// On désactive les onglets vides
	/* if ( !dirs ) tabs.getTab(0).initAttributes({disabled: true});
	if ( !images ) tabs.getTab(1).initAttributes({disabled: true});
	if ( !files ) tabs.getTab(2).initAttributes({disabled: true}); */
}

/**
*	Parse une chaîne qui représente un nombre en français, par exemple 1 234,489
*/
function frNumberParser(data) {
	return YAHOO.util.DataSource.parseNumber(data.gsub(',', '.').gsub(' ', ''));
}

/**
*	Formate un nombre pour l'afficher en français, par exemple 1 234,56
*/
function frNumberFormatter(el, oRecord, oColumn, oData) {
	el.innerHTML = formatNumber(oData, 2);
}
function frNumberFormatterInt(el, oRecord, oColumn, oData) {
	el.innerHTML = formatNumber(oData, 0);
}
function formatNumber(n, digits) {
    if (YAHOO.lang.isNumber(n)) {
		var myData = "" + n.toFixed(digits);
        return myData.gsub(/\./, ',');
		// TODO: espaces pour les milliers
    }
    else {
        return "" + n;
    }
}
