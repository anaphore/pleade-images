<?xml version="1.0" encoding="UTF-8"?>
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0" exclude-result-prefixes="xsl dir i18n xs"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:dir="http://apache.org/cocoon/directory/2.0"
		xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
		xmlns:xs="http://www.w3.org/2001/XMLSchema"
>

<!--
		xmlns:urle="java:java.net.URLEncoder"
		extension-element-prefixes="java"

-->

	<!-- FIXME:
			- les images avec une espace ne passent pas
			- les images redimensionnées ne pas affichées proportionnellement?
	-->

	<!-- L'URL de l'image -->
	<xsl:param name="url" select="''"/>

	<!-- Le chemin vers le theme courant -->
	<xsl:param name="url-theme" select="''"/>

	<!-- Les informations sur le lecteur -->
	<xsl:param name="lecteur" select="''"/>

	<!-- Les informations sur la cote -->
	<xsl:param name="cote" select="''"/>

	<!-- Les informations sur le titre -->
	<xsl:param name="titre" select="''"/>

	<!-- La date -->
	<xsl:param name="date" select="''"/>

	<!-- La locale -->
	<xsl:param name="lang" select="''"/>

	<!-- L'URL relative de l'image (ie, sans racine, etc.) -->
	<xsl:param name="url-relative-image" select="''" />

	<!-- L'un ou l'autre des paramètres suivants doit être fourni -->
	<!-- L'URL de base de l'image -->
	<xsl:param name="url-absolue-image" select="''"/>
	<!-- L'URL de base du dossier d'images -->
	<xsl:param name="url-absolue-dossier-images" select="''"/>

	<!-- La mention de droits -->
	<xsl:param name="droits" select="''"/>

	<!-- La résolution des l'image -->
	<xsl:param name="dpi" select="300"/>

	<!-- Le format (A5, A4 ou A3) TODO: à compléter...? -->
	<xsl:param name="formatParam" select="'A4'"/>

	<!-- Les paramètres de redimensionnement de l'image, en bloc -->
	<xsl:param name="query" select="''"/>

	<!-- La taille réelle de l'image si redimensionnée -->
	<xsl:param name="w" select="''"/>
	<xsl:param name="h" select="''"/>

	<xsl:variable name="format">
		<xsl:choose>
			<xsl:when test="$formatParam != ''"><xsl:value-of select="$formatParam"/></xsl:when>
			<xsl:otherwise><xsl:text>A4</xsl:text></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- Les marges en haut et en bas (1cm = 29 pts) -->
	<xsl:variable name="margin" select="29"/>

	<!-- La hauteur à laisser pour le texte en vertical -->
	<xsl:variable name="text-height" select="72"/>

	<xsl:variable name="_format" select="
			if($format!='') then $format else 'A4'
	" />

	<xsl:variable name="format_height">
		<xsl:choose>
			<xsl:when test="$_format = 'A5'">
				<xsl:value-of select="number(148)"/>
			</xsl:when>
			<xsl:when test="$_format = 'A3'">
				<xsl:value-of select="number(420)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="number(297)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="format_width">
		<xsl:choose>
			<xsl:when test="$_format = 'A5'">
				<xsl:value-of select="number(210)"/>
			</xsl:when>
			<xsl:when test="$_format = 'A3'">
				<xsl:value-of select="number(297)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="number(210)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- Les tailles de papier, une fois les marges enlevees (36pts), en points -->
	<xsl:variable name="papier-grand" select="(($format_height div 25.4) * 72) - (2 * $margin)"/>
	<xsl:variable name="papier-petit" select="(($format_width div 25.4) * 72) - (2 * $margin)"/>

	<!-- Le code du catalogue i18n à utiliser -->
	<xsl:variable name="i18n-catalogue" select="'module-img-server'"/>

	<!-- Déterminer l'orientation -->
	<xsl:template name="get-orientation">
		<xsl:param name="image" select="."/>
		<xsl:choose>
			<xsl:when test="$w != '' and $h != ''">
				<!-- On a un redimensionnement (une seule image) -->
				<xsl:choose>
					<xsl:when test="number($w) > number($h)">landscape</xsl:when>
					<xsl:otherwise>portrait</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="number($image/@width) > number($image/@height)">landscape</xsl:when>
			<xsl:otherwise>portrait</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- L'orientation du papier est déterminée par la première image -->
	<xsl:variable name="or">
		<xsl:call-template name="get-orientation">
			<xsl:with-param name="image" select="/dir:directory/dir:file[1]"/>
		</xsl:call-template>
	</xsl:variable>

	<!-- Les tailles maximales, en points, en largeur et hauteur -->
	<xsl:variable name="maxwidth">
		<xsl:choose>
			<xsl:when test="$or = 'landscape'"><xsl:value-of select="$papier-grand"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$papier-petit"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="maxheight">
		<xsl:choose>
			<xsl:when test="$or = 'landscape'"><xsl:value-of select="$papier-petit - $text-height"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$papier-grand - $text-height"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- Le ratio largeur / hauteur de l'espace disponible pour afficher -->
	<xsl:variable name="page-ratio" select="number($maxwidth) div number($maxheight)"/>

	<!-- La racine du document -->
	<xsl:template match="/dir:directory">
		<!-- La racine du document PDF, en définissant la taille de papier et l'orientation -->
		<itext orientation="{$or}" pagesize="{$_format}" left="{$margin}" right="{$margin}" top="{$margin}" bottom="{$margin}">
			<xsl:apply-templates select="dir:file"/>
		</itext>
	</xsl:template>

	<!-- Une image -->
	<xsl:template match="dir:file">

		<!-- Nouvelle page si pas première -->
		<xsl:if test="position() > 1">
			<newpage/>
		</xsl:if>

		<!-- Test d'en-tête -->
		<!-- FIXME: on peut faire des tableaux, mais il faut déterminer la largeur relative des colonnes,
					et celles-ci ne s'ajustent pas au texte, malheureusement. -->
		<!-- <table columns="3" width="100%" align="center" cellpadding="5.0" cellspacing="5.0" widths="1;98;1">
			<row>
				<cell horizontalalign="Left">Cellule 1</cell>
				<cell horizontalalign="Center">Une plus longue cellule pour voir... Cellule 2</cell>
				<cell horizontalalign="Right">Cellule 3</cell>
			</row>
		</table> -->

		<xsl:variable name="extra">
			<xsl:if test="$query != ''"><xsl:value-of select="concat('?', $query)"/></xsl:if>
		</xsl:variable>

		<!-- Récupération du titre : soit il est en parametre de l'URL, soit il est retourné par la fonctione interne "infosimage" -->
		<xsl:variable name="t">
			<xsl:apply-templates select="." mode="get-title"/>
		</xsl:variable>

		<!-- En-tête -->
		<xsl:variable name="headerw">
			<xsl:choose>
				<xsl:when test="$or = 'landscape'">
					<xsl:value-of select="$papier-grand"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$papier-petit"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="headerh">
			<xsl:choose>
				<xsl:when test="$or='landscape'">30</xsl:when>
				<xsl:otherwise>30</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:call-template name="display-logo">
			<xsl:with-param name="w" select="$headerw"/>
			<xsl:with-param name="h" select="$headerh"/>
		</xsl:call-template>

		<paragraph size="9">
			<xsl:if test="$cote != ''">
				<xsl:value-of select="$cote"/>
				<xsl:text> &#x2014; </xsl:text>
			</xsl:if>
			<xsl:if test="$t != ''">
				<xsl:value-of select="$t"/>
				<!---<xsl:text> &#x2014; </xsl:text>-->
			</xsl:if>
			<!--<xsl:value-of select="$url"/>
			<xsl:if test="not(ends-with($url, '/'))"><xsl:text>/</xsl:text></xsl:if>
			<xsl:text>viewer.html</xsl:text>
			<xsl:value-of select="if ( $extra!='' ) then concat($extra,'&amp;') else '?'" />
			<xsl:value-of select="concat('img=',normalize-space(@name))" />-->
			<!-- <xsl:text> &#x2014; </xsl:text>
			<xsl:value-of select="substring-before($date, ' ')"/>
			<xsl:if test="$lecteur != ''">
				<xsl:text> &#x2014; </xsl:text>
				<xsl:value-of select="$lecteur"/>
			</xsl:if> -->
		</paragraph>

		<!-- Image -->
		<xsl:variable name="image-ratio">
			<xsl:choose>
				<xsl:when test="$w != '' and $h != ''"><xsl:value-of select="number($w) div number($h)"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="number(@width) div number(@height)"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Le nombre de points en largeur de l'image -->
		<xsl:variable name="pw">
			<xsl:choose>
				<xsl:when test="string($image-ratio)='NaN' or number($image-ratio) &lt;=0"><xsl:value-of select="number($maxwidth - 29)"/></xsl:when>
				<xsl:when test="$image-ratio > $page-ratio"><xsl:value-of select="number($maxwidth - 29)"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="($image-ratio div $page-ratio) * number($maxwidth - 29)"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="ph">
			<xsl:choose>
				<xsl:when test="string($image-ratio)='NaN' or number($image-ratio) &lt;=0"><xsl:value-of select="number($maxheight - 29)"/></xsl:when>
				<xsl:when test="$image-ratio > $page-ratio"><xsl:value-of select="($page-ratio div $image-ratio) * number($maxheight - 29)"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="number($maxheight - 29)"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<paragraph size="1">
			<xsl:variable name="url">
				<xsl:apply-templates select="." mode="get-url"/>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="$url!='' and $url != 'false'">
					<image url="{$url}{$extra}" plainwidth="{$pw}" plainheight="{$ph}" align="middle"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="size">12</xsl:attribute>
					<xsl:attribute name="align">center</xsl:attribute>
					<xsl:attribute name="color">red</xsl:attribute>
					<i18n:text key="pdf.image.path.notfound" catalogue="{$i18n-catalogue}">Chemin de l'image incorrect.</i18n:text>
				</xsl:otherwise>
			</xsl:choose>
		</paragraph>

		<!-- Pied de page -->
		<xsl:call-template name="display-footer"/>
	</xsl:template>

	<!--Construire le titre-->
	<xsl:template match="dir:file" mode="get-title">
		<xsl:choose>
			<xsl:when test="$titre!=''"><xsl:value-of select="$titre" /></xsl:when>
			<xsl:when test="@title != ''"><xsl:value-of select="@title"/></xsl:when>
			<xsl:when test="@path != ''">
				<xsl:variable name="h" select="concat('cocoon://functions/ead/infosimage.ajax?path=', @path, @name, '&amp;from=pdf')" />
				<xsl:if test="doc-available($h)">
					<xsl:value-of select="normalize-space(document($h))" />
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="h" select="concat('cocoon://functions/ead/infosimage.ajax?path=', @name, '&amp;from=pdf')" />
				<xsl:if test="doc-available($h)">
					<xsl:value-of select="normalize-space(document($h))" />
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--Construire l'URL de l'image-->
	<xsl:template match="dir:file" mode="get-url">
		<xsl:choose>
			<xsl:when test="@path != ''"><xsl:value-of select="concat($url-absolue-image,'/',@path,'/')"/><xsl:value-of select="@name"/></xsl:when>
			<xsl:when test="$url-absolue-image != ''">
				<xsl:choose>
					<!-- On teste si le chemin vers les images est correct -->
					<xsl:when test="doc-available(concat($url-absolue-image,'/',$url-relative-image, '.xml'))">
						<xsl:value-of select="concat($url-absolue-image,'/',$url-relative-image)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:message>[image2itext.xsl] Chemin non trouvé : <xsl:value-of select="concat($url-absolue-image,'/',$url-relative-image, '.xml')"/></xsl:message>
						<xsl:text>false</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="doc-available(concat($url-absolue-dossier-images,'/',$url-relative-image, '/image-transformation-rules.xml'))"><xsl:value-of select="concat($url-absolue-dossier-images,'/',$url-relative-image)"/><xsl:value-of select="@name"/></xsl:when>
			<xsl:otherwise>
				<xsl:message>[image2itext.xsl] Chemin non trouvé : <xsl:value-of select="concat($url-absolue-dossier-images,'/',$url-relative-image, '/image-transformation-rules.xml')"/></xsl:message>
				<xsl:text>false</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Affichage du logo : isoler pour la personnalisation de l'impression -->
	<xsl:template name="display-logo">
		<xsl:param name="h"/>
		<xsl:param name="w"/>
		<xsl:choose>
			<xsl:when test="doc-available(concat($url-theme, '/i18n/pleade.xml'))">
				<image url="{$url-theme}/images/enTetePDF_{$or}.png" plainwidth="{$w}" plainheight="{$h}"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message>L'image d'en-tête pour le PDF n'est pas disponible (<xsl:value-of select="concat($url-theme, '/images/enTetePDF_', $or, '.png')"/>)</xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Affichage du pied de page -->
	<xsl:template name="display-footer">
		<paragraph size="9" align="center">
			<xsl:value-of select="$droits"/>
			<xsl:text> &#x2014; </xsl:text>
			<xsl:value-of select="format-date(current-date(), '[Do] [MNn] [Y]', $lang, 'AD', $lang)"/>
		</paragraph>
	</xsl:template>

</xsl:stylesheet>
