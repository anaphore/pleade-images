<?xml version="1.0" encoding="UTF-8"?>
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0" exclude-result-prefixes="xsl dir fu xs"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dir="http://apache.org/cocoon/directory/2.0"
	xmlns:fu="org.apache.commons.io.FilenameUtils"
	xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<!-- Filtre un dir:directory en fonction d'une première et dernière image -->
	<!-- Les paramètres de première et dernière sont facultatifs -->

	<!-- Les paramètres passés par le sitemap -->
  <xsl:param name="img-start-name"/>
  <xsl:param name="img-end-name"/>

	<xsl:param name="p" select="1"/><!-- rang de la première image -->
	<xsl:param name="d" select="1"/><!-- rang de la dernière image -->
	<xsl:param name="ns" select="''"/><!-- nom de l'image significative -->
	<xsl:param name="np" select="''"/><!-- nom de la première image -->
	<xsl:param name="nd" select="''"/><!-- nom de la dernière image -->
	<xsl:param name="name" select="''"/><!-- nom de l'unique image -->
	<xsl:param name="chemin-base" select="''" />
	<xsl:param name="chemin-serie" select="''" />
	<xsl:param name="isPl2" select="''" /><!-- marqueur de syntaxe Pleade 2 / Navimages -->

  <xsl:param name="img-start">
    <xsl:if test="normalize-space($img-start-name) != ''">
      <xsl:number select="//dir:file[@name=$img-start-name]"/>
    </xsl:if>
  </xsl:param>
  <xsl:param name="img-end">
    <xsl:if test="normalize-space($img-end-name) != ''">
      <xsl:number select="//dir:file[@name=$img-end-name]"/>
    </xsl:if>
  </xsl:param>

	<xsl:variable name="pl2" as="xs:boolean" select="
		if($isPl2 castable as xs:boolean and $isPl2) then true()
		else if($isPl2 castable as xs:boolean and not($isPl2)) then false()
		else if($isPl2!='') then true()
		else false()
	" />

	<!-- Pour tenir compte de l'absence potentielle de paramètres, on se
			définit des variables pp et dd qui pointeront sur les réelles
			images de première et dernière -->
	<xsl:variable name="nb" select="count(/dir:directory/dir:file)" as="xs:integer"/>
	<xsl:variable name="rp">
		<xsl:choose>
			<xsl:when test="$np!='' and /dir:directory/dir:file[@name=$np]"><xsl:value-of select="count(/dir:directory/dir:file[@name=$np]/preceding-sibling::dir:file) + 1" /></xsl:when>
			<!-- Compatilibité Pleade 2 / Navimages -->
			<xsl:when test="$np!='' and $pl2 and /dir:directory/dir:file[fu:getBaseName(@name)=$np]"><xsl:value-of select="count(/dir:directory/dir:file[fu:getBaseName(@name)=$np]/preceding-sibling::dir:file) + 1" /></xsl:when>
			<xsl:when test="number($p) > 0 and number($p) &lt;= $nb">
				<xsl:value-of select="$p"/>
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="1"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="rd">
		<xsl:choose>
			<xsl:when test="$nd!='' and /dir:directory/dir:file[@name=$nd]"><xsl:value-of select="count(/dir:directory/dir:file[@name=$nd]/preceding-sibling::dir:file) + 1" /></xsl:when>
			<!-- Compatibilité Pleade 2 / Navimages -->
			<xsl:when test="$nd!='' and $pl2 and /dir:directory/dir:file[fu:getBaseName(@name)=$nd]"><xsl:value-of select="count(/dir:directory/dir:file[fu:getBaseName(@name)=$nd]/preceding-sibling::dir:file) + 1" /></xsl:when>
			<xsl:when test="number($d) >= $rp and number($d) &lt;= $nb">
				<xsl:value-of select="$d"/>
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="$nb"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!--<xsl:variable name="pp">
		<xsl:choose>
			<xsl:when test="$p != ''">
				<xsl:choose>
					<xsl:when test="/dir:directory/dir:file[@name = $p]"><xsl:value-of select="$p"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="/dir:directory/dir:file[1]/@name"/></xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="/dir:directory/dir:file[1]/@name"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="dd">
		<xsl:choose>
			<xsl:when test="$d != ''">
				<xsl:choose>
					<xsl:when test="/dir:directory/dir:file[@name = $d]"><xsl:value-of select="$d"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="/dir:directory/dir:file[last()]/@name"/></xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="/dir:directory/dir:file[last()]/@name"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>-->

	<!-- Le dossier à traiter -->
	<xsl:template match="/dir:directory">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="chemin-base">
				<xsl:value-of select="
					if($chemin-base!='') then $chemin-base
					else if(@chemin-base!='') then @chemin-base
					else ''
				" />
			</xsl:attribute>
			<xsl:attribute name="chemin-serie">
				<xsl:value-of select="
					if($chemin-serie!='') then $chemin-serie
					else if(@chemin-serie!='') then @chemin-serie
					else ''
				" />
			</xsl:attribute>
			<!-- On commence à l'image donnée par $pp -->
			<!-- <xsl:apply-templates select="dir:file[@name = $pp]"/> -->
			<xsl:choose>
				<xsl:when test="$name != ''">
					<xsl:apply-templates select="dir:file[@name = $name]"/>
				</xsl:when>
				<xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$img-start castable as xs:integer">
              <xsl:apply-templates select="dir:file[position() >= number($rp + $img-start -1) and position() &lt;= number($rd + $img-start -1)]"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:apply-templates select="dir:file[position() >= number($rp) and position() &lt;= number($rd)]"/>
            </xsl:otherwise>
          </xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>

	<!-- Une image -->
	<xsl:template match="dir:file">
		<!-- On la copie d'abord -->
		<xsl:copy>
			<xsl:choose>
			  <xsl:when test="$ns!='' and @name=$ns"><xsl:attribute name="initial"><xsl:text>yes</xsl:text></xsl:attribute></xsl:when>
				<!-- Compatibilité Pleade 2 / Navimages -->
			  <xsl:when test="$ns!='' and $pl2 and fu:getBaseName(@name)=$ns"><xsl:attribute name="initial"><xsl:text>yes</xsl:text></xsl:attribute></xsl:when>
			</xsl:choose>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
		<!-- Si ce n'est pas la dernière, on traite la suivante -->
		<!-- <xsl:if test="@name != $dd">
			<xsl:apply-templates select="following-sibling::dir:file[1]"/>
		</xsl:if> -->
	</xsl:template>


	<!-- Pour copier du contenu... -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
