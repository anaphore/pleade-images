<?xml version="1.0" encoding="UTF-8"?>
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dir="http://apache.org/cocoon/directory/2.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="dir xs">

	<!-- Le chemin de la série -->
	<xsl:param name="path" select="''"/>

	<!-- Le nom du fichier à afficher -->
	<xsl:param name="name" select="''"/>

	<!-- Le numéro de l'image à afficher -->
	<xsl:param name="no" select="''"/>

	<!-- Le rang de l'image marquant le début de la série -->
	<xsl:param name="p" select="''"/>
	<!-- Le rang de l'image marquant la fin de la série -->
	<xsl:param name="d" select="''"/>
	<!-- Le nom de l'image marquant le début de la série -->
	<xsl:param name="np" select="''"/>
	<!-- Le nom de l'image marquant la fin de la série -->
	<xsl:param name="nd" select="''"/>

	<!-- On intercepte la racine du dossier -->
	<xsl:template match="/dir:directory">
		<!-- On détermine l'image à afficher -->
		<xsl:variable name="imgNo">
			<xsl:choose>
				<xsl:when test="dir:file[@initial='yes']"><xsl:value-of select="count(dir:file[@initial='yes']/preceding::dir:file) + 1" /></xsl:when>
				<xsl:when test="$name = '' and $no = ''">1</xsl:when>	<!-- Par défaut -->
				<xsl:when test="$name!='' and number(dir:file/@initial) castable as xs:integer"><xsl:value-of select="number(dir:file/@initial)"/></xsl:when><!-- Une seule image dans la série, elle apporte avec elle l'information qu'on souhaite -->
				<!--<xsl:when test="dir:file[@name = $name]"><xsl:value-of select="count(dir:file[@name = $name]/preceding-sibling::dir:file) + 1"/></xsl:when>-->
				<xsl:when test="number($no)"><xsl:value-of select="$no"/></xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="first" select="
			if (number($p)&gt;0) then $p
			else if ($name != '') then $imgNo
			else number(-1)
		"/>
		<xsl:variable name="last" select="
			if ( number($d)&gt;0 ) then $d
			else if ($name != '') then $imgNo
			else number(-1)
		"/>
		<series path="{$path}" size="{count(dir:file)}" initial="{$imgNo}" first="{$first}" last="{$last}" name-first="{$np}" name-last="{$nd}" name="{$name}"/>
	</xsl:template>

</xsl:stylesheet>
