<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		XSLT qui permet d'obtenir des informations complètes pour une
		seule image, potentiellement redimensionnée.
		
		Le XML sorti est:
		
			<image
				relative-url=""
				relative-url-wihtout-r=""
				original-width=""
				original-height=""
				original-orientation="h|v"
				rotation=""
				width=""
				height=""
				orientation="h|v"
			/>
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dir="http://apache.org/cocoon/directory/2.0"
	exclude-result-prefixes="xsl dir">
	
	<!-- Les paramèetres d'URL, passés par le sitemap -->
	<xsl:param name="w"/>
	<xsl:param name="h"/>
	<xsl:param name="x"/>
	<xsl:param name="y"/>
	<xsl:param name="r"/>
	
	<!-- Des informations sur l'image -->
	<xsl:param name="path"/>
	<xsl:param name="name"/>

	<!-- La racine de la liste des images, déjà filtrée -->
	<xsl:template match="/dir:directory">
		<images nb="1">
			<xsl:apply-templates select="dir:file"/>
		</images>
	</xsl:template>
	
	<!-- L'image en question -->
	<xsl:template match="dir:file">
		<!-- La partie query de l'URL de l'image, sans r -->
		<xsl:variable name="q">
			<xsl:if test="normalize-space(concat($w, $h, $x, $y)) != ''">
				<xsl:text>?</xsl:text>
				<xsl:if test="$w != ''">w=<xsl:value-of select="$w"/></xsl:if>
				<xsl:if test="$h != ''">
					<xsl:if test="$w != ''">&amp;</xsl:if>
					<xsl:text>h=</xsl:text><xsl:value-of select="$h"/>
				</xsl:if>
				<xsl:if test="$x != ''">
					<xsl:if test="$w != '' or $h != ''">&amp;</xsl:if>
					<xsl:text>x=</xsl:text><xsl:value-of select="$x"/>
				</xsl:if>
				<xsl:if test="$y != ''">
					<xsl:if test="$w != '' or $h != '' or $x != ''">&amp;</xsl:if>
					<xsl:text>y=</xsl:text><xsl:value-of select="$y"/>
				</xsl:if>
			</xsl:if>
		</xsl:variable>
		<!-- La partie query de l'URL de l'image, avec r -->
		<xsl:variable name="qr">
			<xsl:value-of select="$q"/>
			<xsl:if test="$r != ''">
				<xsl:choose>
					<xsl:when test="$q != ''">&amp;</xsl:when>
					<xsl:otherwise>?</xsl:otherwise>
				</xsl:choose>
				<xsl:text>r=</xsl:text><xsl:value-of select="$r"/>
			</xsl:if>
		</xsl:variable>
		<!-- L'orientation originale -->
		<xsl:variable name="oo">
			<xsl:choose>
				<xsl:when test="@width > @height">h</xsl:when>
				<xsl:otherwise>v</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Les nouvelles dimensions -->
		<xsl:variable name="nw">
			<xsl:choose>
				<xsl:when test="$w != ''"><xsl:value-of select="$w"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="@width"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="nh">
			<xsl:choose>
				<xsl:when test="$h != ''"><xsl:value-of select="$h"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="@height"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- La nouvelle orientation -->
		<xsl:variable name="no">
			<xsl:choose>
				<xsl:when test="$nw > $nh">h</xsl:when>
				<xsl:otherwise>v</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- La rotation demandée -->
		<xsl:variable name="rotation">
			<xsl:choose>
				<xsl:when test="$r = ''">0</xsl:when>
				<xsl:otherwise><xsl:value-of select="$r"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<image
			relative-url="{$path}/{$name}{$qr}"
			relative-url-without-r="{$path}/{$name}{$q}"
			original-width="{@width}"
			original-height="{@height}"
			original-orientation="{$oo}"
			rotation="{$rotation}"
			width="{$nw}"
			height="{$nh}"
			orientation="{$no}"
		/>
	</xsl:template>

</xsl:stylesheet>
