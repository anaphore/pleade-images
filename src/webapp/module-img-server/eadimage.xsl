<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
	On envoie ici les informations dont a besoin la fenêtre EAD pour afficher
	l'image et son lien.

	Les informations renvoyées sont fonction du image-transformation-rules.xml.
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dir="http://apache.org/cocoon/directory/2.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="xsl dir">

	<!-- Si on a un @pleade-img dans le document EAD, on le passe ici -->
	<xsl:param name="pleade-img"/>

	<xsl:param name="img-thumbnail-size" select="number(150)"/>

	<!-- Le @pleade:url -->
	<xsl:param name="pleade-url"/>

	<xsl:param name="sortie"/>

	<!-- Le chemin complet et absolu vers les images préparées -->

	<xsl:param name="img">
		<xsl:choose>
			<!-- Une image a été spécifiée, on prend celle là -->
			<xsl:when test="$pleade-img != ''"><xsl:value-of select="$pleade-img"/></xsl:when>
			<!-- Pas d'image spécifiée: on cherche et on prend la première -->
			<xsl:otherwise>
				<xsl:variable name="dirUrl" select="concat('cocoon://img-server/', $pleade-url, 'dir.xml')" />
				<xsl:variable name="first-image" select="if(doc-available($dirUrl)) then document($dirUrl)/dir:directory/dir:file[1]/@name else ()"/>
				<xsl:choose>
					<xsl:when test="$first-image">
						<xsl:value-of select="$first-image"/>
					</xsl:when>
					<!-- Il semble y avoir eu un souci quelquepart, on devrait récupérer une image.. On n'affiche rien, mais on logue -->
					<xsl:otherwise>
						<xsl:message>Impossible de trouver l'image qui illustre la série courante <xsl:value-of select="$pleade-url"/> ; un problème est peut être survenu.</xsl:message>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:param>

	<!-- URL relative à la fenêtre EAD du serveur d'images ==> traité dans la fenêtre EAD, c'est plus logique -->
	<!--<xsl:param name="public-img-server-base" select="'img-server/'"/>-->

	<xsl:variable name="vignette" select="/regles/regle/vignette"/>

	<!-- Taille par défaut des vignettes -->
	<xsl:variable name="default-size" select="if($img-thumbnail-size castable as xs:integer)
			then number($img-thumbnail-size)
			else number(150)" />

	<xsl:variable name="wsize">
		<xsl:choose>
			<!-- Si on a une taille spécifique pour les vignettes, on l'applique -->
			<xsl:when test="$vignette and $vignette/@largeur"><xsl:value-of select="$vignette/@largeur"/></xsl:when>
			<!-- Si rien n'est spécifié pour les vignettes, on applique la taille par défaut -->
			<xsl:otherwise><xsl:value-of select="$default-size"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="hsize">
		<xsl:choose>
			<!-- Si on a une taille spécifique pour les vignettes, on l'applique -->
			<xsl:when test="$vignette and $vignette/@hauteur"><xsl:value-of select="$vignette/@hauteur"/></xsl:when>
			<!-- Si rien n'est spécifié pour les vignettes, on applique la taille par défaut -->
			<xsl:otherwise><xsl:value-of select="$default-size"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="hasHidef" as="xs:boolean" select="
		if (/regles/regle/hidef and /regles/regle/hidef/@existe != '') then true()
			else false()
	" />

	<xsl:template match="/">
		<xsl:variable name="src">
			<!-- La source de l'image peut-être soit la miniature pré-générée ; soit l'image originale retaillée -->
			<xsl:if test="$vignette/@existe != ''"> <!-- Vignette pré-traitée: on passe par l'url adéquate -->
				<xsl:value-of select="concat('__thumbs__/', $vignette/@existe, '/')"/>
			</xsl:if>
			<xsl:value-of select="concat($pleade-url, $img)"/>
			<xsl:if test="not($vignette/@existe)"> <!-- Vignette non pré-traitée: on envoie les tailles pour qu'IM fasse son boulot -->
				<xsl:value-of select="concat('?w=', $wsize, '&amp;h=', $hsize)"/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="href">
			<!-- Le href peut-être soit le lien standard vers la visionneuse, soit le lien vers la visionneuse IIP en cas de présence de hidef -->
			<xsl:value-of select="concat($pleade-url, if(ends-with($pleade-url, '/')) then '' else '/', if($hasHidef) then 'iipviewer.html' else 'viewer.html')"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$sortie = 'json'">{src: "<xsl:value-of select="$src"/>", href: "<xsl:value-of select="$href"/>"}</xsl:when>
			<xsl:otherwise>
				<result>
					<eadimage src="{$src}" href="{$href}" find="{if ($img != '') then true() else false()}"/>
				</result>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
