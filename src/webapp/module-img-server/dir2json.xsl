<?xml version="1.0" encoding="UTF-8"?>
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dir="http://apache.org/cocoon/directory/2.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<!-- Numéro de l'image demandée (optionnel) -->
	<xsl:param name="n" select="0"/>

	<!-- Nom de l'image demandée (optionnel) -->
	<xsl:param name="iname" select="''"/>

	<!-- Image de départ (optionnel) -->
	<xsl:param name="startValue" select="0"/>

	<!-- Image de fin (optionnel) -->
	<xsl:param name="endValue" select="0"/>

	<!-- Un pointeur sur les règles de transformation d'images -->
	<xsl:variable name="rules" select="/root/regles"/>

	<!-- Elément racine -->
	<xsl:template match="/root">
		<!-- On débute par l'élément qui pointe sur la série d'images -->
		<xsl:apply-templates select="dir:directory"/>
	</xsl:template>

	<!-- Elément racine -->
	<xsl:template match="dir:directory">
		<xsl:copy>
			<xsl:text>[</xsl:text>
				<xsl:choose>
					<!-- Renvoie les informations d'une image en fonction de sa position -->
					<xsl:when test="number($n) > 0"><xsl:apply-templates select="dir:file[position() = number($n)]"><xsl:with-param name="no" select="$n"/></xsl:apply-templates></xsl:when>
					<!-- Renvoie les informations d'une image en fonction de son nom -->
					<xsl:when test="$iname != ''">
						<xsl:for-each select="dir:file">
							<xsl:choose>
								<xsl:when test="@name = $iname">
									<xsl:apply-templates select="."><xsl:with-param name="no" select="position()"/></xsl:apply-templates>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="number($startValue) > 0 and number($endValue) > 0 and number($endValue) > number($startValue)">
						<!--<xsl:message>Proceed from <xsl:value-of select="$startValue"/> to <xsl:value-of select="$endValue"/></xsl:message>-->
						<!-- On renvoie ici le nombre total d'éléments, pour le carousel -->
						<xsl:text>{ count: </xsl:text><xsl:value-of select="count(dir:file)"/><xsl:text>},</xsl:text>
						<xsl:call-template name="showRegion">
							<xsl:with-param name="v" select="$startValue"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise><xsl:apply-templates select="dir:file"/></xsl:otherwise>
				</xsl:choose>
			<xsl:text>&#xA;]</xsl:text>
		</xsl:copy>
	</xsl:template>

	<xsl:template name="showRegion">
		<xsl:param name="v"/>
		<!--<xsl:message>Into showRegion with v=<xsl:value-of select="$v"/></xsl:message>-->
		<xsl:if test="number($v) > number($startValue)"><xsl:text>,</xsl:text></xsl:if>
		<xsl:apply-templates select="dir:file[position() = number($v)]">
			<xsl:with-param name="no" select="$v"/>
		</xsl:apply-templates>
		<xsl:if test="number($v) &lt; number($endValue)">
			<xsl:call-template name="showRegion">
				<xsl:with-param name="v" select="number($v) + 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!-- Une image -->
	<xsl:template match="dir:file">
		<xsl:param name="no" select="position()"/>
		<xsl:text>&#xA;&#x9;{</xsl:text>
		<xsl:text>&#xA;&#x9;&#x9;no: </xsl:text>
		<xsl:value-of select="$no"/>
		<xsl:text>,</xsl:text>
		<xsl:text>&#xA;&#x9;&#x9;versions: [</xsl:text>
		<xsl:apply-templates select="image"/>
		<xsl:text>&#xA;&#x9;&#x9;]</xsl:text>
		<xsl:apply-templates select="mosaic"/>
		<xsl:text>&#xA;&#x9;}</xsl:text>
		<xsl:if test="position() != last()">,</xsl:if>
	</xsl:template>
	<xsl:template match="image">
		<xsl:text>&#xA;&#x9;&#x9;&#x9;{</xsl:text>
		<xsl:apply-templates select="@*" mode="property">
			<xsl:with-param name="prefix" select="'&#xA;&#x9;&#x9;&#x9;&#x9;'"/>
		</xsl:apply-templates>
		<xsl:text>&#xA;&#x9;&#x9;&#x9;}</xsl:text>
		<xsl:if test="position() != last()">,</xsl:if>
	</xsl:template>
	<xsl:template match="image/@*" mode="property">
		<xsl:param name="prefix" select="''"/>
		<xsl:variable name="n" select="name()"/>
		<xsl:value-of select="$prefix"/>
		<xsl:value-of select="name()"/>
		<xsl:text>:</xsl:text>
		<xsl:variable name="q"><xsl:text>"</xsl:text></xsl:variable>
		<xsl:variable name="isq" select="if( . castable as xs:integer ) then '' else $q " /><!-- des guillemets s'il s'agit de nombre -->
		<xsl:value-of select="concat($isq,.,$isq)" />
		<xsl:if test="position() != last()">,</xsl:if>
	</xsl:template>
	<xsl:template match="mosaic">
		<xsl:text>,</xsl:text>
		<xsl:text>&#xA;&#x9;&#x9;mosaic: {</xsl:text>
		<xsl:if test="@width">
			<xsl:text>&#xA;&#x9;&#x9;&#x9;width: </xsl:text>
			<xsl:value-of select="@width"/>
		</xsl:if>
		<xsl:if test="@height">
			<xsl:if test="@width">,</xsl:if>
			<xsl:text>&#xA;&#x9;&#x9;&#x9;height: </xsl:text>
			<xsl:value-of select="@height"/>
		</xsl:if>
		<!-- Ancienne version où les détails de la mosaïque sont fournis -->
		<!-- <xsl:apply-templates select="image"/> -->
		<xsl:text>&#xA;&#x9;&#x9;}</xsl:text>
	</xsl:template>

</xsl:stylesheet>
