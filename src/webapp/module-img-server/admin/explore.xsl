<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		Produit l'interface d'exploration des fichiers.
-->
<xsl:stylesheet version="1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
	xmlns:dir="http://apache.org/cocoon/directory/2.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="dir i18n fn pleade xs"
>

	<!-- Un paramètre qui indique le chemin actuellement exploré -->
	<xsl:param name="path"/>

	<!-- Un paramètre qui indique le nom de la page HTML qui est appelée (explore.html normalement) -->
	<xsl:param name="explorer"/>

	<!-- Un paramètre pour indiquer le nom de la page HTML de la visionneuse -->
	<xsl:param name="viewer"/>

	<!-- Un paramètre pour indiquer l'URL vers le module img-viewer -->
	<xsl:param name="img-viewer"/>

	<!-- Un paramètre pour indiquer l'URL vers le module img-server -->
	<xsl:param name="img-server"/>

	<!-- Un format pour les tailles -->
	 <xsl:decimal-format name="size" decimal-separator="," grouping-separator=" "/>

	<!-- L'élément racine est un aggregate -->
	<xsl:template match="/aggregate">
		<html>
			<head>
				<!-- Le titre -->
				<title><i18n:text key="title">pleade 3</i18n:text><i18n:text key="admin.title">- administration</i18n:text><i18n:text key="admin.explore.title">- explorateur</i18n:text> : <xsl:value-of select="$path"/></title>
				<!-- Les CSS spécifiques -->
				<link rel="stylesheet" type="text/css" href="css/module-img-server.css"/>
				<link rel="stylesheet" type="text/css" href="yui/assets/skins/pleade/tabview.css"/>
				<link rel="stylesheet" type="text/css" href="yui/assets/skins/pleade/datatable.css"/>
				<!-- Les .js spécifiques -->
				<script type="text/javascript" src="yui/tabview/tabview-min.js"> // </script>
				<script type="text/javascript" src="yui/datasource/datasource-min.js"> // </script>
				<script type="text/javascript" src="yui/datatable/datatable-min.js"> // </script>
				<script type="text/javascript" src="yui/paginator/paginator-min.js"> // </script>
				<script type="text/javascript" src="js/pleade/module/img/server/module-img-server.js"> // </script>
				<script type="text/javascript" src="i18n/module-img-server-js.js"> // </script>
				<script type="text/javascript">
					<!-- Initialisation des onglets -->
					window.addEvent("load", initExplorer);
				</script>
			</head>
			<body>
				<!-- On a deux dir:directory comme enfant, le premier est la liste de tous les
						fichiers et dossiers, le second est la liste des images -->
				<div id="pl-is-explore" class="pl-is-explore">
					<!-- On doit d'abord indiquer où l'on est afin de pouvoir remonter -->
					<xsl:call-template name="output-breadcrumb"/>
					<!-- Des variables pour contenir les différents éléments -->
					<xsl:variable name="dirs" select="dir:directory[1]/dir:directory"/>
					<xsl:variable name="images" select="dir:directory[2]/dir:file"/>
					<xsl:variable name="files" select="dir:directory[1]/dir:file"/>
					<!-- On crée un div pour le tabview YUI -->
					<div id="pl-is-explore-tabs" class="yui-navset">
						<!-- Les intitulés des onglets -->
						<ul class="yui-nav">
							<li>
								<xsl:if test="count($images) = 0 and count($dirs) > 0"><xsl:attribute name="class">selected</xsl:attribute></xsl:if>
								<a href="#tab-directories"><em><i18n:text key="admin.explore.tab.directories">sous-dossiers</i18n:text> (<xsl:value-of select="count($dirs)"/>)</em></a>
							</li>
							<li>
								<xsl:if test="count($images) > 0"><xsl:attribute name="class">selected</xsl:attribute></xsl:if>
								<a href="#tab-images"><em><i18n:text key="admin.explore.tab.images">images</i18n:text> (<xsl:value-of select="count($images)"/>)</em></a>
							</li>
							<li>
								<xsl:if test="count($images) = 0 and count($dirs) = 0"><xsl:attribute name="class">selected</xsl:attribute></xsl:if>
								<a href="#tab-files"><em><i18n:text key="admin.explore.tab.files">autres fichiers</i18n:text> (<xsl:value-of select="count($files)"/>)</em></a>
							</li>
						</ul>
						<!-- Les onglets eux-mêmes -->
						<div class="yui-content">
							<div id="tab-directories">
								<xsl:call-template name="output-table">
									<xsl:with-param name="id" select="'tbl-directories'"/>
									<xsl:with-param name="rows" select="$dirs"/>
								</xsl:call-template>
							</div>
							<div id="tab-images" class="yui-hidden">
								<xsl:call-template name="output-table">
									<xsl:with-param name="id" select="'tbl-images'"/>
									<xsl:with-param name="rows" select="$images"/>
								</xsl:call-template>
							</div>
							<div id="tab-files" class="yui-hidden">
								<xsl:call-template name="output-table">
									<xsl:with-param name="id" select="'tbl-files'"/>
									<xsl:with-param name="rows" select="$files"/>
								</xsl:call-template>
							</div>
						</div>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>

	<!-- Un template générique pour sortir un tableau -->
	<xsl:template name="output-table">
		<xsl:param name="rows"/>
		<xsl:param name="id"/>
		<xsl:choose>
			<xsl:when test="count($rows) > 0">
				<div id="div-{$id}">
					<table id="{$id}" class="pl-is-tbl-explore pl-is-{$id}" border="1">
						<thead>
							<xsl:apply-templates select="$rows[1]" mode="header"/>
						</thead>
						<tbody>
							<xsl:apply-templates select="$rows" mode="row"/>
						</tbody>
					</table>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<p><i18n:text key="admin.explore.empty.{$id}">aucune entrée</i18n:text></p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Des dossiers comme en-tête de tableau -->
	<xsl:template match="dir:directory" mode="header">
		<tr>
			<xsl:apply-templates select="@name" mode="header"/>
			<xsl:apply-templates select="@date" mode="header"/>
		</tr>
	</xsl:template>

	<!-- Des fichiers comme en-tête de tableau -->
	<xsl:template match="dir:file" mode="header">
		<tr>
			<xsl:apply-templates select="@name" mode="header"/>
			<xsl:apply-templates select="@date" mode="header"/>
			<xsl:apply-templates select="@size" mode="header"/>
			<xsl:apply-templates select="@width" mode="header"/>
			<xsl:apply-templates select="@height" mode="header"/>
			<xsl:apply-templates select="@mimetype" mode="header"/>
			<xsl:if test="@width">
				<th><i18n:text key="admin.explore.tbl.header.links">en-tête générique</i18n:text></th>
			</xsl:if>
		</tr>
	</xsl:template>

	<!-- Les en-têtes, selon les attributs -->
	<xsl:template match="@size" mode="header">
		<!-- Le cas particulier de la taille sortie deux fois -->
		<th><i18n:text key="admin.explore.tbl.header.size.kb">taille (ko)</i18n:text></th>
		<th><i18n:text key="admin.explore.tbl.header.size.mb">taille (mo)</i18n:text></th>
	</xsl:template>
	<xsl:template match="@*" mode="header">
		<th><i18n:text key="admin.explore.tbl.header.{local-name()}">en-tête générique</i18n:text></th>
	</xsl:template>

	<!-- Un dossier comme rangée de tableau -->
	<xsl:template match="dir:directory" mode="row">
		<tr>
			<xsl:apply-templates select="@name" mode="cell"/>
			<xsl:apply-templates select="@date" mode="cell"/>
		</tr>
	</xsl:template>

	<!-- Un fichier comme rangée de tableau -->
	<xsl:template match="dir:file" mode="row">
		<tr>
			<xsl:apply-templates select="@name" mode="cell"/>
			<xsl:apply-templates select="@date" mode="cell"/>
			<xsl:apply-templates select="@size" mode="cell"/>
			<xsl:apply-templates select="@width" mode="cell"/>
			<xsl:apply-templates select="@height" mode="cell"/>
			<xsl:apply-templates select="@mimetype" mode="cell"/>
			<!-- Pour les images, on ajoute une colonne pour y accéder -->
			<xsl:if test="@width">
				<td class="pl-is-explore-links">
					<select id="pl-is-explore-links-list" name="pl-is-explore-links-list" onchange="var url = this.options[this.selectedIndex].value; window.open(url); return false;">
						<option value=""><i18n:text key="admin.explore.links.first">-- sélectionner une version --</i18n:text></option>
						<xsl:for-each select="image">
							<xsl:variable name="label" select="@label"/>
							<xsl:variable name="href" select="concat($img-server, @src)"/>
							<option value="{$href}"><xsl:value-of select="$label"/></option>
						</xsl:for-each>
					</select>
				</td>
			</xsl:if>
		</tr>
	</xsl:template>

	<!-- Le nom du dossier: on fait un lien -->
	<xsl:template match="dir:directory/@name" mode="cell">
		<td class="pl-is-explore-name"><a href="{.}/{$explorer}"><xsl:value-of select="."/></a></td>
	</xsl:template>

	<!-- Le nom de l'image: on fait un lien vers l'image telle quelle -->
	<xsl:template match="dir:file[@width]/@name" mode="cell">
		<!-- <td class="pl-is-explore-name"><a href="{$img-viewer}{$path}/{$viewer}?img={.}"><xsl:value-of select="."/></a></td> -->
		<td class="pl-is-explore-name"><a href="{$img-server}{$path}/{.}"><xsl:value-of select="."/></a></td>
	</xsl:template>

	<!-- Des cellulles de tableau -->
	<xsl:template match="@size" mode="cell">
		<!-- Le cas particulier de la taille, sortie deux fois -->
		<td class="pl-is-explore-size"><xsl:value-of select="format-number(. div 1024, '# ###,00', 'size')"/></td>
		<td class="pl-is-explore-size"><xsl:value-of select="format-number(. div (1024 * 1024), '# ###,00', 'size')"/></td>
	</xsl:template>
	<xsl:template match="@*" mode="cell">
		<td class="pl-is-explore-{local-name()}"><xsl:value-of select="."/></td>
	</xsl:template>

	<!-- Sortie de l'endroit où l'on est -->
	<xsl:template name="output-breadcrumb">
		<div class="pl-is-explore-bc">
			<xsl:variable name="parts" select="fn:tokenize($path, '/')"/>
			<xsl:variable name="nb-parts" select="count($parts)"/>
      <xsl:variable name="agg" select="/aggregate" />
			<!-- INFO (MP) : le lien vers la racine qui ne fonctionne pas car le serveur d'images ne sait pas travailler sans indication de dossier racine.
      <span class="pl-is-explore-bc-roots"><a href="{concat(pleade:add-ancestors-url(0, $nb-parts), $explorer)}"><i18n:text key="admin.explore.bc.roots">racines</i18n:text></a></span>-->
      <span class="pl-is-explore-bc-roots"><i18n:text key="admin.explore.bc.roots">racines</i18n:text></span>
			<xsl:for-each select="$parts">
				<span class="pl-is-explore-bc-sep"><xsl:text> > </xsl:text></span>
				<xsl:choose>
					<xsl:when test="position() != last()">
						<xsl:variable name="url" select="concat(pleade:add-ancestors-url(position(), $nb-parts), $explorer)"/>
						<span class="pl-is-explore-bc-folder"><a href="{$url}"><xsl:value-of select="."/></a></span>
					</xsl:when>
          <!-- On active le lien d'ouverture de la visionneuse seulement s'il y a des images -->
          <xsl:when test="$agg/dir:directory[@sort='name']/dir:file">
            <span class="pl-is-explore-bc-folder-current"><xsl:value-of select="."/> [<a href="{$img-viewer}{$path}/{$viewer}"><i18n:text key="admin.explore.viewer">visionneuse</i18n:text></a>]</span>
          </xsl:when>
					<xsl:otherwise>
						<span class="pl-is-explore-bc-folder-current"><xsl:value-of select="."/></span>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</div>
	</xsl:template>

	<!-- Une fonction qui retourne des ../../ appropriés -->
	<xsl:function name="pleade:add-ancestors-url" as="xs:string">
		<xsl:param name="pos" as="xs:integer"/>
		<xsl:param name="nb" as="xs:integer"/>
		<xsl:variable name="ret">
			<xsl:for-each select="1 to xs:integer($nb - $pos)">../</xsl:for-each>
		</xsl:variable>
		<xsl:value-of select="normalize-space($ret)"/>
	</xsl:function>

</xsl:stylesheet>
