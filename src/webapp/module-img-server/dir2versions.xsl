<?xml version="1.0" encoding="UTF-8"?>
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dir="http://apache.org/cocoon/directory/2.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
>

	<!-- Traitement des règles de transformation des images -->

	<!-- Le format supporté est:
		<regles>
			<regle taille="" largeur="" hauteur="" surface="">
				<image role="" label="" hauteur="" largeur="" pourcentage="" qualite=""/>
				<mosaique largeur="" hauteur=""/>
			</regle>
		</regles>
	-->

	<!-- Le mode de sortie des informations de mosaïque : simple ou comple -->
	<xsl:param name="mosaic-mode" select="'simple'"/>

	<!-- Le rôle spécifique à afficher -->
	<xsl:param name="role" select="''"/>

	<!-- A-t-on des images pré traitées? -->
	<xsl:param name="images-preparees" select="''"/>

	<!-- Un pointeur sur les règles -->
	<xsl:variable name="rules" select="/root/regles"/>

	<!-- Le chemin de base (utile pour les images relatives). On s'assure d'avoir toujours un slash à la fin. -->
	<xsl:variable name="b" select="/root/dir:directory/@chemin-base" />
	<xsl:variable name="base-path" select="if($b!='') then concat($b, if(ends-with($b, '/')) then '' else '/') else ''" />
	<!-- Le chemin de la série. On s'assure d'avoir toujours un slash à la fin. -->
	<xsl:variable name="s" select="/root/dir:directory/@chemin-serie" />
	<xsl:variable name="serie-path" select="if($s!='') then concat($s, if(ends-with($s, '/')) then '' else '/') else ''" />

	<!-- L'élément racine -->
	<xsl:template match="/root">
		<!-- On lance le traitement par le dir:directory -->
		<xsl:apply-templates select="dir:directory"/>
	</xsl:template>

	<!-- L'élément qui pointe sur les dossiers -->
	<xsl:template match="dir:directory">
		<!-- On le copie -->
		<xsl:copy>
			<xsl:attribute name="generated"><xsl:value-of select="current-dateTime()" /></xsl:attribute>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

	<!-- Un fichier -->
	<xsl:template match="dir:file">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
			<xsl:if test="$role != 'original'">
				<xsl:call-template name="create-versions"/>
			</xsl:if>
			<xsl:if test="$role = '' or $role = 'original'">
				<!-- La dernière est toujours l'originale pour l'instant -->
				<xsl:call-template name="sortie-image">
					<xsl:with-param name="mimetype" select="@mimetype"/>
					<xsl:with-param name="width" select="@width"/>
					<xsl:with-param name="height" select="@height"/>
					<xsl:with-param name="size" select="@size"/>
					<xsl:with-param name="src" select="concat($base-path,$serie-path,@name)"/>
					<xsl:with-param name="name" select="@name"/>
					<xsl:with-param name="role" select="'original'"/>
					<xsl:with-param name="label" select="'Image originale'"/>	<!-- TODO: i18n -->
				</xsl:call-template>
			</xsl:if>
		</xsl:copy>
	</xsl:template>

	<!-- Création des versions pour une image -->
	<xsl:template name="create-versions">
		<xsl:choose>
			<xsl:when test="$role != '' and $role != 'original'">
				<xsl:apply-templates select="$rules/regle[image[@role=$role]] | $rules/regle[local-name(child::*) = $role]">
					<xsl:with-param name="image" select="."/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="$rules">
					<xsl:with-param name="image" select="."/>
				</xsl:apply-templates>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Template de copie -->
	<xsl:template match="comment()" priority="+2">
		<!-- Ne pas recopier les commentaires -->
	</xsl:template>
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

	<!-- On applique les règles de transformation pour une image -->
	<xsl:template match="regles">
		<!-- L'image à traiter -->
		<xsl:param name="image" select="."/>
		<!-- On applique simplement les règles définies en passant l'image -->
		<xsl:apply-templates>
			<xsl:with-param name="image" select="$image"/>
		</xsl:apply-templates>
	</xsl:template>

	<!-- Une règle de transformation -->
	<xsl:template match="regle">
		<!-- L'image sur laquelle on l'applique -->
		<xsl:param name="image" select="."/>

		<xsl:variable name="test-taille">
			<xsl:if test="@taille">
					<!-- Le comparateur -->
					<xsl:variable name="comp" select="substring(@taille, 1, 1)"/>
					<!-- La taille limite spécifiée dans la règle -->
					<xsl:variable name="taille" select="number(substring(@taille, 2))"/>
					<!-- La taille actuelle de l'image -->
					<xsl:variable name="size" select="$image/@size"/>	<!-- TODO: vérifier les unités ? -->
					<xsl:choose>
						<!-- On détermine $ok selon le comparateur -->
						<xsl:when test="$comp = '>'">
							<xsl:if test="$size > $taille">ok</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="$size &lt; $taille">ok</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="test-largeur">
			<xsl:if test="@largeur and $test-taille!='ok'">
					<!-- Le comparateur -->
					<xsl:variable name="comp" select="substring(@largeur, 1, 1)"/>
					<!-- La largeur limite spécifiée -->
					<xsl:variable name="largeur" select="number(substring(@largeur, 2))"/>
					<!-- La largeur réelle de l'image -->
					<xsl:variable name="width" select="$image/@width"/>
					<xsl:choose>
						<xsl:when test="$comp = '>'">
							<xsl:if test="$width > $largeur">ok</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="$width &lt; $largeur">ok</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="test-hauteur">
			<xsl:if test="@hauteur and $test-taille!='ok' and $test-largeur!='ok'">
					<!-- Le comparateur -->
					<xsl:variable name="comp" select="substring(@hauteur, 1, 1)"/>
					<!-- La hauteur limite spécifiée -->
					<xsl:variable name="hauteur" select="number(substring(@hauteur, 2))"/>
					<!-- La largeur réelle de l'image -->
					<xsl:variable name="height" select="$image/@height"/>
					<xsl:choose>
						<xsl:when test="$comp = '>'">
							<xsl:if test="$height > $hauteur">ok</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="$height &lt; $hauteur">ok</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="test-surface">
			<xsl:if test="@surface and $test-taille!='ok' and $test-largeur!='ok' and $test-hauteur!='ok'">
				<!-- Le comparateur -->
				<xsl:variable name="comp" select="substring(@surface, 1, 1)"/>
				<!-- La hauteur limite spécifiée -->
				<xsl:variable name="surface" select="number(substring(@surface, 2))"/>
				<!-- La largeur réelle de l'image -->
				<xsl:variable name="res" select="number($image/@height) * number($image/width)"/>
				<xsl:choose>
					<xsl:when test="$comp = '>'">
						<xsl:if test="$res > $surface">ok</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$res &lt; $surface">ok</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:variable>


		<!-- On vérifie d'abord si elle s'applique (si oui, $ok != "") -->
		<xsl:variable name="ok" select="if ( contains( concat($test-taille,$test-hauteur,$test-largeur,$test-surface), 'ok' ) ) then 'ok' else ''"/>

		<!-- Si on doit appliquer les règles, on y va ! -->
		<xsl:if test="$ok != ''">
			<xsl:apply-templates>
				<xsl:with-param name="image" select="$image"/>
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template>

	<!-- Une mosaïque d'images -->
	<xsl:template match="regle/mosaique">
		<xsl:param name="image" select="."/>
		<xsl:call-template name="sortie-mosaique">
			<xsl:with-param name="w" select="@largeur"/>
			<xsl:with-param name="h" select="@hauteur"/>
			<xsl:with-param name="image" select="$image"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="regle/image | regle/vignette | regle/miniature">
		<!-- L'image à traiter -->
		<xsl:param name="image" select="."/>
		<xsl:choose>
			<xsl:when test="not($role != '') and self::image">
				<xsl:call-template name="build-image">
					<xsl:with-param name="image" select="$image"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$role != ''">
				<xsl:call-template name="build-image">
					<xsl:with-param name="image" select="$image"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- Une image transformée -->
	<xsl:template name="build-image">
		<!-- L'image à traiter -->
		<xsl:param name="image" select="."/>

		<!-- La hauteur souhaitée -->
		<xsl:variable name="h">
			<xsl:choose>
				<xsl:when test="(@largeur and @hauteur) and (number($image/@width) &gt; number($image/@height))">
					<xsl:value-of select="ceiling((@largeur div $image/@width) * $image/@height)"/>
				</xsl:when>
				<xsl:when test="(@largeur and @hauteur) and not(number($image/@width) &gt; number($image/@height))">
					<xsl:value-of select="@hauteur"/>
				</xsl:when>
				<!-- On spécifie la largeur -->
				<xsl:when test="@largeur">
					<xsl:value-of select="ceiling((@largeur div $image/@width) * $image/@height)"/>
				</xsl:when>
				<!-- On spécifie la hauteur -->
				<xsl:when test="@hauteur">
					<xsl:value-of select="@hauteur"/>
				</xsl:when>
				<!-- On spécifie un pourcentage -->
				<xsl:when test="@pourcentage">
					<xsl:value-of select="ceiling(@pourcentage * $image/@height div 100)"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<!-- La largeur souhaitée -->
		<xsl:variable name="w">
			<xsl:choose>
				<xsl:when test="(@largeur and @hauteur) and (number($image/@width) &gt; number($image/@height))">
					<xsl:value-of select="@largeur"/>
				</xsl:when>
				<xsl:when test="(@largeur and @hauteur) and not(number($image/@width) &gt; number($image/@height))">
					<xsl:value-of select="ceiling((@hauteur div $image/@height) * $image/@width)"/>
				</xsl:when>
				<!-- On spécifie la largeur -->
				<xsl:when test="@largeur">
					<xsl:value-of select="@largeur"/>
				</xsl:when>
				<!-- On spécifie la hauteur -->
				<xsl:when test="@hauteur">
					<xsl:value-of select="ceiling((@hauteur div $image/@height) * $image/@width)"/>
				</xsl:when>
				<!-- On spécifie un pourcentage -->
				<xsl:when test="@pourcentage">
					<xsl:value-of select="ceiling(@pourcentage * $image/@width div 100)"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>

		<!-- La partie à ajouter pour la qualité -->
		<xsl:variable name="qualite">
			<xsl:if test="@qualite"><xsl:value-of select="concat('&amp;q=', @qualite)"/></xsl:if>
		</xsl:variable>

		<!-- L'URL de l'image, selon qu'on a spécifié la hauteur ou la largeur, la qualite, etc. -->
		<xsl:variable name="src">
			<xsl:choose>
			  <xsl:when test="$images-preparees!='' and not( contains(/root/dir:directory/@chemin-base, 'attached') or contains(/root/dir:directory/@chemin-base, 'relative') )">
					<xsl:value-of select="concat(
							'__thumbs__',
							(if(starts-with(@role,'/')) then '' else '/'),
							@role,
							(if(ends-with(@role,'/')) then '' else '/'),
							$serie-path, $image/@name )" />
				</xsl:when>
			  <xsl:otherwise>
					<!-- Si on a un chemin de base, on l'ajoute en faisant attention au slash final -->
					<!--<xsl:if test="$base-path!=''"><xsl:value-of select="concat($base-path, (if($base-path!='' and ends-with($base-path,'/')) then '' else if(starts-with($serie-path,'/')) then '' else '/'))" /></xsl:if>-->
					<xsl:value-of select="concat($base-path,$serie-path,$image/@name)" />
					 <xsl:choose>
						<xsl:when test="@largeur and @hauteur">
							<xsl:value-of select="concat('?w=', $w, '&amp;h=', $h, $qualite)"/>
						</xsl:when>
						<xsl:when test="@largeur">
							<xsl:value-of select="concat('?w=', $w, $qualite)"/>
						</xsl:when>
						<xsl:when test="@hauteur">
							<xsl:value-of select="concat('?h=', $h, $qualite)"/>
						</xsl:when>
						<xsl:when test="@pourcentage">
							<xsl:value-of select="concat('?percent=', @pourcentage, $qualite)"/>
						</xsl:when>
						<xsl:when test="@qualite">
							<xsl:value-of select="concat('?q=', @qualite)"/>
						</xsl:when>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="@filigrane">
				<xsl:variable name="ww" select="if( @filigrane-largeur castable as xs:integer ) then @filigrane-largeur else $w"/>
				<xsl:variable name="wh" select="if( @filigrane-hauteur castable as xs:integer ) then @filigrane-hauteur else $h"/>
				<xsl:value-of select="concat( ( if(not($images-preparees!='') and (@largeur | @hauteur | @pourcentage | @qualite)) then '&amp;' else '?' ),
																			'wk=', @filigrane, '&amp;ww=',$ww, '&amp;wh=',$wh )" />
			</xsl:if>
		</xsl:variable>

		<!-- Et on sort les spécifications de l'image en fonction des informations calculées -->
		<xsl:call-template name="sortie-image">
			<xsl:with-param name="mimetype" select="$image/@mimetype"/>
			<xsl:with-param name="width" select="$w"/>
			<xsl:with-param name="height" select="$h"/>
			<xsl:with-param name="src" select="$src"/>
			<xsl:with-param name="name" select="$image/@name"/>
			<xsl:with-param name="rule-width" select="@largeur"/>
			<xsl:with-param name="rule-height" select="@hauteur"/>
			<xsl:with-param name="role" select="@role"/>
			<xsl:with-param name="label" select="@label"/>
			<xsl:with-param name="watermark" select="@watermark"/>
		</xsl:call-template>
	</xsl:template>

	<!-- Sortie d'une mosaïque -->
	<xsl:template name="sortie-mosaique">
		<xsl:param name="image" select="."/>
		<xsl:param name="w"/>
		<xsl:param name="h"/>
		<xsl:choose>
			<xsl:when test="$mosaic-mode = 'full'">
				<mosaic size="{ceiling($image/@width div $w) * ceiling($image/@height div $h)}">
					<xsl:apply-templates select="$image" mode="tuile-h">
						<xsl:with-param name="nh" select="1"/>
						<xsl:with-param name="x" select="1"/>
						<xsl:with-param name="w" select="$w"/>
						<xsl:with-param name="h" select="$h"/>
					</xsl:apply-templates>
				</mosaic>
			</xsl:when>
			<xsl:otherwise>
				<mosaic>
					<xsl:if test="number($w)"><xsl:attribute name="width"><xsl:value-of select="$w"/></xsl:attribute></xsl:if>
					<xsl:if test="number($h)"><xsl:attribute name="height"><xsl:value-of select="$h"/></xsl:attribute></xsl:if>
				</mosaic>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- On parcourt horiaontalement et on sort une colonne de tuiles -->
	<xsl:template match="dir:file" mode="tuile-h">
		<xsl:param name="nh" select="1"/>
		<xsl:param name="x" select="1"/>
		<xsl:param name="w" select="100"/>
		<xsl:param name="h" select="100"/>
		<xsl:apply-templates select="." mode="tuile-v">
			<xsl:with-param name="nh" select="$nh"/>
			<xsl:with-param name="nv" select="1"/>
			<xsl:with-param name="x" select="$x"/>
			<xsl:with-param name="y" select="1"/>
			<xsl:with-param name="w" select="$w"/>
			<xsl:with-param name="h" select="$h"/>
		</xsl:apply-templates>
		<!-- On passe à la suivante si nécessaire -->
		<xsl:variable name="rw" select="@width"/>
		<xsl:if test="$x + $w &lt; $rw">
			<xsl:apply-templates select="." mode="tuile-h">
				<xsl:with-param name="nh" select="$nh + 1"/>
				<xsl:with-param name="x" select="$x + $w"/>
				<xsl:with-param name="w" select="$w"/>
				<xsl:with-param name="h" select="$h"/>
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template>

	<xsl:template match="dir:file" mode="tuile-v">
		<xsl:param name="nh" select="1"/>
		<xsl:param name="nv" select="1"/>
		<xsl:param name="x" select="1"/>
		<xsl:param name="y" select="1"/>
		<xsl:param name="w" select="100"/>
		<xsl:param name="h" select="100"/>
		<xsl:apply-templates select="." mode="tuile">
			<xsl:with-param name="nh" select="$nh"/>
			<xsl:with-param name="nv" select="$nv"/>
			<xsl:with-param name="x" select="$x"/>
			<xsl:with-param name="y" select="$y"/>
			<xsl:with-param name="w" select="$w"/>
			<xsl:with-param name="h" select="$h"/>
		</xsl:apply-templates>
		<!-- On passe à la suivante si nécessaire -->
		<xsl:variable name="rh" select="@height"/>
		<xsl:if test="$y + $h &lt; $rh">
			<xsl:apply-templates select="." mode="tuile-v">
				<xsl:with-param name="nh" select="$nh"/>
				<xsl:with-param name="nv" select="$nv + 1"/>
				<xsl:with-param name="x" select="$x"/>
				<xsl:with-param name="y" select="$y + $h"/>
				<xsl:with-param name="w" select="$w"/>
				<xsl:with-param name="h" select="$h"/>
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template>

	<xsl:template match="dir:file" mode="tuile">
		<xsl:param name="nh" select="1"/>
		<xsl:param name="nv" select="1"/>
		<xsl:param name="x" select="1"/>
		<xsl:param name="y" select="1"/>
		<xsl:param name="w" select="100"/>
		<xsl:param name="h" select="100"/>

		<!-- Calcul du nombre de tuiles en largeur puis du numéro de la tuile -->
		<xsl:variable name="nb-h" select="ceiling(@width div $w)"/>
		<xsl:variable name="no" select="($nv - 1) * $nb-h + ($nh - 1)"/>

		<!-- <tile h="{$nh}" v="{$nv}" no="{$no}"> -->
		<xsl:variable name="overflow-w" select="($x + $w) - @width"/>
		<xsl:variable name="ow">
			<xsl:choose>
				<xsl:when test="$overflow-w > 0">
					<xsl:value-of select="$w - $overflow-w"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$w"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="overflow-h" select="($y + $h) - @height"/>
		<xsl:variable name="oh">
			<xsl:choose>
				<xsl:when test="$overflow-h > 0">
					<xsl:value-of select="$h - $overflow-h"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$h"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="src" select="concat($serie-path,@name, '?w=', $ow, '&amp;h=', $h, '&amp;x=', $x, '&amp;y=', $y)"/>
		<xsl:call-template name="sortie-image">
			<xsl:with-param name="mimetype" select="@mimetype"/>
			<xsl:with-param name="width" select="$ow"/>
			<xsl:with-param name="height" select="$oh"/>
			<xsl:with-param name="rule-width" select="$w"/>
			<xsl:with-param name="rule-height" select="$h"/>
			<xsl:with-param name="watermark" select="@watermark"/>
			<xsl:with-param name="src" select="$src"/>
			<xsl:with-param name="name" select="@name"/>
			<xsl:with-param name="date" select="@date"/>
			<xsl:with-param name="xno" select="$nh"/>
			<xsl:with-param name="yno" select="$nv"/>
			<xsl:with-param name="no" select="$no"/>
		</xsl:call-template>
		<!-- </tile> -->
	</xsl:template>

	<xsl:template name="sortie-image">
		<xsl:param name="mimetype" select="''"/>
		<xsl:param name="width" select="0"/>
		<xsl:param name="height" select="0"/>
		<xsl:param name="rule-width" select="0"/>
		<xsl:param name="rule-height" select="0"/>
		<xsl:param name="size" select="0"/>
		<xsl:param name="src" select="''"/>
		<xsl:param name="name" select="''"/>
		<xsl:param name="date" select="''"/>
		<xsl:param name="role" select="''"/>
		<xsl:param name="label" select="''"/>
		<xsl:param name="xno" select="0"/>
		<xsl:param name="yno" select="0"/>
		<xsl:param name="no" select="-1"/>
		<xsl:param name="watermark" select="''" />
		<xsl:param name="prepared" select="''" />
		<image src="{$src}">
			<xsl:if test="$name != ''">
				<xsl:attribute name="name">
					<xsl:value-of select="$name"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$date != ''">
				<xsl:attribute name="date">
					<xsl:value-of select="$date"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$width castable as xs:integer and $width > 0">
				<xsl:attribute name="width">
					<xsl:value-of select="$width"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$height castable as xs:integer and $height > 0">
				<xsl:attribute name="height">
					<xsl:value-of select="$height"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$rule-width castable as xs:integer and $rule-width > 0">
				<xsl:attribute name="ruleWidth">
					<xsl:value-of select="$rule-width"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$rule-height castable as xs:integer and $rule-height > 0">
				<xsl:attribute name="ruleHeight">
					<xsl:value-of select="$rule-height"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$size castable as xs:integer and $size > 0">
				<xsl:attribute name="size">
					<xsl:value-of select="$size"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$label != ''">
				<xsl:attribute name="label">
					<xsl:value-of select="$label"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$role != ''">
				<xsl:attribute name="role">
					<xsl:value-of select="$role"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$mimetype != ''">
				<xsl:attribute name="mimetype">
					<xsl:value-of select="$mimetype"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$xno != 0">
				<xsl:attribute name="xno">
					<xsl:value-of select="$xno"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$yno != 0">
				<xsl:attribute name="yno">
					<xsl:value-of select="$yno"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$no != -1">
				<xsl:attribute name="no">
					<xsl:value-of select="$no"/>
				</xsl:attribute>
			</xsl:if>
		</image>
	</xsl:template>

</xsl:stylesheet>
