<?xml version="1.0" encoding="UTF-8"?>
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		XSLT qui transforme le modèle HTML de manière à ce qu'il
		devienne une application complète
-->

<!--$Id$-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:piv="http://pleade.org/ns/pleade/piv/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="xsl xhtml xs piv fn">

	<!-- L'URL relative vers le thème -->
	<xsl:param name="theme-url" select="'../../theme'"/>

	<xsl:param name="distrib" select="'false'"/>

	<!-- L'URL vers le serveur d'images -->
	<xsl:param name="img-server-url" select="''"/>

	<!-- La zone courante de l'utilisateur -->
	<xsl:param name="pleade-zone" select="''"/>

	<!-- Configuration des fonctionnalités -->
	<xsl:param name="title-enabled" select="'true'"/>
	<xsl:param name="pdf-enabled" select="'true'"/>
	<xsl:param name="zip-enabled" select="'true'"/>
	<xsl:param name="navigation-enabled" select="'true'"/>
	<xsl:param name="status-enabled" select="'true'"/>

	<!-- Configuration de l'affichage des boutons -->
	<xsl:param name="zoom-enabled" select="'true'"/>
	<xsl:param name="lock-enabled" select="'true'"/>
	<xsl:param name="resize-enabled" select="'true'"/>
	<xsl:param name="rotation-enabled" select="'true'"/>
	<xsl:param name="contrast-enabled" select="'true'"/>
	<xsl:param name="luminosity-enabled" select="'true'"/>
	<xsl:param name="preview-enabled" select="'true'"/>
	<xsl:param name="preview-onload" select="'true'"/>
	<xsl:param name="print-enabled" select="'true'"/>
	<xsl:param name="print-formats" select="'A4'"/>
	<xsl:param name="grid-enabled" select="'true'"/>
	<xsl:param name="max-zoom" select="'120'"/>
  <xsl:param name="min-zoom" select="'20'"/>
  <xsl:param name="fragment-infos-enabled" select="'false'"/>
  <xsl:param name="params-enabled" select="'false'"/>
  <xsl:param name="slideshow-enabled" select="'false'"/>
  <xsl:param name="nav-cotes-enabled" select="'false'"/>

	<!-- Nombre maximum d'images pour les téléchargements -->
	<xsl:param name="download-max"/>

	<!-- L'URL de base -->
	<xsl:param name="baselink" select="''"/>

	<!-- Un pointeur vers la définition de la série d'images -->
	<xsl:variable name="series" select="/aggregate/series"/>

	<!-- Le code du catalogue i18n à utiliser -->
	<xsl:variable name="i18n-catalogue" select="'module-img-viewer'"/>

	<!-- Les paramètres à passer au JS pour y (dés)activer les fonctionnalités et boutons -->
	<xsl:variable name="enabled-for-js">
		<xsl:text>{"title": </xsl:text>
		<xsl:value-of select="piv:is-enabled($title-enabled)"/>
		<xsl:text>, "pdf": </xsl:text>
		<xsl:value-of select="piv:is-enabled($pdf-enabled)"/>
		<xsl:text>, "zip": </xsl:text>
		<xsl:value-of select="piv:is-enabled($zip-enabled)"/>
		<xsl:text>, "navigation": </xsl:text>
		<xsl:value-of select="piv:is-enabled($navigation-enabled)"/>
		<xsl:text>, "status": </xsl:text>
		<xsl:value-of select="piv:is-enabled($status-enabled)"/>
		<xsl:text>, "zoom": </xsl:text>
		<xsl:value-of select="piv:is-enabled($zoom-enabled)"/>
		<xsl:text>, "lock": </xsl:text>
		<xsl:value-of select="piv:is-enabled($lock-enabled)"/>
		<xsl:text>, "resize": </xsl:text>
		<xsl:value-of select="piv:is-enabled($resize-enabled)"/>
		<xsl:text>, "rotation": </xsl:text>
		<xsl:value-of select="piv:is-enabled($rotation-enabled)"/>
		<xsl:text>, "contrast": </xsl:text>
		<xsl:value-of select="piv:is-enabled($contrast-enabled)"/>
		<xsl:text>, "luminosity": </xsl:text>
		<xsl:value-of select="piv:is-enabled($luminosity-enabled)"/>
    <xsl:text>, "fragmentInfos": </xsl:text>
    <xsl:value-of select="piv:is-enabled($fragment-infos-enabled)"/>
		<xsl:text>, "params": </xsl:text>
		<xsl:value-of select="piv:is-enabled($params-enabled)"/>
		<xsl:text>, "preview": </xsl:text>
		<xsl:value-of select="piv:is-enabled($preview-enabled)"/>
		<xsl:text>, "previewOnLoad": </xsl:text>
		<xsl:value-of select="piv:is-enabled($preview-onload)"/>
		<xsl:text>, "print": </xsl:text>
		<xsl:value-of select="piv:is-enabled($print-enabled)"/>
		<xsl:text>, "printFormats": "</xsl:text>
		<xsl:value-of select="$print-formats"/><xsl:text>"</xsl:text>
		<xsl:text>, "rotationAngle": </xsl:text>
		<xsl:value-of select="90"/>
		<xsl:text>, "downloadMax": </xsl:text>
		<xsl:value-of select="$download-max"/>
    <xsl:text>, "navigationCotes": </xsl:text>
    <xsl:value-of select="piv:is-enabled($nav-cotes-enabled)"/>
		<!--Appel pour la declaration des variables specifiques à la visionneuse classique-->
		<xsl:call-template name="viewer-enabled-for-js"/>
		<!--Appel au template de surcharge  de variable-->
		<xsl:call-template name="more-enabled-for-js"/>
		<xsl:text>}</xsl:text>
	</xsl:variable>

	<!-- Template permettant la surcharge de declaration de variable pour le JS -->
	<xsl:template name="viewer-enabled-for-js">
		<xsl:text>, "grid": </xsl:text>
		<xsl:value-of select="piv:is-enabled($grid-enabled)"/>
		<xsl:text>, "maxZoom": </xsl:text>
		<xsl:value-of select="$max-zoom"/>
		<xsl:text>, "minZoom": </xsl:text>
		<xsl:value-of select="$min-zoom"/>
	</xsl:template>

	<!-- Template permettant la surcharge de declaration de variable pour le JS -->
	<xsl:template name="more-enabled-for-js">
	
	</xsl:template>
	
	<!-- Une fonction qui permet de savoir si le paramètre doit être placé à true ou false -->
	<xsl:function name="piv:is-enabled" as="xs:boolean">
		<xsl:param name="value"/>
		<xsl:choose>
			<xsl:when test="$value castable as xs:boolean">
				<xsl:value-of select="$value"/>
			</xsl:when>
			<xsl:otherwise>
				<!-- Vérif des zones -->
				<xsl:choose>
					<xsl:when test="fn:tokenize($pleade-zone, ',') = fn:tokenize($value, ',')">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<!-- L'élément racine de l'aggregate -->
	<xsl:template match="/aggregate">
		<xsl:apply-templates select="xhtml:html"/>
	</xsl:template>

	<!-- On intercepte les attributs src, href, ... -->
	<xsl:template match="@src">
		<xsl:attribute name="src"><xsl:value-of select="$theme-url"/>/<xsl:value-of select="."/></xsl:attribute>
	</xsl:template>
	<xsl:template match="xhtml:link/@href">
		<xsl:attribute name="href"><xsl:value-of select="$theme-url"/>/<xsl:value-of select="."/></xsl:attribute>
	</xsl:template>

	<!-- On ajoute un élément script pour contenir quelques variables globales -->
	<xsl:template match="xhtml:head">
		<xsl:copy>
			<xsl:apply-templates select="@*|text()"/>
			<xsl:apply-templates select="xhtml:title"/>
			<xsl:apply-templates select="xhtml:meta"/>
			<i18n:text key="viewer.meta.tags" catalogue="{$i18n-catalogue}"><meta name="copyright" content="Copyright AJLSM"/></i18n:text>
			<xsl:call-template name="css-import"/>
			<xsl:apply-templates select="xhtml:*[not(self::xhtml:meta) and not(self::xhtml:title) and (not(self::xhtml:link) or not(@rel='stylesheet'))]"/>
		</xsl:copy>
	</xsl:template>

	<!-- Template permettant de declarer les css dans le head de la page -->
	<xsl:template name="css-import">
		<xsl:choose>
			<!-- Pour la distribution, on sert des fichiers regroupés et compressés -->
			<xsl:when test="$distrib = 'true'">
				<link href="{$theme-url}/css/pleade-img-viewer-min.css" type="text/css" rel="stylesheet" />
				<!-- FIXME: les CSS locales sont gérées dans les projets... Le système ne peut pas prendre ça en compte atuellement -->
				<link href="{$theme-url}/css/local/pleade.css" type="text/css" rel="stylesheet" />
				<link href="{$theme-url}/css/local/module-img-viewer.css" rel="stylesheet" type="text/css" />
			</xsl:when>
			<!-- Hors distribution, on sert chacun des fichiers, non compressés -->
			<xsl:otherwise>
				<link href="{$theme-url}/css/pleade.css" type="text/css" rel="stylesheet" />
				<link href="{$theme-url}/css/local/pleade.css" type="text/css" rel="stylesheet" />
				<link href="{$theme-url}/css/module-img-viewer.css" rel="stylesheet" type="text/css" />
				<link href="{$theme-url}/css/local/module-img-viewer.css" rel="stylesheet" type="text/css" />
			</xsl:otherwise>
		</xsl:choose>

		<!-- Si jamais il y a des appels CSS dans le template, on les inclus ici -->
		<xsl:apply-templates select="xhtml:link[@rel='stylesheet']"/>

		<!-- On intègre maintenant les feuilles de styles pour IE -->
		<xsl:comment><![CDATA[[if IE]><link type="text/css" rel="stylesheet" href="]]><xsl:value-of select="$theme-url"/>/css/module-img-viewer-IE.css<![CDATA["/><![endif]]]></xsl:comment>
		<xsl:comment><![CDATA[[if IE 6]><link type="text/css" rel="stylesheet" href="]]><xsl:value-of select="$theme-url"/>/css/module-img-viewer-IE6.css<![CDATA["/><![endif]]]></xsl:comment>
		<xsl:comment><![CDATA[[if lte IE 7]><link type="text/css" rel="stylesheet" href="]]><xsl:value-of select="$theme-url"/>/css/module-img-viewer-IE7.css<![CDATA["/><![endif]]]></xsl:comment>
		<!-- On intègre les memes en local-->
		<xsl:comment><![CDATA[[if IE]><link type="text/css" rel="stylesheet" href="]]><xsl:value-of select="$theme-url"/>/css/local/module-img-viewer-IE-local.css<![CDATA["/><![endif]]]></xsl:comment>
		<xsl:comment><![CDATA[[if IE 6]><link type="text/css" rel="stylesheet" href="]]><xsl:value-of select="$theme-url"/>/css/local/module-img-viewer-IE6-local.css<![CDATA["/><![endif]]]></xsl:comment>
		<xsl:comment><![CDATA[[if lte IE 7]><link type="text/css" rel="stylesheet" href="]]><xsl:value-of select="$theme-url"/>/css/local/module-img-viewer-IE7-local.css<![CDATA["/><![endif]]]></xsl:comment>

		<!-- Enfin, les éventuelles surcharges -->
		<xsl:call-template name="more-css-import"/>
	</xsl:template>

	<!-- Template permettant de surcharger la declaration des css de la page -->
	<xsl:template name="more-css-import">
	</xsl:template>

	<!-- Template permettant de declarer les javascript dans le head de la page -->
	<xsl:template name="javascript-import">
		<!-- Attention, ce n'est pas un fichier ! -->
		<script type="text/javascript" src="{$theme-url}/i18n/module-img-viewer-js.js"> <xsl:comment>u</xsl:comment></script>

		<xsl:choose>
			<!-- Pour la distribution, on sert des fichiers regroupés et compressés -->
			<xsl:when test="$distrib = 'true'">
				<!-- Frameworks -->
        <script type="text/javascript" src="{$theme-url}/js/mootools/mootools-core-1.3.1-yc.js"><xsl:comment>u</xsl:comment></script>
        <script type="text/javascript" src="{$theme-url}/js/mootools/mootools-more-1.3.1.1-yc.js"><xsl:comment>u</xsl:comment></script>
				<!-- Generics -->
				<script type="text/javascript" src="{$theme-url}/js/pleade/pleade-img-viewer-generics-min.js"> <xsl:comment>u</xsl:comment></script>
				<!-- Viewer specific -->
				<script type="text/javascript" src="{$theme-url}/js/pleade/pleade-img-viewer-min.js"><xsl:comment>u</xsl:comment></script>
			</xsl:when>
			<!-- Hors distribution, on sert chacun des fichiers, non compressés -->
			<xsl:otherwise>
				<!-- Frameworks -->
        <script type="text/javascript" src="{$theme-url}/js/mootools/mootools-core-1.3.1-nc.js"><xsl:comment>u</xsl:comment></script>
        <script type="text/javascript" src="{$theme-url}/js/mootools/mootools-more-1.3.1.1-nc.js"><xsl:comment>u</xsl:comment></script>
				<!-- Generics -->
				<script type="text/javascript" src="{$theme-url}/js/pleade/common/util/AjaxRequestHandler.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/common/manager/WindowManager.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/common/manager/LayoutManager.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/common/loadingmanager/Loading.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/common/util/mootools-common.js"> <xsl:comment>u</xsl:comment></script>
				<!-- Viewer specific -->
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/cwindow.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/widget/printpanel.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/widget/downloadpanel.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/widget/navbar.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/widget/slidebar.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/image.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/series.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/viewer.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/event.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/frame.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/panel.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/grid.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/grid-tile.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/selection.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/widget/overview.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/widget/toolbar.js"> <xsl:comment>u</xsl:comment></script>
        <script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/widget/fragmentinfospanel.js"> <xsl:comment>u</xsl:comment></script>
				<script type="text/javascript" src="{$theme-url}/js/pleade/module/img/viewer/zoom.js"> <xsl:comment>u</xsl:comment></script>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:call-template name="more-javascript-import"/>
		<xsl:call-template name="viewer-init"/>
	</xsl:template>

	<!-- Template permettant de surcharger la declaration des javascript dans le head de la page -->
	<xsl:template name="more-javascript-import">
	</xsl:template>

	<xsl:template name="viewer-init">
		<script type="text/javascript">
			<xsl:variable name="start" select="
					if($series/@name and $series/@name != '') then $series/@name
					else if ($series/@first eq '-1') then $series/@name-first
					else $series/@first
			"/>
			<xsl:variable name="end" select="
					if($series/@name and $series/@name != '') then $series/@name
					else if ($series/@last eq '-1') then $series/@name-last
					else $series/@name-last
			"/>
			var windowManager = "";
			var viewer = "";
			window.addEvent('domready', function() {
				windowManager = new WindowManager();
				<xsl:value-of select="concat('viewer = new pivViewer(&quot;', $img-server-url, '&quot;, &quot;', $series/@path, '&quot;, ', ($series/@size, 0), ', ', ($series/@initial, 0), ', &quot;', $baselink, '&quot;, ', $enabled-for-js,',&quot;',$start,'&quot;,&quot;',$end,'&quot;);')"/>
			})
			<xsl:call-template name="more-viewer-init"/>
		</script>
	</xsl:template>

	<!-- Surcharge : initialisation de paramètres supplémentaires -->
	<xsl:template name="param-viewer-init"/>

	<!-- Surcharge : aramètres supplémentaires pour l'initialisation de la visionneuse -->
	<xsl:template name="more-viewer-init"/>

	<!-- Traitement du titre de la page, à composer de différentes parties -->
	<xsl:template match="xhtml:title">
		<title>
			<i18n:text i18n:key="viewer.title.prefix" i18n:catalogue="{$i18n-catalogue}"/>
			<xsl:call-template name="get-specific-title"/>
			<i18n:text i18n:key="viewer.title.suffix" i18n:catalogue="{$i18n-catalogue}"/>
		</title>
	</xsl:template>

	<!-- Retourne la partie spécifique du titre de page -->
	<!-- TODO: à faire en fonction de l'architecture pour obtenir des informations sur la série -->
	<xsl:template name="get-specific-title">
		<xsl:value-of select="$series/@path"/>
	</xsl:template>

	<!-- Affichage ou non du titre de l'IR représentées par l'image -->
	<xsl:template match="xhtml:div[@id='piv-series-info']">
		<xsl:if test="$title-enabled = 'true'">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Affichage ou non du cadre de status de l'image -->
	<xsl:template match="xhtml:div[@id='pnl-status']">
		<xsl:if test="$status-enabled = 'true'">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Affichage du menu haut, seulement si l'une des options qu'il contient est active -->
	<!-- [JC] FIXME: la navigation est gérée par le javascript...
	Que faire ? La sortir de ce div ? -->
	<!--<xsl:template match="xhtml:div[@id='piv-body-hmenu']">
		<xsl:if test="$pdf-enabled = 'true' or $zip-enabled = 'true' or $navigation-enabled = 'true'">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>-->

	<!-- Affichage de la zone de téléchargement, uniquement si soit pdf soit zip est actif -->
	<xsl:template match="xhtml:div[@id='piv-dl-menu']">
		<xsl:if test="piv:is-enabled($pdf-enabled) or piv:is-enabled($zip-enabled)">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Affichage du bouton de téléchargement, uniquement si soit pdf soit zip est actif -->
	<xsl:template match="xhtml:a[@id='btn-dl']">
		<xsl:if test="piv:is-enabled($pdf-enabled) or piv:is-enabled($zip-enabled)">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Téléchargement au format PDF -->
	<xsl:template match="xhtml:span[@id='dl-pdf-format']">
		<xsl:if test="piv:is-enabled($pdf-enabled)">
			<xsl:choose>
				<!-- Si on a uniquement le PDF activé, pas besoin de bouton radio, on ne veut que le label -->
				<xsl:when test="not(piv:is-enabled($zip-enabled))">
          <xsl:copy>
            <xsl:apply-templates select="xhtml:label/*|@*|text()"/>
          </xsl:copy>
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy>
						<xsl:apply-templates select="node()|@*|text()"/>
					</xsl:copy>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<!-- Téléchargement au format ZIP -->
	<xsl:template match="xhtml:span[@id='dl-zip-format']">
		<xsl:if test="piv:is-enabled($zip-enabled)">
			<xsl:choose>
				<!-- Si on a uniquement le ZIP activé, pas besoin de bouton radio, on ne veut que le label -->
				<xsl:when test="not(piv:is-enabled($pdf-enabled))">
					<xsl:copy>
						<xsl:apply-templates select="xhtml:label/*|@*|text()"/>
					</xsl:copy>
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy>
						<xsl:apply-templates select="node()|@*|text()"/>
					</xsl:copy>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<!-- Affichage ou non des boutons de zoom -->
	<xsl:template match="xhtml:a[@id='btn-zoomin']|xhtml:a[@id='btn-zoomout']|xhtml:a[@id='btn-zoomselect']">
		<xsl:if test="piv:is-enabled($zoom-enabled)">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Affichage ou non du bouton de verrouillage du zoom -->
	<xsl:template match="xhtml:a[@id='btn-lockzoom']">
		<xsl:if test="piv:is-enabled($zoom-enabled) and piv:is-enabled($lock-enabled)">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Affichage ou non des boutons pour adapter la taille de l'image -->
	<xsl:template match="xhtml:a[@id='btn-realsize']|xhtml:a[@id='btn-fitscreen']">
		<xsl:if test="piv:is-enabled($resize-enabled)">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Affichage ou non du bouton de rotation -->
	<xsl:template match="xhtml:a[@id='btn-rotate']">
		<!-- Le bouton de rotation ne doit jamais être affiché pour les images attachées -->
		<xsl:if test="piv:is-enabled($rotation-enabled) and not( contains(/aggregate/series/@path, '/attached/') )">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Affichage ou non du bouton de contraste -->
	<!-- [JC] FIXME: non implémenté -->
	<xsl:template match="xhtml:a[@id='btn-contrast']">
		<xsl:if test="piv:is-enabled($contrast-enabled)">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Affachage ou non du bouton de la mini-fenêtre de visualisation -->
	<xsl:template match="xhtml:a[@id='btn-overview']">
		<xsl:if test="piv:is-enabled($preview-enabled)">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Affichage ou non du bouton impression -->
	<xsl:template match="xhtml:a[@id='btn-print']">
		<xsl:if test="piv:is-enabled($print-enabled)">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Le menu impression. Si l'impression est désactivée, on ne l'envoie pas -->
	<xsl:template match="xhtml:div[@id='piv-print-menu']">
		<xsl:if test="piv:is-enabled($print-enabled)">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Affichage ou non du bouton grille -->
	<!-- FIXME: l'affichage grille est à passer dans le menu des versions d'images -->
	<xsl:template match="xhtml:a[@id='btn-grid']">
		<xsl:if test="piv:is-enabled($grid-enabled)">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<!-- Affichage ou non du bouton paramètres -->
	<xsl:template match="xhtml:a[@id='btn-param']">
		<xsl:if test="piv:is-enabled($params-enabled)">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*|text()"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>

  <!-- Le panneau d'informations sur le fragment. Si l'affichage des information sur le fragment est désactivée, on ne l'envoie pas. On ne l'envoie pas non plus si la récupération du titre est désactivée. -->
  <xsl:template match="xhtml:div[@id='pnl-fragment-infos']">
    <xsl:if test="piv:is-enabled($title-enabled) and piv:is-enabled($fragment-infos-enabled)">
      <xsl:copy>
        <xsl:apply-templates select="node()|@*|text()"/>
      </xsl:copy>
    </xsl:if>
  </xsl:template>

  <!-- La navigation par cote -->
  <xsl:template match="xhtml:div[@id='pnl-nav-cotes']">
    <xsl:if test="piv:is-enabled($nav-cotes-enabled)">
      <xsl:copy>
        <xsl:apply-templates select="node()|@*|text()"/>
      </xsl:copy>
    </xsl:if>
  </xsl:template>

  <!-- Le séparateur pour la navigation par cote -->
  <xsl:template match="xhtml:span[@id='pnl-nav-cotes-separator']">
    <xsl:if test="piv:is-enabled($nav-cotes-enabled)">
      <xsl:copy>
        <xsl:apply-templates select="node()|@*|text()"/>
      </xsl:copy>
    </xsl:if>
  </xsl:template>

	<!-- On fixe les liens ; si c'est nécessaire -->
	<xsl:template match="xhtml:a[@href != '#' and not(starts-with(@href, 'http://'))]">
		<a>
			<xsl:attribute name="href">
				<xsl:value-of select="$baselink"/>
				<xsl:value-of select="@href"/>
			</xsl:attribute>
			<xsl:copy-of select="@*[not(local-name() = 'href')]"/>
			<xsl:apply-templates/>
		</a>
	</xsl:template>

	<!-- On supprime les éléments classés en "_remove" -->
	<xsl:template match="xhtml:*[@class = '_remove']"/>

	<xsl:template match="xhtml:body">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*|text()"/>
			<xsl:call-template name="javascript-import"/>
		</xsl:copy>
	</xsl:template>

	<!-- Copie des éléments HTML et i18n -->
	<xsl:template match="xhtml:*|i18n:*|@*|text()|comment()">
		<!-- DEBUG -->
		<!--<xsl:message>TRAITEMENT DE: <xsl:value-of select="local-name()"/> (<xsl:value-of select="."/>)</xsl:message>-->
		<xsl:copy>
			<xsl:apply-templates select="node()|@*|text()"/>
		</xsl:copy>
	</xsl:template>

	<!-- On intercepte le reste -->
	<xsl:template match="*"/>

</xsl:stylesheet>

