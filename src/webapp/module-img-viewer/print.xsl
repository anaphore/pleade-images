<?xml version="1.0" encoding="UTF-8"?>
<!--$Id$-->
<!--
Pleade: Outil de publication pour instruments de recherche, notices d'autorités
et corpus d'images numérisés.
Copyright (C) 2003-2011 AJLSM, Anaphore

AJLSM
17, rue Vital Carles
33000 Bordeaux, France
info@ajlsm.com

Anaphore SARL
3 ter chemin de la fontaine
13570 Barbentane, France
info@anaphore.eu

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
-->
<!--
		XSLT qui pilote l'impression d'une ou de plusieurs images.
		Elle s'applique à un aggregate qui contient le modèle HTML
		ainsi qu'une liste d'images.
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:pleade="http://pleade.org/ns/pleade/1.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="xsl xhtml pleade xs">
	
	<!-- L'URL vers le dossier theme -->
	<xsl:param name="theme-url" select="''"/>

	<!-- L'URL de base du serveur d'images -->
	<xsl:param name="img-server-url" select="''"/>
	
	<!-- Le résumé de l'image -->
	<xsl:param name="summary" select="''"/>
	
	<!-- Une fonction pour calculer l'orientation -->
	<xsl:function name="pleade:get-orientation" as="xs:string">
		<xsl:param name="img"/>
		<xsl:choose>
			<xsl:when test="number($img/@width) > number($img/@height)">landscape</xsl:when>
			<xsl:otherwise>portrait</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
	
	<!-- L'orientation de la première image -->
	<xsl:variable name="orientation-first" select="pleade:get-orientation(/aggregate/images/image[1])"/>

	<!-- L'élément racine du aggregate -->
	<xsl:template match="/aggregate">
		<!-- On déclenche la sortie du HTML depuis le template -->
		<xsl:apply-templates select="xhtml:html"/>
	</xsl:template>
	
	<!-- On ajoute une variable globale pour l'orientation -->
	<xsl:template match="xhtml:head">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
			<script type="text/javascript">
				var orientation = "<xsl:value-of select="$orientation-first"/>";
			</script>
		</xsl:copy>
	</xsl:template>
	
	<!-- Un emplacement pour les infos images -->
	<xsl:template match="xhtml:span[@class='infos']">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:value-of select="$summary"/>
		</xsl:copy>
	</xsl:template>

	<!-- On intercepte le div qui doit être sorti pour chaque image. -->
	<xsl:template match="xhtml:div[@class='page']">
		<xsl:variable name="me" select="."/>
		<!-- On boucle sur toutes les images disponibles -->
		<xsl:for-each select="/aggregate/images/image">
			<xsl:variable name="img" select="."/>
			<xsl:apply-templates select="$me" mode="output-image">
				<xsl:with-param name="current-image" select="$img" tunnel="yes"/>
			</xsl:apply-templates>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="xhtml:div[@class='page']" mode="output-image">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>
	
	<!-- Le div qui contient l'image -->
	<xsl:template match="xhtml:div[@class='image']">
		<xsl:param name="current-image" select="/dummy" tunnel="yes"/>
		<!-- On détermine l'orientation -->
		<xsl:variable name="orientation" select="pleade:get-orientation($current-image)"/>
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<!-- On modidie la classe -->
			<xsl:attribute name="class">image image-<xsl:value-of select="$orientation"/></xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	
	<!-- L'image à insérer -->
	<xsl:template match="xhtml:img[@class='img']">
		<xsl:param name="current-image" select="/dummy" tunnel="yes"/>
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="src">
				<xsl:value-of select="concat($img-server-url, '/', $current-image/@relative-url)"/>
			</xsl:attribute>
			<xsl:attribute name="height"><xsl:value-of select="$current-image/@height"/></xsl:attribute>
			<xsl:attribute name="width"><xsl:value-of select="$current-image/@width"/></xsl:attribute>
		</xsl:copy>
	</xsl:template>

	
	<!-- Les éléments du modèle à surcharger -->

	<!-- Liens vers des CSS -->
	<xsl:template match="xhtml:link/@href">
		<xsl:attribute name="href"><xsl:value-of select="$theme-url"/>/<xsl:value-of select="."/></xsl:attribute>
	</xsl:template>
	
	<!-- Titre de la page -->
	<xsl:template match="xhtml:title">
		<title>
			<!-- Celui du modèle -->
			<xsl:apply-templates/>
			<!-- On ajoute le sommaire -->
			<xsl:value-of select="$summary"/>
		</title>
	</xsl:template>

	<xsl:template match="xhtml:*">
		<xsl:element name="{local-name()}">
			<xsl:apply-templates select="@*|node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="xhtml:*/@*" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
